#ifndef DEM_H
#define DEM_H

#include <cmath>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <ios>
#include <cstdint>
#include <stdexcept>
#include <string>
#include <vector>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>



static const double re = 6371.009; // mean earth radius in Km (IUGG)


/// Gets the lat and long of a point distxy km north/east of clatlong (approximate due to orthographic projection)
std::pair<double, double> GetLatLong(const std::pair<double, double> &clatlong, const std::pair<double, double>&distxy)
{
	double x,y,z;
	double x2,z2;
	
	double clat = clatlong.first*M_PI/180.0;
	double clong = clatlong.second*M_PI/180.0;
	x2 = cos(clat);
	z2 = sin(clat);
	y = 0;
	
	x = x2 - sin(clat)*distxy.second/re;
	z = z2 + cos(clat)*distxy.second/re;
	y = distxy.first/re;
	
	double d = sqrt(x*x+y*y+z*z);
	double dp = sqrt(x*x+y*y);
	x/=dp; y/=dp; z/=d;
	
	double elat, elong;
	elat=asin(z);
	elong=fmod(2.0*M_PI + clong + atan2(y,x),2.0*M_PI);
	return std::pair<double,double>(elat*180.0/M_PI,elong*180.0/M_PI);
}

std::string LongName(int i)
{
	std::ostringstream oss;
	if (i<=179) 
	{
		oss << "e";
		oss << std::setw(3) << std::setfill('0') << i;
	}
	else
	{
		oss << "w";
		oss << std::setw(3) << std::setfill('0') << 360-i;
	}
	return oss.str();
}
std::string LatName(int i)
{
	std::ostringstream oss;
	if (i>=0) 
	{
		oss << "n";
		oss << std::setw(2) << std::setfill('0') << i;
	}
	else
	{
		oss << "s";
		oss << std::setw(2) << std::setfill('0') << -i;
	}
	return oss.str();
}	

template <class T>
class SimpleImage
{
private:
	std::vector<T> data;
	int width, height;
public:
	SimpleImage()
	{
		width=0;
		height=0;
	}
	SimpleImage(int width, int height)
	{
		this->width=width;
		this->height=height;
		data=std::vector<T>(width*height);
	}
	SimpleImage(int width, int height, T initial)
	{
		this->width=width;
		this->height=height;
		data=std::vector<T>(width*height, initial);
	}
	double Interp(double x, double y);
	void Load(std::ifstream &f)
	{
		f.read(reinterpret_cast<char *>(&(data[0])),width*height*2);	
	}
	T operator()(int x, int y) const
	{
		if (x<0 || x>=width || y<0 || y>=height) throw std::invalid_argument("1");
		return data[x+width*y];
	}
	T &operator()(int x, int y)
	{
		if (x<0 || x>=width || y<0 || y>=height) throw std::invalid_argument("2");
		return data[x+width*y];
	}
	void OutputPlainText(std::ostream &out)
	{		
		for (int j=0; j<height; j++) 
		{
			for (int i=0; i<width; i++) out << (*this)(i, j) << " ";
			out << std::endl;
		}
	}
	int Width() const {return width;}
	int Height() const {return height;}
};

typedef SimpleImage<int16_t> ShortImage;


/// This bicubic interpolation is problematic at the edges of images.
template <class T>
double SimpleImage<T>::Interp(double x, double y)
{
	// Bicubic interpolation:
	// Assume function takes the form:
	// 	f(x,y) = sum(i=0,...,3) sum(j=0,...,3) a_ij x^i y^j        for (x,y) in (0,1)x(0,1)

	// a_ij constants determined by known f[], the f values, derivatives, and cross derivative at (0,0), (0,1), (1,0) and (1,1)
	// Given assumed form for f(x,y), can get formulae for the components of f[] in terms of the a_ij
	//      e.g. f_x(0,0) = sum(i=0,...,3) sum(j=0,...,3) a_ij i x^(i-1) y^j     at (x=0, y=0)
	//		       = sum(i=0,...,3) sum(j=0,...,3) a_ij i 0^(i-1) 0^j
	//		       = a_10						      since 0^k = 0 if k>0
	// 	15 other similar equations, for f, f_x, f_y, f_xy at each of (0,0), (0,1), (1,0), (1,1)
	// Thus we know f[] in terms of a matrix applied to the length-16 vector a_ij 

	// Invert this 16x16 matrix to get m[][], which determines the vector a_ij from the known f[]

	// We don't actually know the derivative parts of f[], but can approximate these by finite differencing

	int i,j,k;
	int fx=int(floor(x)), cx=int(ceil(x)), fy=int(floor(y)), cy=int(ceil(y));
	double rx=x-floor(x), ry=y-floor(y);
	double f[16]=
		{
			static_cast<double>((*this)(fx,fy)),		// f
			static_cast<double>((*this)(cx,fy)),
			static_cast<double>((*this)(fx,cy)),
			static_cast<double>((*this)(cx,cy)),
			0.5*static_cast<double>((*this)(fx+1,fy)-(*this)(fx-1,fy)),  // df/dx
			0.5*static_cast<double>((*this)(cx+1,fy)-(*this)(cx-1,fy)),
			0.5*static_cast<double>((*this)(fx+1,cy)-(*this)(fx-1,cy)),
			0.5*static_cast<double>((*this)(cx+1,cy)-(*this)(cx-1,cy)),
			0.5*static_cast<double>((*this)(fx,fy+1)-(*this)(fx,fy-1)), // df/dy
			0.5*static_cast<double>((*this)(cx,fy+1)-(*this)(cx,fy-1)),
			0.5*static_cast<double>((*this)(fx,cy+1)-(*this)(fx,cy-1)),
			0.5*static_cast<double>((*this)(cx,cy+1)-(*this)(cx,cy-1)),
			0.25*static_cast<double>(((*this)(fx+1,fy+1)-(*this)(fx-1,fy+1))-((*this)(fx+1,fy-1)-(*this)(fx-1,fy-1))),  // d2f/dxdy
			0.25*static_cast<double>(((*this)(cx+1,fy+1)-(*this)(cx-1,fy+1))-((*this)(cx+1,fy-1)-(*this)(cx-1,fy-1))),
			0.25*static_cast<double>(((*this)(fx+1,cy+1)-(*this)(fx-1,cy+1))-((*this)(fx+1,cy-1)-(*this)(fx-1,cy-1))),
			0.25*static_cast<double>(((*this)(cx+1,cy+1)-(*this)(cx-1,cy+1))-((*this)(cx+1,cy-1)-(*this)(cx-1,cy-1)))
		};
	double m[16][16]={{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0},
			{-3,0,3,0,0,0,0,0,-2,0,-1,0,0,0,0,0},
			{2,0,-2,0,0,0,0,0,1,0,1,0,0,0,0,0},
			{0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0},
			{0,0,0,0,-3,0,3,0,0,0,0,0,-2,0,-1,0},
			{0,0,0,0,2,0,-2,0,0,0,0,0,1,0,1,0},
			{-3,3,0,0,-2,-1,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,-3,3,0,0,-2,-1,0,0},
			{9,-9,-9,9,6,3,-6,-3,6,-6,3,-3,4,2,2,1},
			{-6,6,6,-6,-4,-2,4,2,-3,3,-3,3,-2,-1,-2,-1},
			{2,-2,0,0,1,1,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,2,-2,0,0,1,1,0,0},
			{-6,6,6,-6,-3,-3,3,3,-4,4,-2,2,-2,-2,-1,-1},
			{4,-4,-4,4,2,2,-2,-2,2,-2,2,-2,1,1,1,1}};

	double a[4][4];
	double ans=0;
	for (i=0; i<4; i++) for (j=0; j<4; j++) a[i][j]=0;
	for (i=0; i<4; i++) for (j=0; j<4; j++)
	{
		for (k=0; k<16; k++) a[i][j]+=m[i*4+j][k]*f[k];
	}

	for (i=0; i<4; i++) for (j=0; j<4; j++) ans+=a[i][j]*std::pow(rx,i)*std::pow(ry,j);
	return ans;
}


/// Fills out nx x ny float with heights centered on (latitude,longitude) and with pixel pitch (pitchX, pitchY) kilometers

/*
 *  Files are 1201x1201 unsigned shorts, covering a 1x1 degree area (i.e. 3 arcsecond resolution), labeled by south-west corner
 *  Adjacent files share common edge row/columns (=> bilinear interp. never requires two files)
 */
SimpleImage<float> GetHeightData(std::string path, int nx, int ny, std::pair<double,double> latlong, double pitchX, double pitchY)
{
	struct stat statbuf;
	if ((stat(path.c_str(), &statbuf) == -1) || !S_ISDIR(statbuf.st_mode))
	{
		cerr << path << " is not a readable directory." << endl;
		return SimpleImage<float>();
	}
	const static int lppf=1201;
	int maxLat, minLat, maxLong, minLong;

	SimpleImage<float> data(nx,ny);
	
	std::pair<double,double> np, sp, ep, wp;
	np = GetLatLong(latlong, std::pair<double,double>(0,ny*pitchY/2.0));
	sp = GetLatLong(latlong, std::pair<double,double>(0,-ny*pitchY/2.0));
	
	minLat=floor(sp.first);
	maxLat=floor(np.first);
	
	double ewpsp;
	if (latlong.first>0) ewpsp = ny*pitchY/2.0; // latitude at which largest region of longitude occurs (possibly...)
	else ewpsp=-ny*pitchY/2.0; // erm, not really sure whats going on here...
	
	ep = GetLatLong(latlong, std::pair<double,double>(nx*pitchX/2.0,ewpsp));
	wp = GetLatLong(latlong, std::pair<double,double>(-nx*pitchX/2.0,ewpsp));
	
	minLong = floor(wp.second);
	maxLong = floor(ep.second);
		
	int latSize = maxLat - minLat + 1;
	int longSize = maxLong - minLong + 1 + (minLong>maxLong)*360;
	
	ShortImage rawdat((longSize)*1200 + 1, (latSize)*1200 + 1,-32767);
	ShortImage rawfile(1201, 1201);
	
	/// Loop through files, loading data into rawdat without any interpolation
	/// Used to change 0 to -1000 ... now not doing this
	for (int latIndex = minLat, latCount=0; latIndex<=maxLat; latIndex++, latCount++)
	{
		for (int longIndex = minLong, longCount=0; ((longIndex)%360)!=((maxLong+1)%360); longIndex=(longIndex+1)%360,longCount++)
		{
			std::string fname = path+"dem/"+LatName(latIndex)+"/"+LatName(latIndex)+LongName(longIndex)+".hgt";
			//cout << fname << "... ";
			std::ifstream datafile(fname, std::ios::in | std::ios::binary);
			if (datafile.good())
			{
				//cout << "OK... ";
				rawfile.Load(datafile);	
				if (datafile.good())
				{
					//cout << "Read."<<endl; 
					for (int i=0; i<lppf; i++) for (int j=0; j<lppf; j++)
					{
						int16_t dat=rawfile(i,j);
						dat=((dat&0xFF00)>>8)+((dat&0x00FF)<<8);
						//if (dat==0) dat=-1000;
						rawdat(i+1200*longCount, j+1200*(latSize-latCount-1)) = dat;
					}
				}
				else ;//cout << "Failed to read."<<endl;
			}
			else ;//cout << "not found"<< endl;
		}
	}

	/// Loop through output image pixels, reading interpolated data from rawdat
	for (int i=0; i<nx; i++) for (int j=0; j<ny; j++)
	{
		float h=0;
		int ss=4; // Subsampling for when output resolution << input resolution 
		for (int subi=0; subi<ss; subi++) for (int subj=0; subj<ss; subj++)
		{
			std::pair<double,double> pt = GetLatLong(latlong, std::pair<double,double>((double(i)+double(subi)/double(ss)-0.5*(nx-1.0))*pitchX,(double(j)+double(subj)/double(ss)-0.5*(ny-1.0))*pitchY));
			double imgx = 1200.0*(fmod(360.0+pt.second-minLong,360.0));
			double imgy = 1200.0*(maxLat+1 - pt.first);
			h+=rawdat.Interp(imgx,imgy);
			/*int xind=floor(imgx), xindp=xind+1;
			int yind=floor(imgy), yindp=yind+1;
			double xr = imgx-xind, yr=imgy-yind;
			h+= (1.0-xr) * (yr*rawdat(xind,yindp) + (1-yr)*rawdat(xind,yind)) // Interpolation for when output resolution >= input resolution
			         +xr * (yr*rawdat(xindp,yindp) + (1-yr)*rawdat(xindp,yind));*/
		}
			
		data(i,j)=h/float(ss*ss);
	}
	return data;
	
	/* 
	  Starting from (lat,long) can get 3D point on spherical earth.
	  Use orthographic projection, in which rectangular flat output image produces concave shape in lat/long coords
	   ( does it really? what about regions which include the poles! need to limit to 85 degrees n/s or thereabouts
	  Thus corners of output array are sufficient for max/min limits of input file loading
	  Interpolation; can assume isotropic filter, but should really mip-map.
	 */
	
}

#endif