function Out = FlowStationData(fileroot,filerange,CntrLat,CntrLon,StationLatLon,varargin);

options = struct('plotting','on', ...
    'gettopog','off', ...
    'maxheight',10);

% read the acceptable names
optionNames = fieldnames(options);

% count arguments
nArgs = length(varargin);
if round(nArgs/2)~=nArgs/2
    error('In PlotFlowTopog if optional inputs are used they must appear as propertyName/propertyValue pairs');
end

for pair = reshape(varargin,2,[])
    inpName = lower(pair{1});
    if any(strmatch(inpName,optionNames))
        options.(inpName) = pair{2};
    else
        error('%s is not a recognized parameter name',inpName)
    end
end

    [mperdeglat,mperdeglon] = GetGeoLengths(CntrLat,CntrLon);
    
    StationX = (StationLatLon(:,2)-CntrLon).*mperdeglon;
    StationY = (StationLatLon(:,1)-CntrLat).*mperdeglat;
    
    [StationX,StationY]
    
    for j=1:numel(filerange)

        dat=LoadCNTSData(sprintf(fileroot,filerange(j)));
        
        switch options.plotting
            case 'on'
                if j==1
                    GetTopog=options.gettopog;
                else
                    GetTopog='off';
                end
                PlotFlowTopog(fileroot,filerange(j),GetTopog,'Stations',[StationX,StationY],'MarkerSize',20,'Marker','.','maxheight',options.maxheight);
        end
        
        for k=1:numel(StationX)
            
            if (StationX(k)<dat.xGrid(1) || StationX(k)>dat.xGrid(end))
                if j==1
                    disp(['Error.  Station', num2str(k),' location is outside of the domain.  Skipping.'])
                end
                Out.depth{k}(j) = NaN;
                Out.velX{k}(j) = NaN;
                Out.velY{k}(j) = NaN;
                Out.speed{k}(j) = NaN;
                Out.phi{k}(j) = NaN;
                Out.erosion{k}(j) = NaN;
                Out.topog{k}(j) = NaN;
            elseif (StationY(k)<dat.yGrid(1) || StationY(k)>dat.yGrid(end))
                if j==1
                    disp(['Error.  Station', num2str(k),' location is outside of the domain.  Skipping.'])
                end
                Out.depth{k}(j) = NaN;
                Out.velX{k}(j) = NaN;
                Out.velY{k}(j) = NaN;
                Out.speed{k}(j) = NaN;
                Out.phi{k}(j) = NaN;
                Out.erosion{k}(j) = NaN;
                Out.topog{k}(j) = NaN;
            else
                [~,Ix] = min((StationX(k)-dat.xGrid).^2);
                [~,Iy] = min((StationY(k)-dat.yGrid).^2);
            
		h = dat.data(Ix,Iy,1);
		if h>0.02
                	Out.depth{k}(j) = dat.data(Ix,Iy,1);
                	Out.velX{k}(j) = dat.data(Ix,Iy,2)./dat.data(Ix,Iy,1);
                	Out.velY{k}(j) = dat.data(Ix,Iy,3)./dat.data(Ix,Iy,1);
                	Out.speed{k}(j) = sqrt(Out.velX{k}(j).^2 + Out.velY{k}(j).^2);
                	Out.phi{k}(j) = dat.data(Ix,Iy,4)./dat.data(Ix,Iy,1);
                	Out.erosion{k}(j) = dat.data(Ix,Iy,5);
                	Out.topog{k}(j) = dat.data(Ix,Iy,6);
		else
			Out.depth{k}(j) = 0;
			Out.velX{k}(j) = 0;
			Out.velY{k}(j) = 0;
			Out.speed{k}(j) = 0;
			Out.phi{k}(j) = 0;
			Out.erosion{k}(j) = 0;
			Out.topog{k}(j) = dat.data(Ix,Iy,6);
		end
                
            end
            Out.time(j) = dat.time;
            
        end
        
    end

    return
    
    function [mperdegLat,mperdegLon] = GetGeoLengths(Lat,Lon);
        
        a = 6378137.0; % principal radius of the WGSS84 spheroid (in meters)
        f = 298.257223563; % inverse flattening of the WGSS84 spheroid (in meters)
	
        e2 = (2.0 - 1.0/f)/f; % squared eccentricity.
	
        Latrad = Lat*pi/180.0;
        sinLat = sin(Latrad);
	
        b = 1.0-e2*sinLat*sinLat;
	
        M = a*(1.0-e2)/b/sqrt(b);
	
        N = a/sqrt(b);
	
        r = N*cos(Latrad);
	
        mperdegLon = r*pi/180.0;
	
        mperdegLat = M*pi/180.0;
        
        return
        
    end

end
