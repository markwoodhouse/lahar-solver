#ifndef SAVAGEHUTTEREQN_H
#define	SAVAGEHUTTEREQN_H
#include "Equation.h"

/* Extremely primative Savage-Hutter equation over shallow topography
 * 
 * No proper treatment of F<=mu*N or stopping material
 */

inline double sq(double v) {return v*v;}
inline double max(double a, double b) {return (a>b)?a:b;}
class SavageHutterEqn : public Equation
{
public:
	SavageHutterEqn() : Equation(4,3,0)
	{
		g = 9.81;
		muMin = 0.12;
		muMax = 125;
		cd = 0.03;
		
		SetPositivityPreserving(H);
		SetPositivityReconstruct(HU,H);
		SetPositivityReconstruct(HV,H);
	}	
	
	enum Variables {H=0,HU=1,HV=2,ER=3,B=4,DBDX=5,DBDY=6};
	
	void FillOutEquations(ConvectionFluxFunction &fx,								  
						  ConvectionFluxFunction &fy, 
						  SourceTermFunction &explst, 
						  ImplSourceTermFunction &implst, 
						  JacobianFunction &jacfn,
						  WaveSpeedFunction &xmaxws,
						  WaveSpeedFunction &ymaxws,
						  WaveSpeedFunction &xminws,
						  WaveSpeedFunction &yminws,
						  ExtrasFunction &ef)
	{
		fx=XConvectionFlux;
		fy=YConvectionFlux;
		explst=ExplicitSourceTerms;
		//implst=ImplicitSourceTerms;
		//jacfn=ImplicitJacobian;
		xmaxws=XMaxWaveSpeeds;
		xminws=XMinWaveSpeeds;
		ymaxws=YMaxWaveSpeeds;
		yminws=YMinWaveSpeeds;
		ef=ExtrasFn;
	}
	
private:
	static double g, muMin,muMax,cd;
	const static double heightThreshold=1e-4;
	inline static void ExtrasFn(double *dedt, const VectorArray2d *u)
	{
	}
	
	inline static void XConvectionFlux(double *xFlux, const double *u, const double *extras, const double *dedt)
	{
		xFlux[ER]=0;
        xFlux[H] = u[HU];
		if (u[H]<=heightThreshold)
		{
			xFlux[HU]=0;
			xFlux[HV]=0;
		}
		else
		{
			xFlux[HU] = u[HU] * u[HU] / u[H] + 0.5 * g * u[H] * u[H];
			xFlux[HV] = u[HU] * u[HV] / u[H];
		}
	}

	inline static void YConvectionFlux(double *yFlux, const double *u, const double *extras, const double *dedt)
	{
		yFlux[ER]=0;
        yFlux[H] = u[HV];
        if (u[H]<=heightThreshold)
		{
			yFlux[HU]=0;
			yFlux[HV]=0;
		}
		else
		{
			yFlux[HU] = u[HU] * u[HV] / u[H];
			yFlux[HV] = u[HV] * u[HV] / u[H] + 0.5 * g * u[H] * u[H];
		}
	}



    inline static double XMaxWaveSpeeds(const double *u, const double *extras, const double *dedt)
    {
        if (u[H]<=heightThreshold) return 0;
        return u[HU] / u[H] + sqrt(g*u[H]);
    }
    
    inline static double XMinWaveSpeeds(const double *u, const double *extras, const double *dedt)
    {
        if (u[H]<=heightThreshold) return 0;
        return u[HU] / u[H] - sqrt(g*u[H]);
    }

    inline static double YMaxWaveSpeeds(const double *u, const double *extras, const double *dedt)
    {
        if (u[H]<=heightThreshold) return 0;
        return u[HV] / u[H] + sqrt(g*u[H]);
    }
    
    inline static double YMinWaveSpeeds(const double *u, const double *extras, const double *dedt)
    {
        if (u[H]<=heightThreshold) return 0;
        return u[HV] / u[H] - sqrt(g*u[H]);
    }

	inline static void ExplicitSourceTerms(double *stvect, double *uvect, double *dudxvect, double *dudyvect, const double *extras, const double *dedt)
	{
		
        if (uvect[H]>2.0*heightThreshold)
		{
			stvect[HU] += -g*uvect[H]*uvect[DBDX];
			stvect[HV] += -g*uvect[H]*uvect[DBDY];
			double u, v, modu;
			u = uvect[HU]/uvect[H];
			v = uvect[HV]/uvect[H];
			modu=sqrt(u*u + v*v);
			
			// Completely ad-hoc entrainment
			double slope=sqrt(uvect[DBDX]*uvect[DBDX]+uvect[DBDY]*uvect[DBDY]);
			if (slope>0.53)
			{
				float hlim = uvect[H];
				if (hlim>1) hlim=1;
				if (hlim>0.05)
				{
					float alt=(uvect[B]-2000)/1000;
					if (alt>1) alt=1;
					if (alt<.05) alt=.05;
					float erosionAmount=(slope-0.53)*modu*modu*0.003*hlim*alt;
					stvect[H]+=erosionAmount;
					stvect[ER]-=erosionAmount;
				}
			}
			
			if (modu>0.01) // 1cm/sec
			{
				/// Friction force is at least coulomb with muMin, and at most coulomb with muMax, chezy if between these values.
				/// The muMax upper limit is for numerical stability only - stops singularity as h->0
				float coulombLowAngle = muMin*g*uvect[H];
				float coulombHighAngle = muMax*g*uvect[H];
				float chezy = cd*modu*modu;
				float friction=chezy;
				if (friction>coulombHighAngle) friction=coulombHighAngle;
				if (friction<coulombLowAngle) friction=coulombLowAngle;
				stvect[HU] += -friction*u/modu;
				stvect[HV] += -friction*v/modu;
			}
			else
			{
				uvect[HU]=0.0;
				uvect[HV]=0.0;	
			}
		} 
		if (uvect[H]<heightThreshold)
		{
			uvect[H]=heightThreshold*0.5;
			uvect[HU]=0.0;
			uvect[HV]=0.0;
		}
	}

/*	inline static void ImplicitSourceTerms(double *stvect, double *uvect)
	{
		double h=uvect[H];
		if (h>0)
		{
			double u=uvect[HU]/uvect[H];
			double v=uvect[HV]/uvect[H];
			double modUmu=sqrt((U-u)*(U-u)+v*v);
			stvect[HU]=-cD*(u-U)*modUmu;
			stvect[HV]=-cD*v*modUmu;
		}
	}
	
	inline static void ImplicitJacobian(double *uvect, double t, double *jac)
	{
//   d/dt (h,hu,hv) = f(h,hu,hv) = (-Cd) sqrt((U-(hu/h))^2 + (hv/h)^2)_k+1 (0,u-U,v)_k+1
//  d (hu) / dh = 
//
		// jacobian = dF(v)_i / dv_j (i = rows, j = cols)
		double h=uvect[H];
		if (h>0)
		{
			double u=uvect[HU]/h;
			double v=uvect[HV]/h;
			double umu=(U-u);
			double modu=sqrt(umu*umu+v*v);
			jac[H+4]  =  cD * (2*U*U*u - 4 *u*u*U - v*v*U + 2*u*u*u+2*u*v*v)/(h*modu);
			jac[HU+4] = -cD * (2*U*U-4*u*U+2*u*u+v*v)/(h*modu);
			jac[HV+4] =  cD * v * umu/(h*modu);
			jac[H+8]  =  cD * v * (U*U - 3*u*U + 2*(v*v+u*u))/(h*modu);
			jac[HU+8] =  cD * v * umu / (h * modu);
			jac[HV+8] = -cD * (umu*umu+2*v*v)/(h*modu);
		}
	}*/
};

double SavageHutterEqn::g;
double SavageHutterEqn::muMin;
double SavageHutterEqn::muMax;
double SavageHutterEqn::cd;

#endif	
