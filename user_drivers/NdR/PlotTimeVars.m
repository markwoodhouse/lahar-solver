

function PlotTimeVars%(fileroot,filerange)

%     n=numel(filerange);
%     flux=zeros(1,n);
%     maxh=zeros(1,n);
%     meanphi=zeros(1,n);
%     t=zeros(1,n);
%     for i=1:numel(filerange);      
%         clf
%         dat=LoadCNTSData(sprintf(fileroot,filerange(i)));
%         flux(i)=sum(dat.data(:,400,3))*(dat.xGrid(2)-dat.xGrid(1));
%         maxh(i)=max(dat.data(:,400,1));
%         meanphi(i)=sum(dat.data(:,400,4))./sum(dat.data(:,400,1));
%         t(i)=dat.time;
%     end
%     save tvs.mat flux maxh meanphi t
%     
%     
     set(gcf,'PaperUnits','centimeters');
        myfiguresize = [0 0 12 6];
        set(gcf, 'PaperPosition', myfiguresize);

        unis = get(gcf,'units');
        ppos = get(gcf,'paperposition');
        set(gcf,'units',get(gcf,'paperunits'));
        pos = get(gcf,'position');
        pos(3:4) = ppos(3:4);
        set(gcf,'position',pos);
        set(gcf,'units',unis);
    S=load('tvs.mat');
    
    S.meanphi(S.maxh<0.01)=1;
    plot(S.t,0.001*S.flux,S.t,1-S.meanphi,S.t,S.maxh);
    legend('Flux ($1000\,\text{m}^3\,\text{s}^{-1}$)','Scaled particle concentration','Stage (m)','Location',[.5 .5 .5 .5]);
    h = findobj('type', 'axes');  % Find all sets of axes

	legend boxoff 


    axis([200 600 0 3.5]);
    xlabel('Time (seconds)');
    PrintFig('timevars','font-minion');

end