function PlotFlowTopog(fileroot,filerange,GetTopog,varargin)

options = struct('stations',[], ...
    'markersize',1, ...
    'marker','.', ...
    'markercolour','k', ...
    'savefig','off', ...
    'outdir','outputs',...
    'maxheight',10, ...
    'contours',[2000 2500 3000 3500 4000 4500 5000 5500]);

% read the acceptable names
optionNames = fieldnames(options);

% count arguments
nArgs = length(varargin);
if round(nArgs/2)~=nArgs/2
    error('In PlotFlowTopog if optional inputs are used they must appear as propertyName/propertyValue pairs');
end

for pair = reshape(varargin,2,[])
    inpName = lower(pair{1});
    if any(strmatch(inpName,optionNames))
        options.(inpName) = pair{2};
    else
        error('%s is not a recognized parameter name',inpName)
    end
end

switch options.savefig
    case 'on'
        DirFlag = exist(options.outdir,'dir');
        if DirFlag == 0
            disp(['Making directory ', options.outdir,' for saving figures.']);
            mkdirFlag = mkdir(sprintf('%s',options.outdir));
            if mkdirFlag==0
                error(['Could not make directory ',options.outdir]);
            end
        else
            disp(['Saving figues into existing directory ', options.outdir,'.  Existing files in this directory might be replaced.']);
        end
end

%
    function c=GetHeightCol(h)
        hts=options.contours;
        cols=[173 215 170; 214 232 184; 249 247 199; 250 245 190; 250 203 183; 217 187 202; 242 255 231; 255 255 255]/255;
        
        if numel(hts)>numel(cols(:,1))
            jj=0:1:numel(cols(:,1))-1;
            kk=(0:1:numel(hts)-1).*(numel(cols(:,1))-1)./(numel(hts)-1);
            
            ccR = interp1(jj,cols(:,1),kk)';
            ccG = interp1(jj,cols(:,2),kk)';
            ccB = interp1(jj,cols(:,3),kk)';
            
            cols = [ccR, ccG, ccB];
        end
        
        c=interp1(hts,cols,h);
    end
    function c=GetThicknessCol(h)
        hts=linspace(0,options.maxheight,100);
        cols=jet(100);
        c=interp1(hts,cols,h);
    end

    function DEM=MakeTopog(fileroot,filerange)
        
        dat=LoadCNTSData(sprintf(fileroot,1));
        img=zeros([dat(1).xPoints, dat(1).yPoints,3]);
        DEM.alt=dat(1).data(:,:,6);
        shading=dat(1).data(:,:,7)-dat(1).data(:,:,8);
        for x=1:dat(1).xPoints
            for y=1:dat(1).yPoints
                img(x,y,:)=GetHeightCol(DEM.alt(x,y));
                shade=tanh(0.1*shading(x,y));
                if (shade>0)
                    img(x,y,:)=img(x,y,:)+(1-img(x,y,:))*shade;
                else
                    img(x,y,:)=(0.6*shade+1)*img(x,y,:);
                end
            end
        end
        
        DEM.topogim=img;
        
        DEM.xPoints = dat.xPoints;
        DEM.yPoints = dat.yPoints;
        DEM.xMin = dat.xMin;
        DEM.yMin = dat.yMin;
        DEM.xSize = dat.xSize;
        DEM.ySize = dat.ySize;
        
        save topog.mat DEM
        
        return
    end

switch GetTopog
    case 'on'
        DEM = MakeTopog(fileroot,filerange);
    otherwise
        load('topog.mat');
end

[tx,ty]=size(DEM.alt);

for i=1:numel(filerange);
    clf
    dat=LoadCNTSData(sprintf(fileroot,filerange(i)));
    xofs=(tx-dat.xPoints)/2;
    yofs=(ty-dat.yPoints)/2;
    set(gcf,'PaperUnits','centimeters');
    %myfiguresize = [0 0 12 12];
    myfiguresize = [0 0 20 20];
    set(gcf, 'PaperPosition', myfiguresize);
    
    unis = get(gcf,'units');
    ppos = get(gcf,'paperposition');
    set(gcf,'units',get(gcf,'paperunits'));
    pos = get(gcf,'position');
    pos(3:4) = ppos(3:4);
    set(gcf,'position',pos);
    set(gcf,'units',unis);
    
    axes('position',[.1 .1 .75 0.75]);
    img=DEM.topogim;
    for x=1:dat.xPoints
        for y=1:dat.yPoints
            h=dat.data(x,y,1);
            if (h>0.1)
                img(xofs+x,yofs+y,:)=GetThicknessCol(h);
            elseif (h>0.01)
                fc=GetThicknessCol(h);
                img(xofs+x,yofs+y,1)=((h-0.01)/0.09)*fc(1)+(1-((h-0.01)/0.09))*img(xofs+x,yofs+y,1);
                img(xofs+x,yofs+y,2)=((h-0.01)/0.09)*fc(2)+(1-((h-0.01)/0.09))*img(xofs+x,yofs+y,2);
                img(xofs+x,yofs+y,3)=((h-0.01)/0.09)*fc(3)+(1-((h-0.01)/0.09))*img(xofs+x,yofs+y,3);
            end
        end
    end
    xx = linspace(dat.xMin,dat.xMin+dat.xSize,dat.xPoints);%linspace(0.5,0.95,dat.xPoints).*dat.xSize+dat.xMin;
    yy = linspace(dat.yMin,dat.yMin+dat.ySize,dat.yPoints);%linspace(0.5,0.95,dat.yPoints).*dat.ySize+dat.yMin;
    %image(-4995:10:4995,-4995:10:4995,permute(img,[2 1 3]));
    image(xx,yy,permute(img,[2 1 3]));
    hold on
    %contour(-4995:10:4995,-4995:10:4995,S.alt',1000:200:5000,'Color',[.6 .6 .6]);
    xdem = linspace(DEM.xMin,DEM.xMin+DEM.xSize,DEM.xPoints);
    ydem = linspace(DEM.yMin,DEM.yMin+DEM.ySize,DEM.yPoints);
    contour(xdem,ydem,DEM.alt',2000:50:5500,'Color',[.6 .6 .6]);
    set(gca,'YDir','normal');
    %axis([-2000 2000 -3000 4000]);
    axis equal
    axis([dat.xMin dat.xMin+dat.xSize dat.yMin dat.yMin+dat.ySize])
    pbaspect([4 7 1]);
    xlabel('$x$ (m)','Interpreter','none');
    ylabel('$y$ (m)','Rotation',0,'Interpreter','none');
    
    if ~isempty(options.stations)
        for k=1:numel(options.stations(:,1))
            plot(options.stations(:,1),options.stations(:,2), ...
                'LineStyle','none', ...
                'MarkerSize',options.markersize,'Marker',options.marker,...
                'MarkerFaceColor',options.markercolour,'MarkerEdgeColor',options.markercolour)
        end
    end
    
    xax = get(gca,'Xlim');
    yax = get(gca,'Ylim');
    
    speed=sqrt(dat.data(:,:,2).^2 + dat.data(:,:,3).^2)'./dat.data(:,:,1)';
    speed(dat.data(:,:,1)'<0.02)=0;
    maxspeed=max(max(speed));
    
    validheights=dat.data(:,:,1)';
    validheights(dat.data(:,:,1)'<0.02)=[];
    %validheights(dat.data(:,:,1)'<0.00005)=[];
    %validheights(speed<1e-2)=0;
    vol=sum(sum(validheights));
    vol=vol*(dat(1).xGrid(2)-dat(1).xGrid(1))*(dat(1).yGrid(2)-dat(1).yGrid(1));
    
    if dat.xSize>dat.ySize
        text(xax(1)+0.1*(xax(2)-xax(1)),yax(2)*2.,sprintf('Time: $%0.1f \\; \\mathrm{s}$',dat.time),'Interpreter','none','FontSize',14);
        text(xax(1)+0.1*(xax(2)-xax(1)),yax(2)*1.75,sprintf('Flow volume: $%0.1f \\times 10^3 \\; \\mathrm{m}^3$',vol/1000),'Interpreter', ...
            'none','FontSize',14);
        text(xax(1)+0.1*(xax(2)-xax(1)),yax(2)*1.5,sprintf('Max. speed: $%0.1f \\; \\mathrm{m/s}$',maxspeed),'Interpreter','none','FontSize',14)
        
        axes('position',[.5 .75 .35 .02]);
        colormap(jet);
        imagesc(linspace(0,options.maxheight),1,linspace(0,1));
        axis([0 options.maxheight .5 1.0]);
        set(gca,'YTick',[]);
        xticks = linspace(0,options.maxheight,3);
        set(gca,'XTick',xticks,'XAxisLocation','top');
        %xlabel('Flow depth (m)','Interpreter','latex','FontSize',14);
        xlabel('Flow depth (m)','FontSize',14)
        drawnow
    elseif dat.xSize<dat.ySize
        text(xax(2)*1.1,yax(2)*0.9,sprintf('Time: $$%0.1f \\; \\mathrm{s}$$',dat.time),'Interpreter','latex');
        text(xax(2)*1.1,yax(2)*0.85,sprintf('Flow volume: $%0.1f \\times 10^3 \\; \\mathrm{m}^3$',vol/1000),'Interpreter','latex');
        text(xax(2)*1.1,yax(2)*0.8,sprintf('Max. speed: $$%0.1f \\; \\mathrm{m/s}$$',maxspeed),'Interpreter','latex')
    else
        text(xax(2)*1.1,yax(2)*0.9,sprintf('Time: $$%0.1f \\; \\mathrm{s}$$',dat.time),'Interpreter','latex');
        text(xax(2)*1.1,yax(2)*0.85,sprintf('Flow volume: $%0.1f \\times 10^3 \\; \\mathrm{m}^3$',vol/1000),'Interpreter','latex');
        text(xax(2)*1.1,yax(2)*0.8,sprintf('Max. speed: $$%0.1f \\; \\mathrm{m/s}$$',maxspeed),'Interpreter','latex');
        
        axes('position',[.6 .65 .35 .05]);
        colormap(jet);
        imagesc(linspace(0,options.maxheight),1,linspace(0,1));
        axis([0 10 .5 1.5]);
        set(gca,'YTick',[]);
        set(gca,'XTick',[0 5 10]);
        
        xlabel('Flow depth (m)','Interpreter','latex');
        drawnow
    end
    
    %text(2200,2800,['Flow volume: ',num2str(vol/1000,'%0.1f'),'$\times 10^3 \, \mathrm{m}^3$'],'Interpreter','latex');
    
    switch options.savefig
        case 'on'
            % Export the figure
            PrintFig(sprintf('%s/out%06d',options.outdir,filerange(i)),'font-minion');
            %export_fig(sprintf('outputs/out%06d',filerange(i)),'-png','-r200','zbuffer')
    end
end

return

end
