#include "KPSolver.h"
#include <cmath>
#include <fenv.h>
#include "SegregatingEqn.h"
#include "timesteppers/RK2TimeStepper.h"
#include "dem.h"
#include <fstream>
#include <string>
#include <cstring>
using namespace std;

int set_Lat=0, set_Lon=0;
int set_Tend=0, set_Nout=0;
int set_dtmax=0;
int set_nXPoints=0, set_nYPoints=0, set_regionSizeX=0, set_regionSizeY=0;
int set_capHeight=0, set_capRadius=0, set_capVol=0;
int set_capX=0, set_capY=0, set_phi0=0;
int set_capLat=0, set_capLon=0;
int set_ChezyCo=0;
int set_OutDir=0;
int set_SourceFlux=0, set_SourceTime=0;

static const string basePath="./";

SimpleImage<float> heightData;

const string OutDirDefault="results";
char *OutDir;

// Latitude and Longitude of the centre of the domain (e.g. Lat/Lon of the volcano summit, or a point of interest)
double Lat = 4.892; // Latitude of centre of domain, +/- for N/S of equator.
double Lon = -75.324; // Longitude of centre of domain, +/- for E/W of Greenwich meridian.

// Time of simulation and number of output files
double Tend = 100; // in seconds, end simulation at t=Tend;
int Nout = 100; // number of output files
double dtmax = Tend; // in seconds, maximum step-size;

// X and Y simulation resolution
int nXPoints=400, nYPoints=400; // int nXPoints=2500, nYPoints=2500;

// Region this corresponds to in DEM, in metres
double regionSizeX=3000, regionSizeY = regionSizeX*nYPoints/double(nXPoints); // in metres // double regionSizeX=100000, regionSizeY = regionSizeX*nYPoints/double(nXPoints); // in metres

// Metres per simulation grid cell
double xGridCellSize; // = regionSizeX / double(nXPoints); // in metres
double yGridCellSize; // = regionSizeY / double(nYPoints); // in metres

double cfl=0.25;
double maxdt=1;

double capHeight = 2.0, capRadius=10.0; //default cap height and cap radius
double capVol = 0; // by default capVol is not needed
double capX = 0.0; // in metres, relative to origin at the centre of numerical domain
double capY = 0.0; // in metres, relative to origin at the centre of numerical domain
double capLat = Lat; // by default the cap is in the centre of the domain
double capLon = Lon;
double phi0 = 0.5;  // default value of the initial size distribution

double ChezyCo = 0.03; // default Chezy coefficient

int nSourceFlux, nSourceTime;
double * SourceFluxArray;
double * SourceTimeArray;

// Initial conditions are a parabolic cap release
void InitialConditions(double *u, double x, double y)
{
    u[SegregatingEqn::H] = std::max(0.00005,capHeight*(1-((x-capX)*(x-capX)+(y-capY)*(y-capY))/(capRadius*capRadius))); //paraboloid cap I.C
    u[SegregatingEqn::HU] = 0.0;
    u[SegregatingEqn::HV] = 0.0;
    u[SegregatingEqn::HPHI] = u[SegregatingEqn::H]*phi0;
    u[SegregatingEqn::Q] = 0.0;
    if ( (x-capX)*(x-capX)+(y-capY)*(y-capY)<capRadius*capRadius ) { u[SegregatingEqn::Q] = 1.0; }
}

// Calculate length of degree of latitude and longitude
struct GeoLengths { double mperdegLat; double mperdegLon; };

GeoLengths CalculateGeoLength(double Lat)
{

	GeoLengths retval;
	const double a = 6378137.; // principal radius of the WGSS84 spheroid (in meters)
	const double f = 298.257223563; // inverse flattening of the WGSS84 spheroid (in meters)
	
	double e2 = (2. - 1./f)/f; // squared eccentricity.
	
	double Latrad = Lat*M_PI/double(180);
	double sinLat = sin(Latrad);
	
	double b = 1.-e2*sinLat*sinLat;
	
	double M = a*(1.-e2)/b/sqrt(b);
	
	double N = a/sqrt(b);
	
	double r = N*cos(Latrad);
	
	retval.mperdegLon = r*M_PI/double(180);
	
	retval.mperdegLat = M*M_PI/double(180);
	
	return retval;
}

int countSubstring(const string& stringIn, const string& sub)
{
	if (sub.length() == 0) { return 0; }
	int count = 0;
	for (size_t offset = stringIn.find(sub);
		offset != std::string::npos;
		offset = stringIn.find(sub,offset+sub.length())
	    )
	{
		++count;
	}
	return count;
}


int main(int argc, char *argv[])
{
	cerr << endl;
	cerr << "Starting lahar simulation." << endl << endl;

	cerr << "Reading input settings from Inputs.txt." << endl << endl;

	string buff;
	if (argc==1) return 1;
	ifstream infile(argv[1]);
	stringstream ss;
	string s1;
	string s11;
	string stmp;
	string SourceFluxStr, SourceTimeStr;
	string SourceFluxValStr, SourceTimeValStr;
	int jj;
	
	while( getline(infile, buff) ) {
	
	  if (buff.empty()!=1) // ignore blank line
		{
			buff.resize (buff.size());
			ss.clear();
			ss.str("");
			s1.clear();
			s11.clear();
	
			ss << buff;
			ss >> s1;
			
			s11 = s1.at(0);
		  
			if ( s11.compare("%")!=0 && s11.compare(" ")!=0 )
			{
				if (s1.compare("nXPoints") == 0) { ss >> nXPoints; set_nXPoints = 1; }
				else if (s1.compare("nYPoints") == 0) { ss >> nYPoints; set_nXPoints = 1; }
				else if (s1.compare("regionSizeX") == 0) { ss >> regionSizeX; set_regionSizeX = 1; }
				else if (s1.compare("regionSizeY") == 0) { ss >> regionSizeY; set_regionSizeY = 1; }
				else if (s1.compare("capHeight") == 0) { ss >> capHeight; set_capHeight = 1; }
				else if (s1.compare("capRadius") == 0) { ss >> capRadius; set_capRadius = 1; }
				else if (s1.compare("capVolume") == 0) { ss >> capVol; set_capVol = 1; }
				else if (s1.compare("capX") == 0) { ss >> capX; set_capX = 1; }
  			else if (s1.compare("capY") == 0) { ss >> capY; set_capY = 1; }
	  		else if (s1.compare("capLat") == 0) { ss >> capLat; set_capLat = 1; }
	  		else if (s1.compare("capLon") == 0) { ss >> capLon; set_capLon = 1; }
	  		else if (s1.compare("phi0") == 0) { ss >> phi0; set_phi0 = 1; }
	  		else if (s1.compare("Lat") == 0) { ss >> Lat; set_Lat = 1; }
	  		else if (s1.compare("Lon") == 0) { ss >> Lon; set_Lon = 1; }
	  		else if (s1.compare("ChezyCoeff") == 0) { ss >> ChezyCo; set_ChezyCo = 1; }
	  		else if (s1.compare("Tend") == 0) { ss >> Tend;	set_Tend = 1; }
	  		else if (s1.compare("Nout") == 0) { ss >> Nout;	set_Nout = 1; }
	  		else if (s1.compare("dtMax") == 0) { ss >> dtmax;	set_dtmax = 1; }
	  		else if (s1.compare("ResultsDir") == 0) {
	  			ss >> stmp;
	  			set_OutDir = 1;
	  			OutDir = strdup(stmp.c_str());
	  		}
	  		else if (s1.compare("SourceFlux") == 0) {
	  			ss >> SourceFluxStr;
	  			nSourceFlux = countSubstring(SourceFluxStr,",");
	  			nSourceFlux += 1;
	  			SourceFluxArray= new double[nSourceFlux];
	  			int StrEnd;
	  			string delim=",";
	  			for (jj=0; jj<nSourceFlux; jj++)
	  			{
	  				StrEnd=SourceFluxStr.find(delim);
	  				SourceFluxValStr = SourceFluxStr.substr(0,StrEnd);
	  				SourceFluxStr.erase(0,StrEnd+delim.length());
	  				SourceFluxArray[jj] = stod(SourceFluxValStr);
	  			}
	  			set_SourceFlux = 1;
  			}
  			else if (s1.compare("SourceTime") == 0) {
  				ss >> SourceTimeStr;
  				nSourceTime = countSubstring(SourceTimeStr,",");
   				nSourceTime += 1;
  				SourceTimeArray= new double[nSourceTime];
  				int StrEnd;
  				string delim=",";
  				for (jj=0; jj<nSourceTime; jj++)
  				{
  					StrEnd=SourceTimeStr.find(delim);
  					SourceTimeValStr = SourceTimeStr.substr(0,StrEnd);
  					SourceTimeStr.erase(0,StrEnd+delim.length());
  					SourceTimeArray[jj] = stod(SourceTimeValStr);
  				}
  				set_SourceTime = 1;
  			}
  			else { 
  				cerr << "Input " << s1 << " not recognized." << endl; 
  				ss >> stmp;
  			}
   		}
	  }
	}
	
	if (set_capLat==1 && set_capLon==0) {cerr << "ERROR!!  If capLat is given in the input file, then capLon must also be set." << endl << "Quitting" << endl << endl; return 1;}
	if (set_capLat==0 && set_capLon==1) {cerr << "ERROR!!  If capLon is given in the input file, then capLat must also be set." << endl << "Quitting" << endl << endl; return 1;}
	if (set_capX==1 && set_capY==0) {cerr << "ERROR!!  If capX is given in the input file, then capY must also be set." << endl << "Quitting" << endl << endl; return 1;}
	if (set_capX==0 && set_capY==1) {cerr << "ERROR!!  If capY is given in the input file, then capX must also be set." << endl << "Quitting" << endl << endl; return 1;}
	if (set_capX==1 && set_capLat==1) {cerr << "ERROR!!  If capX and capY are given in the input file, then capLat and capLon must not be set." << endl << "Quitting" << endl << endl; return 1;}
	
	if (set_capLat==1)
	{
		GeoLengths LatLonLengths = CalculateGeoLength(Lat);
		capX = (capLon-Lon)*LatLonLengths.mperdegLon;
		capY = (capLat-Lat)*LatLonLengths.mperdegLat;
		
		cerr << "capX = " << capX << endl;
		cerr << "capY = " << capY << endl << endl;
		
		if (abs(capX)>0.5*regionSizeX) {cerr << "ERROR!!  The capLat value in the input file puts the source outside the domain." << endl << "Quitting" << endl << endl; return 1;}
		if (abs(capY)>0.5*regionSizeY) {cerr << "ERROR!!  The capLon value in the input file puts the source outside the domain." << endl << "Quitting" << endl << endl; return 1;}
	}

	if (set_capHeight==1 && set_capRadius==1 && set_capVol==1) {cerr << "ERROR!!  You have set all of capHeight, capRadius and capVolume.  Only two of these three parameters can be specified." << endl << "Quitting" << endl << endl; return 1;}
	if (set_capHeight+set_capRadius+set_capVol==1) {cerr << "ERROR!!  Only one of capHeight, capRadius and capVolume has been set.  Need two of these three parameters to be set." << endl << "Quitting" << endl << endl; return 1;}

	if (set_capHeight==1 && set_capRadius==1) { capVol = 0.5*M_PI*capHeight*capRadius*capRadius; }
	if (set_capHeight==1 && set_capVol==1) { capRadius = sqrt(2.0/M_PI*capVol/capHeight); }
	if (set_capRadius==1 && set_capVol==1) { capHeight = 2.0/M_PI*capVol/capRadius/capRadius; }

	if (set_ChezyCo==0) {
		cerr << "WARNING!!  ChezyCoeff was not set in Inputs.txt." << endl;
		cerr << "           Using default value of " << ChezyCo << "." << endl << endl;
	}
	else {
		cerr << "ChezyCoeff set to " << ChezyCo << " in Inputs.txt." << endl << endl;
	}

	if (set_OutDir==0) {
		OutDir = strdup(OutDirDefault.c_str());
		cerr << "WARNING!!  ResultsDir was not set in Inputs.txt." << endl;
		cerr << "           Using default directory " << OutDir << "." << endl << endl;
	}
	else {
		cerr << "Results will be in the folder " << OutDir << endl << endl;
	}
	
	if (set_SourceFlux==0 && set_SourceTime==0) {
		nSourceFlux = 1;
		nSourceTime = 1;
		SourceFluxArray= new double[1];
		SourceTimeArray= new double[1];
		SourceFluxArray[0]=0.0;
		SourceTimeArray[0]=0.0;	
	}
	else if (set_SourceFlux==1 && set_SourceTime==0) { cerr << "ERROR!!  In Inputs.txt file: SourceFlux is set but SourceTime is not set." << endl << "Quitting" << endl << endl; return 1; }
	else if (set_SourceFlux==0 && set_SourceTime==1) { cerr << "ERROR!!  In Inputs.txt file: SourceTime is set but SourceFlux is not set." << endl << "Quitting" << endl << endl; return 1; }
	else {
		if (nSourceFlux != nSourceTime) { cerr << "ERROR!!  In Inputs.txt file: The number of SourceTime points is not the same as the number of SourceFlux points." << endl << "Quitting" << endl << endl; return 1; }
	}
	
	cerr << "nXPoints = " << nXPoints << endl;
	cerr << "nYPoints = " << nYPoints << endl;
	
	cerr << "regionSizeX = " << regionSizeX << endl;
	cerr << "regionSizeY = " << regionSizeY << endl;
	
	xGridCellSize = regionSizeX / double(nXPoints); // in metres
	yGridCellSize = regionSizeY / double(nYPoints);
	
	if (xGridCellSize != yGridCellSize) { cerr << "WARNING!!  Non-square cells...." << endl;}

	cerr << "Completed reading of Inputs.txt.  Continuing to solver." << endl << endl;
	
	// Load the DEM required into the 'heightData' variable
	// DEM is centred about lat/long (-1.466944, -78.441667) (Tungurahua).
	// For space reasons, only relevant files of ~100GB SRTM-based DEM dataset are included in the zip
	cerr << "Outputting " << nXPoints << "x" << nYPoints << " grid, covering region " << regionSizeX << "x" << regionSizeY << " m in length."<<endl;
	//heightData = GetHeightData(basePath, nXPoints, nYPoints, {-1.427874, -78.432044}, 0.001*xGridCellSize, 0.001*yGridCellSize);
	heightData = GetHeightData(basePath, nXPoints, nYPoints, {Lat, Lon}, 0.001*xGridCellSize, 0.001*yGridCellSize);

	// Make a new equation of type 'SegregatingEqn'
	SegregatingEqn eqn;

	// Make a new solver, which solves this equation with a specified resolution and timestepper
	KPSolver solver(nXPoints, nYPoints, &eqn, new RK2TimeStepper());
	eqn.SetSolverPtr(&solver);

	// Set CFL of timestepper 
	dynamic_cast<RK2TimeStepper*>(solver.TimeStepperPtr())->SetCFL(cfl);
	dynamic_cast<RK2TimeStepper*>(solver.TimeStepperPtr())->SetMaxdt(dtmax);

	// Turn on outputting of simulation time to command line
	solver.TimeStepperPtr()->RealtimeOutput(true);

	// Set limiter to MinMod1
	solver.SetLimiter(KPSolver::MinMod1);

	// Set domain over which to solve equations. We solve dimensional equations (heresy!), with SI units
	//solver.SetDomain(-0.5*regionSizeX,-0.5*regionSizeY,regionSizeX,regionSizeY);
	solver.SetDomain(-0.5*regionSizeX,-0.5*regionSizeY,regionSizeX,regionSizeY);

	// Set the initial conditions using the function above
	solver.SetInitialConditions(InitialConditions,1);
	
	// Manually loop over all the points and set the topography 'initial condition' using the heightData variable loaded above
	// Note that we set both terrain height (B) but also derivatives (from finite-differencing)
	for (int i=0; i<nXPoints; i++) for (int j=0; j<nYPoints; j++)
	{
		solver.Solution()(SegregatingEqn::B, i, j) = heightData(i, j);
		if (i>0 && j>0 && i<nXPoints-1 && j<nYPoints-1)
		{
			solver.Solution()(SegregatingEqn::DBDX, i, j) = (heightData(i+1, j)-heightData(i-1, j))/(2.0*xGridCellSize);
			solver.Solution()(SegregatingEqn::DBDY, i, j) = (heightData(i, j+1)-heightData(i, j-1))/(2.0*yGridCellSize);
		}
	}

	// Set boundary conditions to be most appropriate for flows which leave the simulation domain.
	// This stops the worst unphysical behaviour when flows leave the domain, but having a domain big enough
	// to contain the entire flow is distinctly preferable. 
	solver.SetExtrapolatedBoundaryConditions();	
	
	// Run the simulation up to t=Tend (seconds), outputting Nout intermediate files
	solver.Run(OutDir,Tend,Nout); // solver.Run(4000.0,500);

	free(OutDir);

  return 0;
}
