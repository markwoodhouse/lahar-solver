#ifndef SEGREGATINGEQN_H
#define	SEGREGATINGEQN_H
#include "Equation.h"
#include <algorithm>
using namespace std;
/*     Example equations for a segregating two-component flow with entrainment and mixed chezy/coulomb friction
 *     Equations should be something along the lines of:
 *     
 *     dh/dt + d/dx (hu) + d/dy (hv) = entrainment
 *     d/dt (hu) + d/dx (hu^2 + g h^2/2) + d/dy (huv) = -g h db/dx + friction
 *     d/dt (hv) + d/dx (huv) + d/dy (hv^2 + g h^2/2) = -g h db/dy + friction
 *     d/dt (h phi) + d/dx [(h u phi)(alpha + (1-alpha)phi)] = entrainmentPhi * entrainment
 *
 *     but need checking...
 */

class KPSolver; // forward declaration of solver class

inline double sq(double v) {return v*v;}
inline double max(double a, double b) {return (a>b)?a:b;}

extern double ChezyCo;

extern int nSourceFlux, nSourceTime;
extern double * SourceFluxArray;
extern double * SourceTimeArray;

class SegregatingEqn : public Equation
{
public:
	SegregatingEqn() : Equation(5,4,0)
	{
		g = 9.81;
		muMin = 0.12;
		muMax = 125;
		//cd = 0.03;
		alpha=0.5;

		solverPtr = NULL;
		
		SetPositivityPreserving(H);
		SetPositivityPreserving(HPHI);
		SetPositivityReconstruct(HU,H);
		SetPositivityReconstruct(HV,H);
	}	
	
	enum Variables {H=0,HU=1,HV=2,HPHI=3,ER=4,B=5,DBDX=6,DBDY=7,Q=8};
	
	void FillOutEquations(ConvectionFluxFunction &fx,								  
						  ConvectionFluxFunction &fy, 
						  SourceTermFunction &explst, 
						  ImplSourceTermFunction &implst, 
						  JacobianFunction &jacfn,
						  WaveSpeedFunction &xmaxws,
						  WaveSpeedFunction &ymaxws,
						  WaveSpeedFunction &xminws,
						  WaveSpeedFunction &yminws,
						  ExtrasFunction &ef)
	{
		fx=XConvectionFlux;
		fy=YConvectionFlux;
		explst=ExplicitSourceTerms;
		//implst=ImplicitSourceTerms;
		//jacfn=ImplicitJacobian;
		xmaxws=XMaxWaveSpeeds;
		xminws=XMinWaveSpeeds;
		ymaxws=YMaxWaveSpeeds;
		yminws=YMinWaveSpeeds;
		ef=ExtrasFn;
	}
	
	void SetSolverPtr(KPSolver *sptr)
	{
		solverPtr = sptr;
	}

	KPSolver *GetSolverPtr()
	{
		return solverPtr;
	}
private:
	static KPSolver *solverPtr;
	static double g, muMin,muMax,cd, alpha;
	constexpr const static double heightThreshold=1e-4;
	inline static void ExtrasFn(double *dedt, const VectorArray2d *u)
	{
	}
	
	inline static void XConvectionFlux(double *xFlux, const double *u, const double *extras, const double *dedt)
	{
        xFlux[H] = u[HU];
		if (u[H]<=heightThreshold)
		{
			xFlux[HU]=0;
			xFlux[HV]=0;
			xFlux[HPHI]=0;
		}
		else
		{
			xFlux[HU] = u[HU] * u[HU] / u[H] + 0.5 * g * u[H] * u[H];
			xFlux[HV] = u[HU] * u[HV] / u[H];
			xFlux[HPHI]=0;
			xFlux[HPHI] = (u[HU]*u[HPHI]/u[H]) * (alpha + (1-alpha)*(u[HPHI]/u[H]));
		}
	}

	inline static void YConvectionFlux(double *yFlux, const double *u, const double *extras, const double *dedt)
	{
        yFlux[H] = u[HV];
        if (u[H]<=heightThreshold)
		{
			yFlux[HU]=0;
			yFlux[HV]=0;
			yFlux[HPHI]=0;
		}
		else
		{
			yFlux[HU] = u[HU] * u[HV] / u[H];
			yFlux[HV] = u[HV] * u[HV] / u[H] + 0.5 * g * u[H] * u[H];
			yFlux[HPHI] = (u[HV]*u[HPHI]/u[H]) * (alpha + (1-alpha)*(u[HPHI]/u[H]));
		}
	}



    inline static double XMaxWaveSpeeds(const double *u, const double *extras, const double *dedt)
    {
        if (u[H]<=heightThreshold) return 0;
        return max(u[HU] / u[H] + sqrt(g*u[H]),(u[HU]/u[H])*(alpha+(1-alpha)*(u[HPHI]/u[H])));
    }
    
    inline static double XMinWaveSpeeds(const double *u, const double *extras, const double *dedt)
    {
        if (u[H]<=heightThreshold) return 0;
        return min(u[HU] / u[H] - sqrt(g*u[H]),(u[HU]/u[H])*(alpha+(1-alpha)*(u[HPHI]/u[H])));
    }

    inline static double YMaxWaveSpeeds(const double *u, const double *extras, const double *dedt)
    {
        if (u[H]<=heightThreshold) return 0;
        return max(u[HV] / u[H] + sqrt(g*u[H]),(u[HU]/u[H])*(alpha+(1-alpha)*(u[HPHI]/u[H])));
    }
    
    inline static double YMinWaveSpeeds(const double *u, const double *extras, const double *dedt)
    {
        if (u[H]<=heightThreshold) return 0;
        return min(u[HV] / u[H] - sqrt(g*u[H]),(u[HU]/u[H])*(alpha+(1-alpha)*(u[HPHI]/u[H])));
    }

	inline static void ExplicitSourceTerms(double *stvect, double *uvect, double *dudxvect, double *dudyvect, const double *extras, const double *dedt)
	{
		// Get the current time from the solver

		{
			double Qf;
			double t = solverPtr->Time();
			double Qt;
			int j;
			
			if (nSourceFlux == 1)
			{
				if (t<SourceTimeArray[0]) { Qt = 0.0; }
				else { Qt = SourceFluxArray[0];	}
			}
			else
			{
				if (t<SourceTimeArray[0]) { Qt = 0.0; }
				else if (t==SourceTimeArray[0]) { Qt = SourceFluxArray[0]; }
				else if (t>=SourceTimeArray[nSourceFlux-1]) { Qt = SourceFluxArray[nSourceFlux-1]; }
				else
				{
					for ( j=1; j<nSourceFlux; j++ )
					{
						if (t>SourceTimeArray[j-1] && t<SourceTimeArray[j]) {
							Qt = SourceFluxArray[j-1] + (SourceFluxArray[j]-SourceFluxArray[j-1])*(t-SourceTimeArray[j-1])/(SourceTimeArray[j]-SourceTimeArray[j-1]);
						}
					}
				}
			}
			
			Qf = uvect[Q]*Qt;
			stvect[H] += Qf;
		}

        if (uvect[H]>2.0*heightThreshold)
		{
			stvect[HU] += -g*uvect[H]*uvect[DBDX];
			stvect[HV] += -g*uvect[H]*uvect[DBDY];
			double u, v, modu;
			u = uvect[HU]/uvect[H];
			v = uvect[HV]/uvect[H];
			modu=sqrt(u*u + v*v);
			
			// Completely ad-hoc entrainment
			double slope=sqrt(uvect[DBDX]*uvect[DBDX]+uvect[DBDY]*uvect[DBDY]);
			if (slope>0.53) // only entrain if the slope is "steep"
			{
				float hlim = uvect[H];
				if (hlim>1) hlim=1;
				if (hlim>0.05) // only entrain if flow is >5cm deep
				{
					float entrainmentPhi=0.5; // entrained material has phi=0.5
					float alt=(uvect[B]-2000)/1000; // Get the terrain altitude and scale it...
					if (alt>1) alt=1; // ...and clamp it between 0.05 and 1
					if (alt<.05) alt=.05;

					// Make up some erosion function,
					float erosionAmount=(slope-0.53)*modu*modu*0.003*hlim*alt;
					stvect[H]+=erosionAmount; // increase flow height by this amount
					stvect[ER]-=erosionAmount; // decrease record of how much material eroded by this amount (this does not feed back on the shape of the topography used to calculate the momentum terms etc.)
					stvect[HPHI]+=entrainmentPhi*erosionAmount; // increase the amount of small particles by the erosion amount * the phi of the eroded material
				}
			}
			
			if (modu>0.01) // 1cm/sec
			{
				/// Friction force is:
				///  -- at least coulomb with mu = muMin
				///  -- at most coulomb with mu = muMax
				///  ---chezy drag if between these values.

				/// The muMax upper limit is for numerical stability only: it stops the equations becoming stiff as h->0
				/// The stiffness arises from the fact that as h->0, the chezy drag law predicts that flows slow to zero speed
				/// over arbitrarily small timescales. Sometimes this is unavoidable, and an implicit timestepper has to be used,
				/// at least for the source terms. This is in the code, if required. However, it introduces quite a few hassles
				/// numerically, so regularising the friction law with a maximum mu is a useful workaround. Given the uncertainty
				/// over the rheology / friction law, the workaround may even be more physically accurate than the simple chezy drag.
				float coulombLowAngle = muMin*g*uvect[H];
				float coulombHighAngle = muMax*g*uvect[H];
				float chezy = ChezyCo*modu*modu;
				//float granular = coulombLowAngle*(exp(10.0*uvect[HPHI]/uvect[H])-1.0)/(exp(10.0)-1.0);
				float friction=chezy;
				if (friction>coulombHighAngle) friction=coulombHighAngle;
				if (friction<coulombLowAngle) friction=coulombLowAngle;
				//cerr << "friction = " << friction << ".  modu = " << modu << endl;
				stvect[HU] += -friction*u/modu;
				stvect[HV] += -friction*v/modu;
			}
			else
			{
				uvect[HU]=0.0;
				uvect[HV]=0.0;	
			}
		}

		// Fix up potential numerical issues with very thin regions. "Thin" = less than 100um thick, so
		// physically the fix should be negligible --- even if over large simulation regions, a 100um thick layer over the
		// whole domain can add up to many cubic metres.
		if (uvect[H]<heightThreshold)
		{
			uvect[H]=heightThreshold*0.5;
			uvect[HU]=0.0;
			uvect[HV]=0.0;
			uvect[HPHI]=uvect[H]*0.5;
		}
	}

/*	inline static void ImplicitSourceTerms(double *stvect, double *uvect)
	{
		double h=uvect[H];
		if (h>0)
		{
			double u=uvect[HU]/uvect[H];
			double v=uvect[HV]/uvect[H];
			double modUmu=sqrt((U-u)*(U-u)+v*v);
			stvect[HU]=-cD*(u-U)*modUmu;
			stvect[HV]=-cD*v*modUmu;
		}
	}
	
	inline static void ImplicitJacobian(double *uvect, double t, double *jac)
	{
//   d/dt (h,hu,hv) = f(h,hu,hv) = (-Cd) sqrt((U-(hu/h))^2 + (hv/h)^2)_k+1 (0,u-U,v)_k+1
//  d (hu) / dh = 
//
		// jacobian = dF(v)_i / dv_j (i = rows, j = cols)
		double h=uvect[H];
		if (h>0)
		{
			double u=uvect[HU]/h;
			double v=uvect[HV]/h;
			double umu=(U-u);
			double modu=sqrt(umu*umu+v*v);
			jac[H+4]  =  cD * (2*U*U*u - 4 *u*u*U - v*v*U + 2*u*u*u+2*u*v*v)/(h*modu);
			jac[HU+4] = -cD * (2*U*U-4*u*U+2*u*u+v*v)/(h*modu);
			jac[HV+4] =  cD * v * umu/(h*modu);
			jac[H+8]  =  cD * v * (U*U - 3*u*U + 2*(v*v+u*u))/(h*modu);
			jac[HU+8] =  cD * v * umu / (h * modu);
			jac[HV+8] = -cD * (umu*umu+2*v*v)/(h*modu);
		}
	}*/
};

double SegregatingEqn::g;
double SegregatingEqn::muMin;
double SegregatingEqn::muMax;
double SegregatingEqn::cd;
double SegregatingEqn::alpha;
KPSolver *SegregatingEqn::solverPtr;

#endif	
