

function PlotFlowErosion()

    function c=GetErosionCol(h)
        h(h>1)=1;
        h(h<-1)=-1;
        hts=[-1 0 1];
        cols=[1 0 0;0 0 0; 0 0 1];
        c=interp1(hts,cols,h);
    end

    S=load('topog.mat');
    [ty tx]=size(S.alt);
      
        dat=LoadCNTSDataEnd('results/100x720/%06d.txt');
        xofs=(tx-dat.xPoints)/2;
        yofs=(ty-dat.yPoints)/2;
        set(gcf,'PaperUnits','centimeters');
        myfiguresize = [0 0 12 12];
        set(gcf, 'PaperPosition', myfiguresize);

        unis = get(gcf,'units');
        ppos = get(gcf,'paperposition');
        set(gcf,'units',get(gcf,'paperunits'));
        pos = get(gcf,'position');
        pos(3:4) = ppos(3:4);
        set(gcf,'position',pos);
        set(gcf,'units',unis);
    
        axes('position',[.3 .1 .5 .7]);
        img=S.topogim;
        for x=1:dat.xPoints
            for y=1:dat.yPoints                
                h=dat.data(x,y,1);          
                er=dat.data(x,y,5);
                if (h>0.03 || er<-0.03 )
                     img(xofs+x,yofs+y,:)=GetErosionCol(h+er);
                end
            end
        end
        image(-4995:10:4995,-4995:10:4995,permute(img,[2 1 3]));
        hold on
        contour(-4995:10:4995,-4995:10:4995,S.alt',1000:200:5000,'Color',[.6 .6 .6]);
        set(gca,'YDir','normal');
        axis([-2000 2000 -3000 4000]);
        pbaspect([4 7 1]);
        xlabel('$x$ (m)');
        ylabel('$y$ (m)','Rotation',0);
        
        axes('position',[.36 .91 .35 .05]);
        colormap(GetErosionCol(linspace(-1,1)));
        imagesc(linspace(-1,1),1,linspace(0,1));
        axis([-1 1 .5 1.5]);
        set(gca,'YTick',[]);
        set(gca,'XTick',[-1 0 1]);
        xlabel('Erosion/deposition (m)');
        drawnow
        PrintFig('erosion','font-minion');
end