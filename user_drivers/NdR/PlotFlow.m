

function PlotFlow(dat)
    colormap(jet(2560));
    for i=1:numel(dat); 
        speed=sqrt(dat(i).data(:,:,2).^2 + dat(i).data(:,:,3).^2)'./dat(i).data(:,:,1)';
        speed(dat(i).data(:,:,1)'<0.02)=0;
        phi=dat(i).data(:,:,4)./dat(i).data(:,:,1);
        phi(dat(i).data(:,:,1)<0.02)=1;
        
        %imagesc(speed);
       %imagesc(dat(i).data(:,:,1)'+0.001*dat(i).data(:,:,6)'); 
       imagesc(phi'); 
       
        set(gca,'YDir','normal'); 
       % caxis([0 10]); 
        maxdepth=max(max(dat(i).data(:,:,1)));
        validheights=dat(i).data(:,:,1)';
        validheights(dat(i).data(:,:,1)'<0.02)=0;
        validheights(speed<1e-2)=0;
        vol=sum(sum(validheights));
        vol=vol*(dat(1).xGrid(2)-dat(1).xGrid(1))*(dat(1).yGrid(2)-dat(1).yGrid(1));
        maxspeed=max(max(speed));
        title(sprintf('max depth=%0.1f m, volume=%0.1f x10^3 m^3, max speed=%0.1f m/s',maxdepth,vol/1000,maxspeed));
        drawnow;
        pause(0.02);
    end
end