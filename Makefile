SOURCES=\
	$(CORE_SRC)/KPSolver.cpp\
	$(CORE_SRC)/VectorArray2d.cpp

LIBRARY=$(CORE_LIB)/libkpsolver.a

CORE_SRC=src
CORE_LIB=lib
CORE_INCLUDE=include

CFLAGS=-Wall -c -I$(CORE_INCLUDE)
RELEASE_CFLAGS=-O2 -DNDEBUG -ffast-math 
DEBUG_CFLAGS=-g

ifeq ($(CONF),Release)
	CFLAGS += $(RELEASE_CFLAGS)
else
	CFLAGS += $(DEBUG_CFLAGS)
endif

CC=g++

OBJECTS=$(SOURCES:.cpp=.o)

all: $(SOURCES) $(LIBRARY)
rebuild: clean all
$(LIBRARY): $(OBJECTS)
	ar rcs $(LIBRARY) $(OBJECTS)
.cpp.o:
	$(CC) $(CFLAGS) $< -o $@
clean:
	rm -f $(OBJECTS) $(LIBRARY)
