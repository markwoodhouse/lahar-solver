

#include "Equation.h"

#include "KPSolver.h"

//time_steppers/time_steppers.cpp"
#include <cstdlib>
#include <sstream>
#include <cstring>
#include <iomanip>

#include <cmath> /// Required only for Kurganov&Petrova positivity preserving stuff
#include <iostream>
#include <limits>
#include <sys/time.h>
#include <sys/stat.h>
#include <errno.h>

using namespace std;

void NullBCFunction(double *u, double x, double y)
{
}

void NullExtrasFn(double *dedt, const VectorArray2d *u)
{
}

void NullExplicitSourceTerms(double *stvect, double *uvect, double *dudxvect, double *dudyvect, const double *extras, const double *dedt)
{
}

void NullImplicitSourceTerms(double *stvect, double *uvect)
{
}

void NullImplicitJacobian(double *uvect, double t, double *jac)
{
}

double GetTimer()
{
	timeval tv;
	gettimeofday(&tv,NULL);
	return double(tv.tv_sec)+double(tv.tv_usec)*0.000001;
}

///////////////////////////////////////////////////////////////////////////////////////////////
/// Limiters

inline double min(const double a, const double b)
{
    if (a < b) return a;
    else return b;
}

inline double max(const double a, const double b)
{
    if (a > b) return a;
    else return b;
}

inline double sign(const double a)
{
    if (a < 0) return -1;
    else if (a == 0) return 0;
    else return 1;
}

inline double KPSolver::LimiterMinMod1(const double a, const double b)
{
    if (sign(a)==1 && sign(b)==1) return min(a,b);
    else if (sign(a)==-1 && sign(b)==-1) return max(a,b);
    else return 0;
}

inline double KPSolver::LimiterMinMod2(const double a, const double b)
{
    const double theta = 2;//1.25;
    if (sign(a) == 1 && sign(b) == 1 && sign(a + b) == 1) return min(theta * a, min(theta * b, 0.5 * (a + b)));
    else if (sign(a) == -1 && sign(b) == -1 && sign(a + b) == -1) return max(theta * a, max(theta * b, 0.5 * (a + b)));
    else return 0;
}

inline double KPSolver::LimiterNone(const double a, const double b)
{
    return 0.5 * (a + b);
}

inline double weno_func(const double &a)
{
    const static double eps = 1e-6;
    double epsaa=a*a+eps;
    return 1.0 / (epsaa*epsaa);
}

inline double KPSolver::LimiterWENO(const double a, const double b)
{
    double wa=weno_func(a), wb=weno_func(b);
    return (wa*a + wb*b) / (wa+wb);
}




//////////////////////////////////////////////////////////////////////////////////////
/// Utility functions
string CreateTimeString()
{
	char startTimeBuffer[80];
    time_t rawtime;
    struct tm * timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(startTimeBuffer, 80, "%Y/%m/%d %H:%M:%S", timeinfo);
	return string(startTimeBuffer);
}


//////////////////////////////////////////////////////////////////////////////////////
/// KPSolver:
/// 	Constructor & destructor

KPSolver::KPSolver(int nx, Equation *lEqn, TimeStepper *lTs)
{
	Construct(nx,1,lEqn,lTs);
}

KPSolver::KPSolver(int nx, int ny, Equation *lEqn, TimeStepper *lTs) 
{
	Construct(nx,ny,lEqn,lTs);
}

void KPSolver::Construct(int nx, int ny, Equation *lEqn, TimeStepper *lTs)
{
	ts=lTs;
	eqn=lEqn;
	nXPoints=nx;
	nYPoints=ny;
	isOneD=false;
	if (nYPoints==1) isOneD=true;
		
	nSolvedDimensions=eqn->NSolvedFields();
	nSpecifiedDimensions=eqn->NSpecifiedFields();
	nExtras=eqn->NExtras();
    dimension = nSolvedDimensions + nSpecifiedDimensions;
	
	SetPositivityPreservationHeight(1e-4);
	
    u.SetSize(dimension, nXPoints, nYPoints, nExtras);
    uLimX.SetSize(dimension, nXPoints, nYPoints);   // limited x-derivatives at cell centre
    uLimY.SetSize(dimension, nXPoints, nYPoints);   // limited y-derivatives at cell centre
    uPlusX.SetSize(dimension, nXPoints + 1, nYPoints); // value at + side of inter-cell discontinuity
    uMinusX.SetSize(dimension, nXPoints + 1, nYPoints); // value at - side of inter-cell discontinuity
    uPlusY.SetSize(dimension, nXPoints, nYPoints + 1); // value at + side of inter-cell discontinuity
    uMinusY.SetSize(dimension, nXPoints, nYPoints + 1); // value at - side of inter-cell discontinuity
    hXFlux.SetSize(dimension, nXPoints + 1, nYPoints);
    hYFlux.SetSize(dimension, nXPoints, nYPoints + 1);
    terrainCorner.SetSize(1, nXPoints+1, nYPoints + 1);
    terrainEdgeX.SetSize(1, nXPoints+1, nYPoints);
    terrainEdgeY.SetSize(1, nXPoints, nYPoints+1);

	// Set initial conditions to NaN to force them to be set.
	for (int i = 0; i < nXPoints; i++) for (int j = 0; j < nYPoints; j++) for (int d=0; d<dimension; d++)
		u(d, i, j)=numeric_limits<double>::quiet_NaN();
    for (int i=0; i<nExtras; i++) u(i)=numeric_limits<double>::quiet_NaN();
	
	for (int i = 0; i < nXPoints; i++) for (int j = 0; j < nYPoints; j++)
		terrainCorner(0, i, j)=numeric_limits<double>::quiet_NaN();
	
	xConvectionFlux=0; yConvectionFlux=0;
	xMaxWaveSpeedFunction=0; yMaxWaveSpeedFunction=0;
	xMinWaveSpeedFunction=0; yMinWaveSpeedFunction=0;
	explicitSourceTermFunction=NullExplicitSourceTerms;
	implicitSourceTermFunction=0;
	extrasFunction=NullExtrasFn;
	jacobianFunction=0;
	

    eqn->FillOutEquations(xConvectionFlux, yConvectionFlux,
            explicitSourceTermFunction, implicitSourceTermFunction, jacobianFunction,
            xMaxWaveSpeedFunction, yMaxWaveSpeedFunction, 
            xMinWaveSpeedFunction, yMinWaveSpeedFunction, extrasFunction);

	if (!xMaxWaveSpeedFunction) throw std::runtime_error("xMaxWaveSpeedFunction is required but not set in equation");
	if (!xMinWaveSpeedFunction) throw std::runtime_error("xMinWaveSpeedFunction is required but not set in equation");
	if (!xConvectionFlux) throw std::runtime_error("xConvectionFlux is required but not set in equation");
	
	if (!isOneD)
	{
		if (!yMaxWaveSpeedFunction) throw std::runtime_error("yMaxWaveSpeedFunction is required but not set in equation");
		if (!yMinWaveSpeedFunction) throw std::runtime_error("yMinWaveSpeedFunction is required but not set in equation");
		if (!yConvectionFlux) throw std::runtime_error("yConvectionFlux is required but not set in equation");
	}
	
	if (implicitSourceTermFunction)
	{
		if (!jacobianFunction) throw std::runtime_error("Implicit source term function set, but no Jacobian provided in equation");
	}
	else
	{
		implicitSourceTermFunction=NullImplicitSourceTerms;
		jacobianFunction=NullImplicitJacobian;
	}
	
    SetDomain(0, 0, 1, 1);
    SetLimiter(MinMod2);
	
    startTime=CreateTimeString();

    simulationTime = 0;
    outputCount = 0;


    SetInitialStepSize(1e-3);
    for (int i = 0; i < 4; i++)
    {
        gradientBCs[i] = new GradientBoundary[dimension];
        fluxBCs[i] = new FluxBoundary[dimension];
        fluxValueBCFunction[i] = NullBCFunction;
        fluxFluxBCFunction[i] = NullBCFunction;
    }

    SetBoundaryConditionFunctions(KPSolver::North);
    SetBoundaryConditionFunctions(KPSolver::South);
    SetBoundaryConditionFunctions(KPSolver::East);
    SetBoundaryConditionFunctions(KPSolver::West);
	
    uPlusConvectionFlux=new double[nSolvedDimensions];
    uMinusConvectionFlux=new double[nSolvedDimensions];   
	
}
KPSolver::~KPSolver()
{
	if (ts) delete ts;
    for (int i = 0; i < 4; i++)
    {
        delete [] gradientBCs[i];
        delete [] fluxBCs[i];
    }
    delete [] uPlusConvectionFlux;
	delete [] uMinusConvectionFlux;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Setters

/// \brief Sets the initial steps size; useful if the initial conditions (e.g. all zeros) do not give a useful CFL condition.
void KPSolver::SetInitialStepSize(double val)
{
    integrationStepSize=val;
}

/// \brief Sets the flux limiter
void KPSolver::SetLimiter(Limiter l)
{
    switch (l)
    {
        case None: limiterPtr = LimiterNone; break;
        case MinMod1: limiterPtr = LimiterMinMod1; break;
        case MinMod2: limiterPtr = LimiterMinMod2; break;
        case WENO: limiterPtr = LimiterWENO; break;
    }
}

/// \brief Sets periodic boundary conditions in all variables on all boundaries.
void KPSolver::SetPeriodicBoundaryConditions()
{
    for (int d = 0; d < dimension; d++)
    {
        SetBoundaryConditionType(North, d, KPSolver::GradientPeriodic, KPSolver::FluxPeriodic);
        SetBoundaryConditionType(South, d, KPSolver::GradientPeriodic, KPSolver::FluxPeriodic);
        SetBoundaryConditionType(East, d, KPSolver::GradientPeriodic, KPSolver::FluxPeriodic);
        SetBoundaryConditionType(West, d, KPSolver::GradientPeriodic, KPSolver::FluxPeriodic);
    }
}

/// \brief Sets first-order extrapolated boundary conditions in all variables on all boundaries.
void KPSolver::SetExtrapolatedBoundaryConditions()
{
    for (int d = 0; d < dimension; d++)
    {
        SetBoundaryConditionType(North, d, KPSolver::GradientExtrapolate1, KPSolver::FluxUseExtrapolated);
        SetBoundaryConditionType(South, d, KPSolver::GradientExtrapolate1, KPSolver::FluxUseExtrapolated);
        SetBoundaryConditionType(East, d, KPSolver::GradientExtrapolate1, KPSolver::FluxUseExtrapolated);
        SetBoundaryConditionType(West, d, KPSolver::GradientExtrapolate1, KPSolver::FluxUseExtrapolated);
    }
}

/// \brief Sets the size of the domain. Usually one would use KPSolver::SetDomain instead
void KPSolver::SetDomainSize(double xsize, double ysize)
{
	if (xsize<=0) throw invalid_argument("Domain X size must be greater than zero in KPSolver::SetDomainSize");
	if (ysize<=0) throw invalid_argument("Domain Y size must be greater than zero in KPSolver::SetDomainSize");
    domainXSize = xsize;
    domainYSize = ysize;

    deltaX = domainXSize / nXPoints;
    deltaY = domainYSize / nYPoints;
    deltaXRecip = 1.0 / deltaX;
    deltaYRecip = 1.0 / deltaY;
}

/// \brief Sets the offset of the domain. Usually one would use KPSolver::SetDomain instead
void KPSolver::SetDomainOffset(double xo, double yo)
{
    domainXOffset = xo;
    domainYOffset = yo;
}

/// \brief Sets the mathematical domain over which the equations will be solved
void KPSolver::SetDomain(double xo, double yo, double xsize, double ysize)
{
    SetDomainOffset(xo, yo);
    SetDomainSize(xsize, ysize);
}


/** \brief Sets the type of boundary condition, for a particular variable, on a particular boundary
 * 
 * Two boundary conditions are required: a boundary condition used when working out limited derivatives in the reconstruction process,
 * and a boundary condition on the fluxes. Some flux options require SetBoundaryConditionFunctions to be called too
 */

void KPSolver::SetBoundaryConditionType(BoundaryID bid, int variable, GradientBoundary gradientBC, FluxBoundary fluxBC)
{
    gradientBCs[bid][variable] = gradientBC;
    fluxBCs[bid][variable] = fluxBC;
}

/** \brief Sets functions that describe the flux boundary conditions, if required.
 * 
 * Todo: document exactly what the flux boundary conditions do and how these functions work.
 */
void KPSolver::SetBoundaryConditionFunctions(BoundaryID bid, BoundaryConditionPointer valueFn, BoundaryConditionPointer fluxFn)
{
    fluxValueBCFunction[bid] = valueFn;
    fluxFluxBCFunction[bid] = fluxFn;
}



///////////////////////////////////////////////////////////////////////////////////////////////
/// Output routines

void KPSolver::OutputResults(std::string filename)
{
    OutputResults(filename.c_str());
}

void KPSolver::OutputResults(const char *filename)
{
    ofstream outFile(filename);
    OutputResults(outFile);
    outFile.close();
}

void KPSolver::OutputResults(ofstream &out)
{
    int d, i, j;
	
    out.precision(16);

    out << "8" << endl;
	
    out << "1 " << dimension << " " << nXPoints << " " << nYPoints << endl;
    out << "2 " << domainXOffset << " " << domainYOffset << " " << domainXSize << " " << domainYSize << " " << simulationTime << " " << -1 << endl; // Old CFL number is output as -1: What actually is going on with CFL?
    out << "3 " << CreateTimeString() << endl;
    out << "4 " << startTime << endl;
    out << "5 " << outputCount++ << endl;
    out << "6 " << -1 << endl; // old FrCoef
	out << "7 ";
	eqn->OutputParameters(out);
	out << endl;

	out << "8 " << nExtras;
	for (int i=0; i<nExtras; i++) out << " " << u.ExtrasPtr()[i];
	out << endl;
	
    for (d = 0; d < dimension; d++)
    {
        for (j = 0; j < nYPoints; j++)
        {
            for (i = 0; i < nXPoints; i++) out << u(d, i, j) << " ";
            out << endl;
        }
    }
}



///////////////////////////////////////////////////////////////
///    Initial conditions 

void KPSolver::SetInitialConditions(VectorSpatialFunctionPt icf, ExtrasPt ept, int subSamplesPerDimension)
{
	ept(u.ExtrasPtr());
	SetSpatialInitialConditions(icf, subSamplesPerDimension);
}

void KPSolver::SetInitialConditions(VectorSpatialFunctionPt icf, int subSamplesPerDimension)
{
	SetSpatialInitialConditions(icf, subSamplesPerDimension);
}

void KPSolver::SetSpatialInitialConditions(VectorSpatialFunctionPt icf, int subSamplesPerDimension)
{
    int i, j, d; 

	double *sampleval;
	sampleval=new double[dimension];
    // Set initial conditions
    for (i = 0; i < nXPoints; i++) for (j = 0; j < nYPoints; j++)
    {
        double xPos = domainXOffset + (i + 0.5) * deltaX;
        double yPos = domainYOffset + (j + 0.5) * deltaY;
		
		for (int d=0; d<dimension; d++) u.Ptr(i, j)[d] =0;
		for (int si=0; si<subSamplesPerDimension; si++) for (int sj=0; sj<subSamplesPerDimension; sj++)
		{
			icf(sampleval, xPos + (2.0*(si+0.5)/double(subSamplesPerDimension) - 1.0)* deltaX * 0.5, yPos + (2.0*(sj+0.5)/double(subSamplesPerDimension) - 1.0)* deltaY * 0.5);
			for (int d=0; d<dimension; d++) u.Ptr(i, j)[d]+=sampleval[d];
		}
		for (int d=0; d<dimension; d++) u.Ptr(i, j)[d] /= subSamplesPerDimension*subSamplesPerDimension;
	}
	delete[] sampleval;
	
    // Setup limited gradients and values at either side of the shocks, for specified fields
    // Now changed to zero-extrapolation rather than periodic. Something else may be appropriate (sample icf on domain boundaries?)
    if (nSpecifiedDimensions > 0)
    {
        for (j = 0; j < nYPoints; j++)
        {
            int prevYCell = max(j - 1,0);
            int nextYCell = min(j + 1, nYPoints-1);
            for (i = 0; i < nXPoints; i++)
            {
                int prevXCell = max(i - 1,0);
                int nextXCell = min(i + 1, nXPoints-1);
                for (d = nSolvedDimensions; d < dimension; d++)
                {
                    uLimX(d, i, j) = deltaXRecip * limiterPtr(u(d, nextXCell, j) - u(d, i, j), u(d, i, j) - u(d, prevXCell, j));
                    uLimY(d, i, j) = deltaYRecip * limiterPtr(u(d, i, nextYCell) - u(d, i, j), u(d, i, j) - u(d, i, prevYCell));
                }
            }
             for (d = nSolvedDimensions; d < dimension; d++)
             {
            // HACK: this is actually first-order extrapolation, more suitable for the case where specified fields are actually
            //       the coordinates x,y.
            // HACK: x-direction only...
			uLimX(d,0,j)=uLimX(d,1,j);
			uLimX(d,nXPoints-1,j)=uLimX(d,nXPoints-2,j);
					
			}
        }
        
        for (j = 0; j < nYPoints; j++)
		{
			for (i = 0; i <= nXPoints; i++)
			{
				int thisYCell = j;
				int prevXCell = max(i - 1,0);
				int nextXCell = min(i,nXPoints-1);

            // HACK: this is actually first-order extrapolation, more suitable for the case where specified fields are actually
            //       the coordinates x,y.
            // HACK: x-direction only...
				for (d = nSolvedDimensions; d < dimension; d++)
				{
					uPlusX(d, i, j) = u(d, nextXCell, thisYCell) - uLimX(d, nextXCell, thisYCell)*0.5 * deltaX;
					uMinusX(d, i, j) = u(d, prevXCell, thisYCell) + uLimX(d, prevXCell, thisYCell)*0.5 * deltaX;
				}
			}
			for (d = nSolvedDimensions; d < dimension; d++)
			{
				uMinusX(d,0,j)=uPlusX(d,0,j);
				uPlusX(d,nXPoints,j)=uMinusX(d,nXPoints,j);
			}
        }
     
        
		//for (i = 0; i <= nXPoints; i++)cout << uPlusX(2, i, 0) << " " << uMinusX(2, i, 0) << endl;

        for (j = 0; j <= nYPoints; j++) for (i = 0; i < nXPoints; i++)
        {

            int thisXCell = i;
            int prevYCell = max(j - 1, 0);
            int nextYCell = min(j,nYPoints-1);

            for (d = nSolvedDimensions; d < dimension; d++)
            {
                uPlusY(d, i, j) = u(d, thisXCell, nextYCell) - uLimY(d, thisXCell, nextYCell)*0.5 * deltaY;
                uMinusY(d, i, j) = u(d, thisXCell, prevYCell) + uLimY(d, thisXCell, prevYCell)*0.5 * deltaY;
            }
        }
    }
}

void KPSolver::SetTerrain(ScalarSpatialFunctionPt fn)
{
	// Set terrain on cell corners
	int i,j;
    for (i = 0; i <= nXPoints; i++)
        for (j = 0; j <= nYPoints; j++)
        {
            double xPos = domainXOffset + i * deltaX;
            double yPos = domainYOffset + j * deltaY;
            fn(terrainCorner(1,i,j), xPos, yPos);
        }
    // Interpolate cell corners to get terrain on cell edges
    for (i = 0; i < nXPoints; i++) for (j = 0; j <= nYPoints; j++)
		terrainEdgeX(0,i,j)=0.5*(terrainCorner(0,i,j)+terrainCorner(0,i+1,j));
    for (i = 0; i <= nXPoints; i++) for (j = 0; j < nYPoints; j++)
		terrainEdgeY(0,i,j)=0.5*(terrainCorner(0,i,j)+terrainCorner(0,i,j+1));
	
}

void KPSolver::SampleFunction(VectorArray2d &soln, VectorSpatialFunctionPt fn)
{
    int i, j;
    soln.SetSize(dimension, nXPoints, nYPoints);
    // Set initial conditions
    for (i = 0; i < nXPoints; i++)
        for (j = 0; j < nYPoints; j++)
        {
            double xPos = domainXOffset + (i + 0.5) * deltaX;
            double yPos = domainYOffset + (j + 0.5) * deltaY;
            fn(soln.Ptr(i, j), xPos, yPos);
        }
}

void KPSolver::LoadInitialConditions(const char *filename, int d)
{
    ifstream inFile(filename);
    LoadInitialConditions(inFile,d);
    inFile.close();
}

void KPSolver::LoadInitialConditions(ifstream &in,int d)
{
    int i, j;

    for (j = 0; j < nYPoints; j++)
    {
        for (i = 0; i < nXPoints; i++)
        {
            u(d,i,j)=0;
            in >> u(d, i, j);
        
            in.ignore();
        }
        in.ignore(10,'\n');
    }
}


///////////////////////////////////////////////////////////////
///    'Solve' methods

void KPSolver::IntegrateFor(double integrationTime)
{
    IntegrateTo(simulationTime + integrationTime);
}

void KPSolver::IntegrateTo(double destinationTime)
{
	ts->Integrate(simulationTime, destinationTime, this, u);
}

/** \brief Integrates equations and outputs results at equally spaced times
 * Integrates from 0 to finalTime, outputting at nSteps+1 points (including the initial conditions).
 **/
void KPSolver::Run(double finalTime, int nSteps, const char *filename)
{
	OutputResults(GenerateOutputFilename(filename, 0));

	for (int i=1; i<=nSteps; i++)
	{
		IntegrateTo(finalTime*double(i)/double(nSteps));
		OutputResults(GenerateOutputFilename(filename, i));
	}
}

/** \brief Integrates equations and outputs results at equally spaced times, using automatically generated filenames
 * Integrates from 0 to finalTime, outputting at nSteps+1 points (including the initial conditions).
 * Files are automatically generated in subfolders of a results/ folder (which will be created if it does not exist).
 * The subfolder names are generated from the registered parameters of the problem. The files within the subfolders are
 * of the form 000000.txt, 000001.txt, etc.
 **/
void KPSolver::Run(const char *OutDir, double finalTime, int nSteps)
{
	if (mkdir(OutDir,S_IRUSR|S_IWUSR|S_IXUSR|S_IRGRP|S_IROTH))
	{
		if (errno!=EEXIST)
		{
			cerr << "Could not create results/ directory" << endl;
			exit(1);
		}
	}
		
	ostringstream diross;
	string p=string(OutDir);
	string q=string("/");
	p += q;
	diross << p;
	eqn->OutputParameters(diross,'_');
	if (isOneD) diross << XPoints();
	else diross << XPoints()<<"x"<<YPoints();
	
	if (mkdir(diross.str().c_str(),S_IRUSR|S_IWUSR|S_IXUSR|S_IRGRP|S_IROTH))
	{
		if (errno==EEXIST) cerr << "Writing into existing directory " << OutDir << diross.str() << endl;
		else
		{
			cerr << "Could not create results directory" << OutDir << diross.str() << endl;
			exit(1);
		}
	}
	else cerr << "Creating directory " << diross.str()  << endl;
	diross << "/";
	Run(finalTime, nSteps ,diross.str().c_str());
}

/** \brief Appends a number and .txt extension to an existing basename to get a file name

 If basename is omitted or is of zero length, returned string will be 000000.txt
 If basename ends in / or \ (i.e. it represents a directory), returned string will be basename/000000.txt
 If basename ends in any other character, returned string will be named basename_000000.txt
 **/
string KPSolver::GenerateOutputFilename(const char *basename, int n, int width)
{
	stringstream oss;
	if (basename && strlen(basename)>0)
	{
		oss << basename;
		if (!(basename[strlen(basename)-1]=='/' || basename[strlen(basename)-1]=='\\')) oss << "_";
	}
	
	oss << setw(width) << setfill('0') << n <<".txt";
	return oss.str();
}

///////////////////////////////////////////////////////////////
///    Limited derivative calculation


void KPSolver::CalculateLimitedDerivs(const VectorArray2d &uvect)
{
    int i, j, d;

    // Over all y-points, but not East and West boundaries, calculate limited X-derivs

    for (j = 0; j < nYPoints; j++)
    {
        for (i = 1; i < nXPoints - 1; i++)
        {
            int prevXCell = (i - 1 + nXPoints) % nXPoints;
            int nextXCell = (i + 1) % nXPoints;

            for (d = 0; d < nSolvedDimensions; d++)
            {
                uLimX(d, i, j) = deltaXRecip * limiterPtr(uvect(d, nextXCell, j) - uvect(d, i, j), uvect(d, i, j) - uvect(d, prevXCell, j));
            }
			
        }
    }

    // Over all x-points, but not North and South boundaries, calculate limited Y-derivs
    if (!isOneD) for (j = 1; j < nYPoints - 1; j++)
    {
        int prevYCell = (j - 1 + nYPoints) % nYPoints;
        int nextYCell = (j + 1) % nYPoints;
        for (i = 0; i < nXPoints; i++)
        {
            for (d = 0; d < nSolvedDimensions; d++)
            {
                uLimY(d, i, j) = deltaYRecip * limiterPtr(uvect(d, i, nextYCell) - uvect(d, i, j), uvect(d, i, j) - uvect(d, i, prevYCell));
            }
        }
    }

    // Now fill in boundaries:

	if (!isOneD)
	{
		// Y-derivs on South boundary
		j = 0;
		for (i = 0; i < nXPoints; i++) for (d = 0; d < nSolvedDimensions; d++) CalculateLimitedDerivsBoundary(d, i, j, uvect, South, gradientBCs[South][d]);

		// Y-derivs on North boundary
		j = nYPoints - 1;
		for (i = 0; i < nXPoints; i++) for (d = 0; d < nSolvedDimensions; d++) CalculateLimitedDerivsBoundary(d, i, j, uvect, North, gradientBCs[North][d]);
	}
	
    // X-derivs on West boundary
    i = 0;
    for (j = 0; j < nYPoints; j++) for (d = 0; d < nSolvedDimensions; d++) CalculateLimitedDerivsBoundary(d, i, j, uvect, West, gradientBCs[West][d]);

    // X-derivs on East boundary
    i = nXPoints - 1;
    for (j = 0; j < nYPoints; j++) for (d = 0; d < nSolvedDimensions; d++) CalculateLimitedDerivsBoundary(d, i, j, uvect, East, gradientBCs[East][d]);
}

void KPSolver::CalculateLimitedDerivsBoundary(int d, int i, int j, const VectorArray2d &uvect, BoundaryID bid, GradientBoundary bType)
{

    int prevXCell, prevYCell, jIncr = 0;
    int nextXCell, nextYCell, iIncr = 0;

    if (bType != GradientPeriodic)
    {
        switch (bid)
        {
            case North: jIncr = -1;
                break;
            case South: jIncr = 1;
                break;
            case East: iIncr = -1;
                break;
            case West: iIncr = 1;
                break;
        }
    }

    switch (bid)
    {
        case North: case South:
            switch (bType)
            {
                case GradientPeriodic:
                    prevXCell = i;
                    nextXCell = i;
                    prevYCell = (j - 1 + nYPoints) % nYPoints;
                    nextYCell = (j + 1) % nYPoints;
                    uLimY(d, i, j) = deltaYRecip * limiterPtr(uvect(d, nextXCell, nextYCell) - uvect(d, i, j), uvect(d, i, j) - uvect(d, prevXCell, prevYCell));
                    break;
                case GradientExtrapolate0: uLimY(d, i, j) = 0;
                    break;
                case GradientExtrapolate1: uLimY(d, i, j) = uLimY(d, i + iIncr, j + jIncr);
                    break;
                case GradientExtrapolate2: uLimY(d, i, j) = 2.0 * uLimY(d, i + iIncr, j + jIncr) - uLimY(d, i + 2 * iIncr, j + 2 * jIncr);
                    break;
            }
            break;

        case East: case West:
            switch (bType)
            {
                case GradientPeriodic:
                    prevXCell = (i - 1 + nXPoints) % nXPoints;
                    nextXCell = (i + 1) % nXPoints;
                    prevYCell = j;
                    nextYCell = j;
                    uLimX(d, i, j) = deltaXRecip * limiterPtr(uvect(d, nextXCell, nextYCell) - uvect(d, i, j), uvect(d, i, j) - uvect(d, prevXCell, prevYCell));
                    break;
                case GradientExtrapolate0: uLimX(d, i, j) = 0;
                    break;
                case GradientExtrapolate1: uLimX(d, i, j) = uLimX(d, i + iIncr, j + jIncr);
                    break;
                case GradientExtrapolate2: uLimX(d, i, j) = 2.0 * uLimX(d, i + iIncr, j + jIncr) - uLimX(d, i + 2 * iIncr, j + 2 * jIncr);
                    break;
            }
            break;
    }

}


///////////////////////////////////////////////////////////////
///    Flux calculation
void KPSolver::CalculateFluxes(const VectorArray2d &uvect, const VectorArray2d &results)
{
    int i, j, d;

    // Calculate fluxes at each cell edge

    double wsPlus, wsMinus, aPos, aNeg;
    // Do X fluxes
    for (j = 0; j < nYPoints; j++) for (i = 0; i <= nXPoints; i++)
    {
        int thisYCell = j;

        int prevXCell = (i - 1 + nXPoints) % nXPoints;
        int nextXCell = i % nXPoints;

        for (d = 0; d < nSolvedDimensions; d++)
        {
            uPlusX(d, i, j) = uvect(d, nextXCell, thisYCell) - uLimX(d, nextXCell, thisYCell)*0.5 * deltaX;
            uMinusX(d, i, j) = uvect(d, prevXCell, thisYCell) + uLimX(d, prevXCell, thisYCell)*0.5 * deltaX;
        }
        
	}
	
	
	
    for (j = 0; j <= nYPoints; j++) for (i = 0; i < nXPoints; i++)
    {
        int thisXCell = i;

        int prevYCell = (j - 1 + nYPoints) % nYPoints;
        int nextYCell = j % nYPoints;

        for (d = 0; d < nSolvedDimensions; d++)
        {
            uPlusY(d, i, j) = uvect(d, thisXCell, nextYCell) - uLimY(d, thisXCell, nextYCell)*0.5 * deltaY;
            uMinusY(d, i, j) = uvect(d, thisXCell, prevYCell) + uLimY(d, thisXCell, prevYCell)*0.5 * deltaY;
        }
	}


	//cout << "uPlus="<<uPlusX(0,133,100)<<" uMinus="<<uMinusX(0,133+1,100)<<" i="<<133<<" j="<<100<<" d="<<0<<" h="<<uvect(0,133,100)<<" hu="<<uvect(1,133,100)<<" hv="<<uvect(2,133,100)<<endl;
	for (d=0; d<nSolvedDimensions; d++)
	{
		if (eqn->IsPositivityPreserving(d))
		{
			for (j = 0; j < nYPoints; j++) for (i = 0; i <= nXPoints; i++)
			{
				/// Force reconstruction to have non-negative height at cell edges
				if (uMinusX(d,i,j)<0)
				{
					double overshoot=-uMinusX(d,i,j);
					uMinusX(d,i,j)=0;
					if (i>0)
					{
						uPlusX(d,i-1,j)-=overshoot;
						assert(uPlusX(d,i-1,j)>-1e-16);
						if (uPlusX(d,i-1,j)<0) uPlusX(d,i-1,j)=0;
					}
				}
				if (uPlusX(d,i,j)<0)
				{
					double overshoot=-uPlusX(d,i,j);
					uPlusX(d,i,j)=0;
					if (i<nXPoints)
					{
						uMinusX(d,i+1,j)-=overshoot;
					//	if (uMinusX(d,i+1,j)<-1e-16)
					//	{
					//			cout << "uPlus="<<uPlusX(d,i,j)<<" uMinus="<<uMinusX(d,i+1,j)<<" i="<<i<<" j="<<j<<" d="<<d<<" uvect="<<uvect(d,i,j)<<endl;
					//	}
						assert(uMinusX(d,i+1,j)>-1e-16);
						if (uMinusX(d,i+1,j)<0) uMinusX(d,i+1,j)=0;
					}
				}
			}
			
			if (!isOneD) for (j = 0; j <= nYPoints; j++) for (i = 0; i < nXPoints; i++)
			{
				/// Force reconstruction to have non-negative height at cell edges
				if (uMinusY(d,i,j)<0)
				{
					double overshoot=-uMinusY(d,i,j);
					uMinusY(d,i,j)=0;
					if (j>0)
					{
						uPlusY(d,i,j-1)-=overshoot;
						assert(uPlusY(d,i,j-1)>-1e-16);
						if (uPlusY(d,i,j-1)<0) uPlusY(d,i,j-1)=0;
					}
				}
				if (uPlusY(d,i,j)<0)
				{
					double overshoot=-uPlusY(d,i,j);
					uPlusY(d,i,j)=0;
					if (j<nYPoints)
					{
						uMinusY(d,i,j+1)-=overshoot;
						assert(uMinusY(d,i,j+1)>-1e-16);
						if (uMinusY(d,i,j+1)<0) uMinusY(d,i,j+1)=0;
					}
				}
			}
		}
	}
	
	for (d=0; d<nSolvedDimensions; d++)
	{
		if (eqn->IsPositivityReconstruct(d)) /// Correct low heights, after Kurganov & Petrova (2007)
		{
			unsigned d2=eqn->PositivityReconstructId(d);
			//cout << "Reconstructing " << d << " to " << d2 << endl;
			double dx4=posPresEps4;//1e-1*deltaX*deltaX*deltaX*deltaX;
			for (j = 0; j < nYPoints; j++) for (i = 0; i <= nXPoints; i++)
			{
				double h=uPlusX(d2,i,j);
				assert(h>=0);
				double h4=h*h*h*h;
				double hu=uPlusX(d,i,j); // 'hu' (and 'u', below) can represent either x- or y- momenta, depending on value of d
				double u=sqrt(2.0)*h*hu/sqrt(h4+max(h4,dx4));
				uPlusX(d,i,j)=h*u;
				//if (hu!=uPlusX(d,i,j)) cout << "Changed uPlusX("<<d<<","<<i<<","<<j<<") from "<<hu<<" to "<<uPluxX(d,i,j)<<", h="<<uPluxX(d2,i,j)<<endl;
				
				h=uMinusX(d2,i,j);
				assert(h>=0);
				h4=h*h*h*h;
				hu=uMinusX(d,i,j);
				u=sqrt(2.0)*h*hu/sqrt(h4+max(h4,dx4));
				uMinusX(d,i,j)=h*u;
			}
			
			if (!isOneD)
			{
				double dy4=posPresEps4;//1e-1*deltaY*deltaY*deltaY*deltaY;
				for (j = 0; j <= nYPoints; j++) for (i = 0; i < nXPoints; i++)
				{
					double h=uPlusY(d2,i,j);
					assert(h>=0);
					double h4=h*h*h*h;
					double hu=uPlusY(d,i,j);
					double u=sqrt(2.0)*h*hu/sqrt(h4+max(h4,dy4));
					uPlusY(d,i,j)=h*u;
					
					h=uMinusY(d2,i,j);
					assert(h>=0);
					h4=h*h*h*h;
					hu=uMinusY(d,i,j);
					u=sqrt(2.0)*h*hu/sqrt(h4+max(h4,dy4));
					uMinusY(d,i,j)=h*u;
				}
			}
		}
	}
	for (j = 0; j < nYPoints; j++) for (i = 0; i <= nXPoints; i++)
    {
        wsPlus =  xMaxWaveSpeedFunction(uPlusX.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
        wsMinus = xMaxWaveSpeedFunction(uMinusX.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
      //  if (i==0) aPos=wsPlus;
		//else if (i==nXPoints) aPos=wsMinus;
		//else 
			aPos = (wsPlus > wsMinus) ? wsPlus : wsMinus;
		aPos=max(aPos,0);
		
        wsPlus =  xMinWaveSpeedFunction(uPlusX.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
        wsMinus = xMinWaveSpeedFunction(uMinusX.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
       // if (i==0) aNeg=wsPlus;
		//else if (i==nXPoints) aNeg=wsMinus;
		//else
			aNeg = (wsPlus < wsMinus) ? wsPlus : wsMinus;
		aNeg=min(aNeg,0);
		
		if (abs(aPos)>1e-16 && deltaX/abs(aPos)<unitCFLTimeStep) unitCFLTimeStep=deltaX/abs(aPos);
		if (abs(aNeg)>1e-16 && deltaX/abs(aNeg)<unitCFLTimeStep) unitCFLTimeStep=deltaX/abs(aNeg);

        xConvectionFlux(uPlusConvectionFlux, uPlusX.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
        xConvectionFlux(uMinusConvectionFlux, uMinusX.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());

		double dif=aPos-aNeg;
        for (d = 0; d < nSolvedDimensions; d++)
        {
			if (dif<1e-10) hXFlux(d, i, j) = 0;
			else
			{
				hXFlux(d,i,j) =  ((aPos*uMinusConvectionFlux[d] - aNeg*uPlusConvectionFlux[d]) + (uPlusX(d, i, j) - uMinusX(d, i, j))*(aPos*aNeg))/dif;

				//throw "here";
			}
				
		}
    }
    
    // Do Y fluxes
    if (!isOneD) for (j = 0; j <= nYPoints; j++) for (i = 0; i < nXPoints; i++)
    {	
		wsPlus =  yMaxWaveSpeedFunction(uPlusY.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
        wsMinus= yMaxWaveSpeedFunction(uMinusY.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
        //if (j==0) aPos=wsPlus;
		//else if (j==nYPoints) aPos=wsMinus;
		//else 
			aPos = (wsPlus > wsMinus) ? wsPlus : wsMinus;
		aPos=max(aPos,0);
		
        wsPlus =  yMinWaveSpeedFunction(uPlusY.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
        wsMinus = yMinWaveSpeedFunction(uMinusY.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
       // if (j==0) aNeg=wsPlus;
		//else if (j==nYPoints) aNeg=wsMinus;
		//else 
			aNeg = (wsPlus < wsMinus) ? wsPlus : wsMinus;
		aNeg=min(aNeg,0);
		
		if (abs(aPos)>1e-16 && deltaX/abs(aPos)<unitCFLTimeStep) unitCFLTimeStep=deltaX/abs(aPos);
		if (abs(aNeg)>1e-16 && deltaX/abs(aNeg)<unitCFLTimeStep) unitCFLTimeStep=deltaX/abs(aNeg);

        yConvectionFlux(uPlusConvectionFlux, uPlusY.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
        yConvectionFlux(uMinusConvectionFlux, uMinusY.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());

		double dif=aPos-aNeg;
        for (d = 0; d < nSolvedDimensions; d++)
		{
			if (dif<1e-10) hYFlux(d, i, j) = 0;  
			else hYFlux(d, i, j) = ((aPos*uMinusConvectionFlux[d] - aNeg*uPlusConvectionFlux[d]) + (uPlusY(d, i, j) - uMinusY(d, i, j))*(aPos*aNeg))/dif;
        }
    }
}

void KPSolver::SetBoundaryFlux(int i, int j, BoundaryID bid, FluxBoundary *boundaryType, const VectorArray2d &uvect,const VectorArray2d &results)
{
    int d;
    double xPos, yPos;
    double wsPlus, wsMinus, aPos, aNeg;


    // Get position
    xPos = domainXOffset + i*deltaX;
    yPos = domainYOffset + j*deltaY;
    switch (bid)
    {
        case North: case South:
            xPos += 0.5 * deltaX;
            break;
        case East: case West:
            yPos += 0.5 * deltaY;
            break;
    }


    for (d = 0; d < nSolvedDimensions; d++)
    {
        if (boundaryType[d] == FluxUseExtrapolated || boundaryType[d] == FluxZeroFlux || boundaryType[d] == FluxSetValue) switch (bid)
        {
            case West: uMinusX(d, i, j) = uPlusX(d, i, j); break;
            case East: uPlusX(d, i, j) = uMinusX(d, i, j); break;
            case South: uMinusY(d, i, j) = uPlusY(d, i, j); break;
            case North: uPlusY(d, i, j) = uMinusY(d, i, j); break;
        }

        if (boundaryType[d] == FluxPeriodic) switch (bid)
        {
            case West: uMinusX(d, i, j) = uMinusX(d, nXPoints, j); break;
            case East: uPlusX(d, i, j) = uPlusX(d, 0, j); break;
            case South: uMinusY(d, i, j) = uMinusY(d, i, nYPoints); break;
            case North: uPlusY(d, i, j) = uPlusY(d, i, 0); break;
        }
    }

    // If value BC function is set, overwrite values
    // NOTE: this is a nonideal (possibly wrong) implementation
    // outerShockValue = 2*boundaryConditionValue - innerShockValue
    // is right (or at least right to a higher order), I think, though potentially unstable.
    // Due to this implementation, it is better to use zero-flux boundaries at walls,
    // rather than u=0 type conditions.
    
    // Later NOTE: testing this actually revealed that a) the 2*bv - innershock gave the wrong results when setting H=0 at a front
    //                                                 b) it made very little difference when setting finite Fr at front

    switch (bid)
    {
        case West: fluxValueBCFunction[bid](uMinusX.Ptr(i, j), xPos, yPos); break;
        case East: fluxValueBCFunction[bid](uPlusX.Ptr(i, j), xPos, yPos); 	break;
        case South: fluxValueBCFunction[bid](uMinusY.Ptr(i, j), xPos, yPos); break;
        case North: fluxValueBCFunction[bid](uPlusY.Ptr(i, j), xPos, yPos); break;
    }


    switch (bid)
    {
        case North: case South:
			wsPlus =  yMaxWaveSpeedFunction(uPlusY.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
			wsMinus= yMaxWaveSpeedFunction(uMinusY.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
			aPos=max(max(wsPlus,wsMinus),0);

			wsPlus =  yMinWaveSpeedFunction(uPlusY.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
			wsMinus = yMinWaveSpeedFunction(uMinusY.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
			aNeg=min(min(wsPlus,wsMinus),0);
		
            yConvectionFlux(uPlusConvectionFlux, uPlusY.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
            yConvectionFlux(uMinusConvectionFlux, uMinusY.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());

            for (d = 0; d < nSolvedDimensions; d++)
            {
                switch (boundaryType[d])
                {
                    case FluxZeroFlux: hYFlux(d,i,j)=0; break;
                    default: 			
						if (abs(aPos-aNeg)<1e-16)
							hYFlux(d, i, j) = 0; // this is a hack which may cause problems at fronts etc. where aPos->aNeg but both are far from zero
						else
							hYFlux(d, i, j) =  (aPos*uMinusConvectionFlux[d] - aNeg*uPlusConvectionFlux[d])/(aPos-aNeg) + (uPlusY(d, i, j) - uMinusY(d, i, j))*(aPos*aNeg)/(aPos-aNeg);
						break;
                }
                // Overwrite any fluxes desired with the flux BC.
                fluxFluxBCFunction[bid](hYFlux.Ptr(i,j), xPos, yPos);
            }

            break;

        case East: case West:
			wsPlus =  xMaxWaveSpeedFunction(uPlusX.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
			wsMinus = xMaxWaveSpeedFunction(uMinusX.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
			aPos=max(max(wsPlus,wsMinus),0);
			
			wsPlus =  xMinWaveSpeedFunction(uPlusX.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
			wsMinus = xMinWaveSpeedFunction(uMinusX.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
			aNeg=min(min(wsPlus,wsMinus),0);

            xConvectionFlux(uPlusConvectionFlux, uPlusX.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
            xConvectionFlux(uMinusConvectionFlux, uMinusX.Ptr(i, j), uvect.ExtrasPtr(), results.ExtrasPtr());
            for (d = 0; d < nSolvedDimensions; d++)
            {
                switch (boundaryType[d])
                {
                    case FluxZeroFlux: hXFlux(d,i,j)=0; break;
                    default:
						if (abs(aPos-aNeg)<1e-16)
							hXFlux(d, i, j) = 0; // this is a hack which may cause problems at fronts etc. where aPos->aNeg but both are far from zero
						else
							hXFlux(d, i, j) = (aPos*uMinusConvectionFlux[d] - aNeg*uPlusConvectionFlux[d])/(aPos-aNeg) + (uPlusX(d, i, j) - uMinusX(d, i, j))*(aPos*aNeg)/(aPos-aNeg);
						break;
                }
                // Overwrite any fluxes desired with the flux BC.
                fluxFluxBCFunction[bid](hXFlux.Ptr(i,j), xPos, yPos);
            }
            break;
    }

}

void KPSolver::CalculateFluxBoundaryConditions(const VectorArray2d&uvect, const VectorArray2d &results)
{
    int i, j;
	
	if (!isOneD)
	{
		j = 0;
		for (i = 0; i < nXPoints; i++) SetBoundaryFlux(i, j, South, fluxBCs[South], uvect,results);

		j = nYPoints;
		for (i = 0; i < nXPoints; i++) SetBoundaryFlux(i, j, North, fluxBCs[North], uvect,results);
	}
    i = 0;
    for (j = 0; j < nYPoints; j++) SetBoundaryFlux(i, j, West, fluxBCs[West], uvect,results);

    i = nXPoints;
    for (j = 0; j < nYPoints; j++) SetBoundaryFlux(i, j, East, fluxBCs[East], uvect,results);
}



/** \brief Time derivative function

 Overridden from VectorFunctionBase
 Calculates the semi-discrete time-derivative of the solution in uvect at time t,
 putting the result in result, and calculating the time-step required for CFL=1,
 putting this is unitCFL.
 
 Execution is as follows
 \li Calculate limited derivatives of uvect
 \li Calculate fluxes of uvect
 \li Result is the difference of fluxes at cell boundaries
 \li Apply the source term function, which is at liberty to do what it likes with result.

*/
void KPSolver::CalculateExplicitTimeDerivative(const VectorArray2d&uvect, const double t, VectorArray2d&result, double &unitCFL)
{
    int i, j, d;
	evaluationTime=t;
	unitCFLTimeStep=numeric_limits<double>::max();
	
	extrasFunction(result.ExtrasPtr(), &uvect);
    CalculateLimitedDerivs(uvect);
    CalculateFluxes(uvect,result);
    CalculateFluxBoundaryConditions(uvect,result);
	unitCFL=unitCFLTimeStep;
	//cout << "unitCFL="<<unitCFL<<" deltaX=" << deltaX << " maxA="<<deltaX/unitCFLTimeStep << endl;
	
	if (!isOneD)
	{
		for (j = 0; j < nYPoints; j++) for (i = 0; i < nXPoints; i++)
		{
			for (d = 0; d < nSolvedDimensions; d++)
			{
				result(d, i, j) = (hXFlux(d, i, j) - hXFlux(d, i + 1, j)) * deltaXRecip
								+ (hYFlux(d, i, j) - hYFlux(d, i, j + 1)) * deltaYRecip;

			}
			
			for (; d<dimension; d++) result(d, i, j)=0; // Clear result for unsolved-for dimensions
			explicitSourceTermFunction(result.Ptr(i, j), uvect.Ptr(i, j), uLimX.Ptr(i,j), uLimY.Ptr(i,j), uvect.ExtrasPtr(),result.ExtrasPtr());
		}
	}
	else
	{
		for (i = 0; i < nXPoints; i++)
		{
			for (d = 0; d < nSolvedDimensions; d++) 
				result(d, i, 0) = (hXFlux(d, i, 0) - hXFlux(d, i + 1, 0)) * deltaXRecip;
			for (; d<dimension; d++) result(d, i, 0)=0; // Clear result for unsolved-for dimensions
			explicitSourceTermFunction(result.Ptr(i, 0), uvect.Ptr(i, 0), uLimX.Ptr(i,0), uLimY.Ptr(i,0),uvect.ExtrasPtr(),result.ExtrasPtr());
		}
	}
}

void KPSolver::CalculateImplicitTimeDerivative(double *uvect, double t, double *result)
{			
	implicitSourceTermFunction(result, uvect);
}

// Is this version ever called??
/*
void KPSolver::CalculateImplicitTimeDerivative(VectorArray2d&uvect, double t, VectorArray2d&result)
{
    int i, j, d;
	evaluationTime=t;
	if (!isOneD)
	{
		for (j = 0; j < nYPoints; j++) for (i = 0; i < nXPoints; i++)
		{
			for (d = 0; d < nSolvedDimensions; d++) result(d, i, j) = 0;
			implicitSourceTermFunction(result.Ptr(i, j), uvect.Ptr(i, j),  uLimX.Ptr(i,j), uLimY.Ptr(i,j), uvect.ExtrasPtr(), result.ExtrasPtr());
		}
	}
	else
	{
		for (i = 0; i < nXPoints; i++)
		{
			for (d = 0; d < nSolvedDimensions; d++) result(d, i, 0) = 0;
			implicitSourceTermFunction(result.Ptr(i, 0), uvect.Ptr(i, 0),  uLimX.Ptr(i,0), uLimY.Ptr(i,0), uvect.ExtrasPtr(), result.ExtrasPtr());
		}
	}
}
*/
