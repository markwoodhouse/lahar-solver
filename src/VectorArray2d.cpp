#include "VectorArray2d.h"
#include <cstring>
#include <cmath>

using namespace std;

VectorArray2d::VectorArray2d() {data=NULL; width=0; height=0; dimension=0;extras=0;}

VectorArray2d::VectorArray2d(const VectorArray2d &rhs)
{
	//if (data) delete[] data;
	data=0; 
	width=rhs.width; height=rhs.height; dimension=rhs.dimension; extras=rhs.extras;
	if (rhs.data)
	{
		data=new double[width*height*dimension+extras];
		if (!data) return;
		std::memcpy(data,rhs.data,(width*height*dimension+extras)*sizeof(double));
	}
}

void VectorArray2d::SetSize(int d, int w, int h, int ex)
{
	if (width==w && height==h && dimension==d && extras==ex) return;
	if (data) delete[] data;
	data=0;
	width=w; height=h; dimension=d; extras=ex;
	data=new double[width*height*dimension+extras];
	if (!data) return;
	Zero();
}


VectorArray2d& VectorArray2d::operator=(const VectorArray2d &rhs)
{
	if (!rhs.data) return *this;
	if (width!=rhs.width || height!=rhs.height || dimension!=rhs.dimension || extras!=rhs.extras || !data)
	{
		if (data) delete[] data;
		data=0; 
		width=rhs.width; height=rhs.height; dimension=rhs.dimension; extras=rhs.extras;
		data=new double[width*height*dimension+extras];
	}
	if (data) memcpy(data,rhs.data,(width*height*dimension+extras)*sizeof(double));
	return *this;
}

VectorArray2d::VectorArray2d(int d, int w, int h, int ex)
{
	width=w; height=h; dimension=d; extras=ex;
	data=0;
	data=new double[width*height*dimension+extras];
	if (!data) return;
	Zero();
}
VectorArray2d::VectorArray2d(int l)
{
	width=1; height=1; dimension=l; extras=0;
	data=0;
	data=new double[width*height*dimension+extras];
	if (!data) return;
	Zero();
}

double VectorArray2d::L2Norm()
{
	double sum2=0;
	int x,y,d;
	for (y=0; y<height; y++) for (x=0; x<width; x++) for (d=0; d<dimension; d++) sum2+=(*this)(d,x,y)*(*this)(d,x,y);
	return sqrt(sum2/(width*height));
}
double VectorArray2d::L1Norm()
{
	double sum2=0;
	int x,y,d;
	for (y=0; y<height; y++) for (x=0; x<width; x++) for (d=0; d<dimension; d++) sum2+=fabs((*this)(d,x,y));
	return sum2/(width*height);
}


double VectorArray2d::LInfNorm()
{
	double maxmod=0;
	int x,y,d;
	for (y=0; y<height; y++) for (x=0; x<width; x++) for (d=0; d<dimension; d++) if (fabs((*this)(d,x,y))>maxmod) maxmod=fabs((*this)(d,x,y));
	return maxmod;
}

void VectorArray2d::Zero()
{
	for (int y=0; y<height; y++) for (int x=0; x<width; x++) for (int d=0; d<dimension; d++) (*this)(d,x,y)=0;
	for (int i=0; i<extras; i++) (*this)(i)=0;
}

VectorArray2d::~VectorArray2d()
{
	if (data) delete[] data;
	data=0; 
}

VectorArray2d VectorArray2d::operator+(const VectorArray2d &a)
{
	int x,y;
	if (width!=a.Width() || height!=a.Height() || dimension!=a.Dimension()) throw invalid_argument("Incompatible size/dimension in VectorArray2d::operator+");
	VectorArray2d tmp(dimension,width,height,extras);
	for (y=0; y<height; y++) for (x=0; x<width; x++) for (int d=0; d<dimension; d++) tmp(d,x,y)=(*this)(d,x,y)+a(d,x,y);
	return tmp;
}

VectorArray2d VectorArray2d::operator-(const VectorArray2d &a)
{
	int x,y;
	if (width!=a.Width() || height!=a.Height() || dimension!=a.Dimension())  throw invalid_argument("Incompatible size/dimension in VectorArray2d::operator-");
	VectorArray2d tmp(dimension,width,height,extras);
	for (y=0; y<height; y++) for (x=0; x<width; x++) for (int d=0; d<dimension; d++) tmp(d,x,y)=(*this)(d,x,y)-a(d,x,y);
	return tmp;
}

VectorArray2d VectorArray2d::operator*(const double &f)
{	int x,y;
	VectorArray2d tmp(dimension,width,height,extras);
	for (y=0; y<height; y++) for (x=0; x<width; x++) for (int d=0; d<dimension; d++) tmp(d,x,y)=(*this)(d,x,y)*f;
	return tmp;
}
