function ImageScCNTSData(dat,variableInd)

if (variableInd=='h')
    imagesc(dat.xGrid,dat.yGrid,dat.data(:,:,1)');
    caxis([0 .1]);
elseif (strcmp(variableInd,'hshaded'))
    mdat=-dat.data(1:(dat.xPoints-1),1:(dat.yPoints-1),1)+dat.data(2:dat.xPoints,2:dat.yPoints,1);

    mdat(find(dat.data(1:(dat.xPoints-1),1:(dat.yPoints-1),1)<0.01))=-50;
    image(dat.xGrid,dat.yGrid,mdat'*1000+5000);
    cmvect=0.5*(tanh(100*(gray(10001)-.5))+1);
    cmvect(:,1)=cmvect(:,1)*1;
    cmvect(:,2)=cmvect(:,2)*.82;
    cmvect(:,3)=cmvect(:,3)*.55;
    cmvect(1,:)=[.5 .5 .5];
    colormap(cmvect);
elseif (strcmp(variableInd,'movingh'))
    jet2=jet(2560); jet2(1,:)=[0 0 0];
    colormap(jet2);   
    imagesc(dat.xGrid,dat.yGrid,dat.data(:,:,1)'.*((sqrt(dat.data(:,:,3).^2+dat.data(:,:,2).^2)'./dat.data(:,:,1)')>.001));
elseif (variableInd=='u')
    imagesc(dat.xGrid,dat.yGrid,dat.data(:,:,2)'./dat.data(:,:,1)');
elseif (variableInd=='v')
    imagesc(dat.xGrid,dat.yGrid,dat.data(:,:,3)'./dat.data(:,:,1)');
elseif (strcmp(variableInd,'speed'))
    imagesc(dat.xGrid,dat.yGrid,sqrt(dat.data(:,:,3).^2+dat.data(:,:,2).^2)'./dat.data(:,:,1)');
elseif (strcmp(variableInd,'fr'))
    imagesc(dat.xGrid,dat.yGrid,log(sqrt(dat.data(:,:,3).^2+dat.data(:,:,2).^2)'./(sqrt(dat.frCoef)*(dat.data(:,:,1)'.^1.5)))/log(10));
    colormap([gray(256);jet(256)]);
    caxis([-2 2]);
elseif (strcmp(variableInd,'div'))
    u=dat.data(:,:,2)./dat.data(:,:,1);
    v=dat.data(:,:,3)./dat.data(:,:,1);
    dx=dat.xGrid(2)-dat.xGrid(1);
    dy=dat.yGrid(2)-dat.yGrid(1);
    
    diverg=zeros(dat.xPoints,dat.yPoints);
    diverg(2:(dat.xPoints-1),2:(dat.yPoints-1)) = (v(3:dat.xPoints,2:(dat.yPoints-1))-v(1:(dat.xPoints-2),2:(dat.yPoints-1)))/dx + (u(2:(dat.xPoints-1),3:dat.yPoints)-u(2:(dat.xPoints-1),1:(dat.yPoints-2)))/dy;
    
    imagesc(dat.xGrid,dat.yGrid,diverg');
    colred=[ones(256,1)*255,(0:255)',(0:255)']/255;
    colblue=[(255:-1:0)',(255:-1:0)',ones(256,1)*255]/255;
    colormap([colred;colblue]);
    caxis([-1 1]);
else
    imagesc(dat.xGrid,dat.yGrid,dat.data(:,:,variableInd)');
end

pbaspect([dat.xSize,dat.ySize,1]);
axis([dat.xMin dat.xMin+dat.xSize dat.yMin dat.yMin+dat.ySize]);
set(gca,'YDir','normal')
truesize