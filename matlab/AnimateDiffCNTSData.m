function AnimateDiffCNTSData(dat,variableInd)

range=1:(length(dat)-2);
ranges=6:(length(dat)-40);
finaldat=dat(length(dat)).data(:,:,variableInd)';
times=zeros(1,length(dat));
amplitudes=zeros(1,length(dat));
waveamt=zeros(1,length(dat));
onemode=dat(5).data(:,:,variableInd)'-dat(200).data(:,:,variableInd)';

for i=range
 %   figure(1);
 %   imagesc(dat(i).xGrid,dat(i).yGrid,((dat(i).data(:,:,variableInd)')-finaldat));
  %  pbaspect([dat(i).xSize,dat(i).ySize,1]);
 %   colorbar
 %   axis([dat(i).xMin dat(i).xMin+dat(i).xSize dat(i).yMin dat(i).yMin+dat(i).ySize]);
 %   set(gca,'YDir','normal')
 %   truesize
 %   title(sprintf('t=%f',dat(i).time));
%    colormap(jet(2560));

  %  drawnow;
%    figure(2);
%    hold on
%    plot(dat(i).time,log10(norm((((dat(i).data(:,:,variableInd)')-finaldat)))),'kx');
%    pause(.03);
    times(i)=dat(i).time;
    amplitudes(i)=log(norm((((dat(i).data(:,:,variableInd)')-finaldat))));
    thismode=dat(i).data(:,:,variableInd)'-dat(200).data(:,:,variableInd)';
    lambda=sum(sum(thismode.*onemode))./sum(sum(thismode.*thismode));
    waveamt(i)=lambda.*norm(dat(i).data(:,:,variableInd)' - dat(200).data(:,:,variableInd)');
end
B = [ones(length(dat)-45,1) times(ranges)'] \ amplitudes(ranges)'
figure(2)
plot(times(ranges),amplitudes(ranges),'kx');
figure(3)
plot(times(ranges),waveamt(ranges),'k');
