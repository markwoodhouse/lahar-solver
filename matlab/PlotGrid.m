function PlotGrid(nxframes, nyframes, plotfunc, varargin)

optargin = size(varargin,2);

xshift=0; yshift=0;
 
xspace=0.5; yspace=0.5;
xbord=1; ybord=1;

xborderSet=2;
xspaceSet=1;
widthSet=0;
subwidthSet=0;

yborderSet=2;
yspaceSet=1;
heightSet=0;
subheightSet=0;

subaspectSet=0;

absposSet=0;
keepYAxes=0;
for i=1:2:optargin
    if (strcmp(varargin(i),'XBorder'))
        xbord=varargin{i+1};
        xborderSet=1;
    elseif (strcmp(varargin(i),'YBorder'))
        ybord=varargin{i+1};
        yborderSet=1;
    elseif (strcmp(varargin(i),'XShift'))
        xshift=varargin{i+1};
    elseif (strcmp(varargin(i),'YShift'))
        yshift=varargin{i+1};
    elseif (strcmp(varargin(i),'XSpace'))
        xspace=varargin{i+1};
        xspaceSet=1;
    elseif (strcmp(varargin(i),'YSpace'))
        yspace=varargin{i+1};
        yspaceSet=1;
    elseif (strcmp(varargin(i),'Width'))
        width=varargin{i+1};
        widthSet=1;
    elseif (strcmp(varargin(i),'Height'))
        height=varargin{i+1};
        heightSet=1;
    elseif (strcmp(varargin(i),'SubWidth'))
        subwidth=varargin{i+1};
        subwidthSet=1;
    elseif (strcmp(varargin(i),'SubHeight'))
        subheight=varargin{i+1};
        subheightSet=1;
    elseif (strcmp(varargin(i),'SubAspect'))
        subaspect=varargin{i+1};
        subaspectSet=1;
    elseif (strcmp(varargin(i),'AbsolutePosition'))
        abspos=varargin{i+1};
        absposSet=1;
    elseif (strcmp(varargin(i),'KeepYAxes'));
        keepYAxes=varargin{i+1};
    else
        error(['Unrecognised arg: ' varargin{i}]);
    end
end

if (absposSet==0) % If not using absolute positioning, check that we have enough things set
    if (widthSet && subwidthSet && xspaceSet && xborderSet)
        error('Conflict: width, subplot width, xspace and xborder all set');
    end

    if (widthSet && subwidthSet && xspaceSet)
        xbord = (width - nxframes*subwidth - (nxframes-1)*xspace)/2;
    elseif (xborderSet && subwidthSet && xspaceSet)
        width = 2*xbord + nxframes*subwidth + (nxframes-1)*xspace;
    elseif (widthSet && xborderSet && xspaceSet)
        subwidth = (width - 2*xbord - (nxframes-1)*xspace)/nxframes;
        subwidthSet=1;
    elseif (widthSet && subwidthSet && xborderSet)
        xspace = (width - 2*xbord - nxframes*subwidth)/(nxframes-1);
    else
        error('Not enough x-sizes set');
    end



    if (subaspectSet && subwidthSet && subheightSet)
        error('Conflict: subplot width, height and aspect all set');
    end

    if (subaspectSet && subwidthSet)
        subheight=subwidth/subaspect;
        subheightSet=1;
    end

    if (subaspectSet && subheightSet)
        subwidth=subheight*subaspect;
        subwidthSet=1;
    end


    if (heightSet && subheightSet && yspaceSet&& yborderSet)
        error('Conflict: height, subplot height, yspace and yborder all set');
    end
    if (heightSet && subheightSet && yspaceSet)
        ybord = (height - nyframes*subheight - (nyframes-1)*yspace)/2;
    elseif (yborderSet && subheightSet && yspaceSet)
       height = 2*ybord + nyframes*subheight + (nyframes-1)*yspace;
    elseif (heightSet && yborderSet && yspaceSet)
        subheight = (height - 2*ybord - (nyframes-1)*yspace)/nyframes;
    elseif (heightSet && subheightSet && yborderSet)
        yspace = (height - 2*ybord - nyframes*subheight)/(nyframes-1);
    else
        error('Not enough y-sizes set');
    end
else % absolute position mode
    if (~(subwidthSet && subheightSet && xspaceSet && yspaceSet))
        error('In absolute mode, need to set subwidth, subheight, xspace and yspace');
    end
    if (xborderSet==1 || yborderSet==1 || heightSet || widthSet)
       warning('In absolute mode, height, width and border properties are ignored'); 
    end
end

% Setup page size unless we have absolute position mode
if (absposSet==0)
    set(gcf,'PaperUnits','centimeters');
    left = 0; bottom = 0; 
    myfiguresize = [left, bottom, width, height];
    set(gcf, 'PaperPosition', myfiguresize);

    unis = get(gcf,'units');
    ppos = get(gcf,'paperposition');
    set(gcf,'units',get(gcf,'paperunits'));
    pos = get(gcf,'position');
    pos(3:4) = ppos(3:4);
    set(gcf,'position',pos);
    set(gcf,'units',unis);
else
    
    psize=get(gcf, 'PaperPosition');
    width=psize(3); height=psize(4);
    xofs=abspos(1)/width;
    yofs=abspos(2)/height;
end
xspacing=xspace/width;
yspacing=yspace/height;

xborder=xbord/width;
yborder=ybord/height;

subplotwidth = subwidth / width;
subplotheight = subheight / height;

if (absposSet==0)
    xpos=xborder+(0:(nxframes-1))*(subplotwidth+xspacing)+xshift*xborder;
    ypos=yborder+(0:(nyframes-1))*(subplotheight+yspacing)+yshift*yborder;
else
    xpos=xofs+(0:(nxframes-1))*(subplotwidth+xspacing);
    ypos=yofs+(0:(nyframes-1))*(subplotheight+yspacing);
end

for i=1:nxframes
    for j=1:nyframes
        frameIndex=i+j*nxframes;
        
        h(frameIndex)=axes('position',[xpos(i) ypos(j) subplotwidth subplotheight]);
        
       % box on;
       % plot(1:1000);
       % xlabel('x axis');
       % ylabel('y');
        plotfunc(i,j);
        
        
        if j~=1
            set(gca,'XTickLabel',[])
            set(get(gca,'XLabel'),'String','')
        end
        if (i~=1 && ~keepYAxes)
            set(gca,'YTickLabel',[])
            set(get(gca,'YLabel'),'String','')
        end
  
        set(gca,'Layer','top')
    end
end

%PrintLaTeX(1,'nicotest',width);