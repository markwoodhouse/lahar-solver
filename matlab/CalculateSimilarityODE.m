function CalculateSimilarityODE
  % hold on;
    opts=odeset('RelTol',1e-11,'AbsTol',1e-11,'Events',@event);
    
    
   % desiredVolume=pi/100;
  %  for hs=0.059:.0001:.06
  %   plot(hs,VolumeResidualFromH0(hs),'x');
   %  hold on
  %   drawnow
  % end
    H0=0.0595;
    eta0=1e-8;
    [h,hp]=powerseries(H0,eta0);
    [eta,hv]=ode45(@ode,[eta0 10],[h hp,0],opts);
    plot(eta,hv(:,1)); hold on;
    plot(-eta,hv(:,1));
    
    
    function vol=VolumeResidualFromH0(H0)
        eta0=1e-8;
        [h,hp]=powerseries(H0,eta0);
        [eta,hv]=ode45(@ode,[eta0 0.3],[h hp,0],opts);
        vol=2*hv(end,3);
        vol=vol-desiredVolume;
    end

    function [h,hp]=powerseries(H0,eta)
        U=1; CD=1; gp=1;
        K=0.8*U*sqrt(CD/gp);
        H1=-K.^2/(12*H0);
        h=H0+H1*(eta.^3);
        hp=3*H1*(eta.^2);
        
    end
    function [v,term,dir]=event(eta,hv)
        v=hv(1)-1e-5;
        term=1;
        dir=0;
    end
    function dh=ode(eta,hv)
        U=1; CD=1; gp=1;
        K=0.8*U*sqrt(CD/gp);
        h=hv(1);
        hp=hv(2);
        dh(1)=hp; % d/deta (H) = H'
        %dh(2)=(-K*(h+eta*hp) + 3*(-hp)^1.5 * h^0.5)/(h^1.5 * (-hp)^-.5);
        dh(2)=(-K*(h+eta*hp)*sqrt(h*(-hp)) - 3*hp^2*h)/(h^2);
        dh(3)=h; %integral of h wrt eta
        dh=dh';
    end        
end