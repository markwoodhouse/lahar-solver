function PrintFig(filename,varargin)
%  PrintFigu(figno, filename)
%  filename should have no extension

os=getenv('os');
switch os
    case {'Windows_NT'}
        windowsfilename = strrep(filename,'/','\');
        windowsCurrDir = pwd;
        currDir = strrep(windowsCurrDir,'\','/');
    otherwise
        if ismac
            os='MacOSX';
        else
            if isunix
                os='Unix';
            else
                error('Do not recognize operating system');
            end
        end
        currDir = pwd;
end

debug=0;
noborder=0;
% Turn debug on to enable pink bounding box
if (nargin>1)
    if (strcmp(varargin{1},'trim')==1)
        noborder=1;
    end
    if (strcmp(varargin{1},'debug')==1)
        debug=1;
    end
end

pause(.5); % avoid stupid MATLAB race conditions

figno=gcf;
if isobject(figno)
    figno=figno.Number;
end

unis = get(gcf,'units');
set(gcf,'Units','centimeters');
posvec=get(gcf,'Position');
width=posvec(3);
set(gcf,'Units',unis);

set(0, 'DefaultTextInterpreter', 'none');

% Set fixed-width font for PSfrag substitutions so that 
% number-width changes don't shift insertion points for LaTeX text.
% This should probably be done in laprint
hax = findobj(gcf,'Type','axes');
set(hax,'FontName','FixedWidth');
% Also using tablular figures in math mode, and math-mode tick lables,
% to ensure that axis labels that say '1' look properly aligned.

% Add corner annotations to stop bounding box shrinkage
if (noborder==0)
    a=annotation('line',[.0001 .001],[.001 .0001],'Color',[.995 .995 .995]);
    set(a,'LineWidth',0.001);
    annotation('line',[.9999 .999],[.999 .9999],'Color',[.995 .995 .995]);
    set(a,'LineWidth',0.001);
   % annotation('line',[.0001 .001],[.5 .5],'Color',[.995 .995 .995]);
   % annotation('line',[.9999 .999],[.5 .5],'Color',[.995 .995 .995]);
end

% Visible bounding box for debugging
if (debug==1)
    annotation('rectangle',[0 0 1 1],'Color',[1 .5 .5]);
end
    % 'keepfontprops', 'on',
    
switch os
    case {'Windows_NT'}
        longfilename=[windowsCurrDir,'\',windowsfilename];
    otherwise
        longfilename=[currDir,'/',filename];
end
         
laprintChris(figno,longfilename, 'asonscreen', 'on', 'mathticklabels','on','head', 'off', ...
    'width', width, 'scalefonts','off','factor', 1.0, 'figcopy','off', ...
    'printcmd',sprintf('print -f%d -depsc2 -r4000 -painters %s.eps',figno,filename));

fid = fopen([filename '.eps'], 'r');
str = fread(fid);
fclose(fid);
str = char(str');
str = strrep(str, 'ISOLatin1Encoding', 'StandardEncoding');
str = strrep(str, '0 cap', '1 cap');
str = strrep(str, '/DA { [6 dpi2point mul] 0 setdash } bdef', '/DA { [3 dpi2point mul] 0 setdash } bdef');
fid = fopen([filename '.eps'], 'w');
fprintf(fid, '%s', str);
fclose(fid);

switch os
    case {'Windows_NT'}
        system(sprintf('move %s.tex %s-psfragsubs.tex',windowsfilename,windowsfilename));
    otherwise
        system(sprintf('mv %s.tex %s-psfragsubs.tex',filename,filename));
end

fid = fopen(sprintf('%s.tex',filename), 'w');
fprintf(fid, '\\documentclass{article}\n');
fprintf(fid, '\\pagestyle{empty}\n');
fprintf(fid, '\\usepackage[margin=0.2cm,a2paper]{geometry}\n');
fprintf(fid, '\\usepackage{amsmath,amssymb}\n');
fprintf(fid, '\\usepackage{graphicx}\n');
fprintf(fid, '\\usepackage{psfrag}\n');
fprintf(fid, '\\usepackage{color}\n');

%fprintf(fid, '\\usepackage{mathptmx}\n');
%fprintf(fid, '\\usepackage[T1]{fontenc}\n');

%fprintf(fid, '\\usepackage[mathlf,mathtabular,swash,footnotefigures]{MinionPro}\n');
%fprintf(fid, '\\usepackage[T1]{fontenc}\n');

%fprintf(fid, '\\usepackage[scaled]{helvet}\n');
%fprintf(fid, '\\renewcommand*\\familydefault{\\sfdefault}\n');
%fprintf(fid, '\\usepackage[T1]{fontenc}\n');
%fprintf(fid, '\\usepackage{sfmath}\n');


fprintf(fid, '\\newcommand{\\df}{\\phantom{0}}\n');

k=strfind(filename,'/');
k=k(end);
result = filename(1:k-1);

%[status, result]=system(sprintf('dirname %s',filename))
line = ['\\graphicspath{{',result(1:(numel(result))), '/}}\n'];
fprintf(fid, line);
fprintf(fid, '\\begin{document}\n');
fprintf(fid,'\\small\n'); %%% HACK JFM-specific. Makes graphics font the same size as the caption font
line = ['\\input{',currDir,'/',filename,'-psfragsubs}\n'];
fprintf(fid, line);
fprintf(fid, '\\end{document}\n');

fclose(fid);

% Run psfragtopdf
k=strfind(filename,'/');
k=k(end);
dirName = filename(1:k-1);
baseName = filename(k+1:end);

switch os
    case {'Windows_NT'}
        firstStepName=[windowsfilename '-psfragtopdf1'];
        firstStepBaseName=[baseName '-psfragtopdf1'];
        commandLine=['COPY ' windowsfilename '.tex ' firstStepName '.tex'];
    otherwise
        [~,baseName]=system(['basename ' filename]);
        baseName=strtrim(baseName);
        [~,dirName]=system(['dirname ' filename]);
        dirName=strtrim(dirName);
        firstStepName=[filename '-psfragtopdf1'];
        firstStepBaseName=[baseName '-psfragtopdf1'];
        commandLine=['cp ' filename '.tex ' firstStepName '.tex'];
end
[s,retval]=system(commandLine);
if (s)
    fprintf(['cp failed: ' retval '\n']);
    close(gcf);
    return;
end

switch os
    case {'Windows_NT'}
        [s,latexCommand]=system(['where latex']);
    case {'MacOSX'}        
        latexCommand='/usr/texbin/latex';
    case {'Unix'}
        latexCommand='/usr/bin/latex';
end

commandLine=[latexCommand ' --interaction=nonstopmode -output-directory ' dirName ' ' firstStepName '.tex']
[s,retval]=system(commandLine);
if (s)
    fprintf(['LaTeX failed: ' retval]);
    close(gcf);
    return;
end

switch os
    case {'Windows_NT'}
        commandLine=['cd ' dirName '& dvips -q -t a2 -o ' firstStepBaseName '.ps ' firstStepBaseName '.dvi'] ;
    case {'MacOSX'}
        commandLine=['cd ' dirName '; /usr/texbin/dvips -q -t a2 -o ' firstStepBaseName '.ps ' firstStepBaseName '.dvi'] ;
    otherwise
        commandLine=['cd ' dirName '; dvips -q -t a2 -o ' firstStepBaseName '.ps ' firstStepBaseName '.dvi'] ;
end
[s,retval]=system(commandLine);
if (s)
    fprintf(['dvips failed: ' retval]);
    close(gcf);
    return;
end

switch os
    case {'Windows_NT'}
        commandLine=['ps2pdf -dPDFSETTINGS#/prepress -sPAPERSIZE#a2 ' firstStepName '.ps ' firstStepName '.pdf'];
    case {'MacOSX'}
        commandLine=['/opt/local/bin/ps2pdf -dPDFSETTINGS#/prepress -sPAPERSIZE#a2 ' firstStepName '.ps ' firstStepName '.pdf'];
    case {'Unix'}
        commandLine=['ps2pdf -dPDFSETTINGS=/prepress -sPAPERSIZE=a2 ' firstStepName '.ps ' firstStepName '.pdf'];
end
[s,retval]=system(commandLine);
if (s)
    fprintf(['ps2pdf failed: ' retval]);
    close(gcf);
    return;
end

switch os
    case {'Windows_NT'}
        commandline=['C:\"Program Files"\gs\gs9.10\bin\gswin64c -q -dBATCH -dNOPAUSE -sDEVICE#bbox ' firstStepName '.pdf'];
        [s,retval]=system(commandline);
        if (s)
            fprintf(['gswin64c bbox failed: ' retval]);
            close(gcf);
            return;
        end
        I=strfind(retval,'HiResBoundingBox: ');
        HiResBBstring = retval(I:end);
        I=strfind(HiResBBstring,':');
        HiResBB = HiResBBstring(I+2:end);
        HiResBB = str2num(HiResBB);
        
        CropBox(1) = HiResBB(1);
        CropBox(2) = HiResBB(2);
        CropBox(3) = HiResBB(3);
        CropBox(4) = HiResBB(4);
        
        CropBoxString = num2str(CropBox(1),'%.6f');
        CropBoxString = [CropBoxString,' ',num2str(CropBox(2),'%.5f')];
        CropBoxString = [CropBoxString,' ',num2str(CropBox(3),'%.5f')];
        CropBoxString = [CropBoxString,' ',num2str(CropBox(4),'%.5f')];
        
        commandline=['c:\"Program Files"\gs\gs9.10\bin\gswin64c -o ' filename '.pdf -sDEVICE#pdfwrite -c "[/CropBox [' CropBoxString ']" -c " /PAGES pdfmark" -f ' firstStepName '.pdf'];
        [s,retval]=system(commandline);
        if (s)
            fprintf(['gswin64c cropping failed: ' retval]);
            close(gcf);
            return;
        end
        
    case {'MacOSX'}
        
        commandLine=['/usr/texbin/pdfcrop --hires ' firstStepName '.pdf  ' filename '.pdf']; % --margins 1
        [s,retval]=system(commandLine);
        if (s)
            fprintf(['pdfcrop failed: ' retval]);
            close(gcf);
            return;
        end
        
    case {'Unix'}
        
        commandLine=['pdfcrop --hires ' firstStepName '.pdf  ' filename '.pdf']; % --margins 1
        [s,retval]=system(commandLine);
        if (s)
            fprintf(['pdfcrop failed: ' retval]);
            close(gcf);
            return;
        end
        
end

switch os
    case {'Windows_NT'}
        
        commandLine=['DEL ' firstStepName '.tex'];
        [s,retval]=system(commandLine);
        if (s)
            fprintf(['DEL failed: ' retval]);
            close(gcf);
            return;
        end
               
        commandLine=['DEL ' windowsfilename '.eps'];
        [s,retval]=system(commandLine);
        if (s)
            fprintf(['DEL failed: ' retval]);
            close(gcf);
            return;
        end
        commandLine=['DEL ' windowsfilename '-psfragsubs.tex'];
        [s,retval]=system(commandLine);
        if (s)
            fprintf(['DEL failed: ' retval]);
            close(gcf);
            return;
        end
        commandLine=['DEL ' windowsfilename '.tex'];
        [s,retval]=system(commandLine);
        if (s)
            fprintf(['DEL failed: ' retval]);
            close(gcf);
            return;
        end
        commandLine=['DEL ' firstStepName '.aux'];
        [s,retval]=system(commandLine);
        if (s)
            fprintf(['DEL failed: ' retval]);
            close(gcf);
            return;
        end
        commandLine=['DEL ' firstStepName '.log'];
        [s,retval]=system(commandLine);
        if (s)
            fprintf(['DEL failed: ' retval]);
            close(gcf);
            return;
        end
        commandLine=['DEL ' firstStepName '.pdf'];
        [s,retval]=system(commandLine);
        if (s)
            fprintf(['DEL failed: ' retval]);
            close(gcf);
            return;
        end
        commandLine=['DEL ' firstStepName '.dvi'];
        [s,retval]=system(commandLine);
        if (s)
            fprintf(['DEL failed: ' retval]);
            close(gcf);
            return;
        end
        commandLine=['DEL ' firstStepName '.ps'];
        [s,retval]=system(commandLine);
        if (s)
            fprintf(['DEL failed: ' retval]);
            close(gcf);
            return;
        end
        commandLine=['DEL ' firstStepName '.pfg'];
        [s,retval]=system(commandLine);
        if (s)
            fprintf(['DEL failed: ' retval]);
            close(gcf);
            return;
        end
    otherwise
        commandLine=['rm ' firstStepName '.tex ' filename '.eps ' filename '-psfragsubs.tex ' filename '.tex '  firstStepName '.aux ' firstStepName '.log ' firstStepName '.pdf ' firstStepName '.dvi ' firstStepName '.ps ' firstStepName '.pfg'];
        [s,retval]=system(commandLine);
        if (s)
            fprintf(['rm failed: ' retval]);
            close(gcf);
            return;
        end
end
        
close(gcf);
      
