  
function dataarr = LoadCNTSDataArray(filestring,range,doDataLoad)
  if nargin < 3
    doDataLoad=1;
  end
firsttime=1;
for i=1:length(range)
    str=sprintf(filestring,range(i));
    
    [fid,message] = fopen(str,'r');
    if (fid==-1)
            if (i>1)
                warning('SDKT:EndOfData','Stopped loading at %s due to end of data',sprintf(filestring,range(i-1)));
            else
                error('SDKT:FileNotFound','%s does not exist: no files loaded',sprintf(filestring,range(i)));
            end
           return;
    end
    fclose(fid);
    
    data=LoadCNTSData(str,doDataLoad);
    if (firsttime==1)
        datatimestamp=data.startTime;
        firsttime=0;
    else
        if (~strcmp(data.startTime,datatimestamp))
            warning('SDKT:EndOfData','Stopped loading at %s due to dataset change',sprintf(filestring,range(i-1)));
            return
        end
    end
    dataarr(i)=data;
end