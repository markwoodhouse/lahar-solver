% Loads data and settings from CartesianNTSolver.OutputResults() into a MATLAB structure

function dat = LoadCNTSData(filename,doDataLoad)
  if nargin < 2
    doDataLoad=1;
  end
fid = fopen(filename,'r');
tline = fgetl(fid);
nHeaderLines=sscanf(tline,'%d');

for i=0:(nHeaderLines-1)
    tline = fgetl(fid);
    linedat=sscanf(tline,'%d');
    headerType=linedat(1);
    
    dat.nExtras=0; % for pre-extras results files
    
    if (headerType==1)
        tmp_data=sscanf(tline,'%d %d %d %d');
        dat.nDims=tmp_data(2);
        dat.xPoints=tmp_data(3);
        dat.yPoints=tmp_data(4);
    elseif (headerType==2)
        tmp_data=sscanf(tline,'%d %f %f %f %f %f %f');
        dat.xMin=tmp_data(2);
        dat.yMin=tmp_data(3);
        dat.xSize=tmp_data(4);
        dat.ySize=tmp_data(5);
        dat.time=tmp_data(6);
        dat.cflNo=tmp_data(7);
        dat.xGrid=dat.xMin + (dat.xSize/dat.xPoints) .* ((1:dat.xPoints)-0.5);
        dat.yGrid=dat.yMin + (dat.ySize/dat.yPoints) .* ((1:dat.yPoints)-0.5);
    elseif (headerType==3)
       tmp_data=sscanf(tline,'%d %s%c%s');
       dat.outputTime=char(tmp_data(2:length(tmp_data))');
    elseif (headerType==4)
       tmp_data=sscanf(tline,'%d %s%c%s');
       dat.startTime=char(tmp_data(2:length(tmp_data))');
    elseif (headerType==5)
       tmp_data=sscanf(tline,'%d %d');
       dat.outputIndex=tmp_data(2);
    elseif (headerType==6)
       tmp_data=sscanf(tline,'%d %f');
       dat.frCoef=tmp_data(2);
    elseif (headerType==7)
       readstr=tline;
       [~,~,~,nextind] = sscanf(readstr,'%d',1);
       while (nextind<numel(readstr))
           readstr=readstr(nextind:end);
           [param_name,~,~,nextind] = sscanf(readstr,'%s ',1);
           readstr=readstr(nextind:end);
           [param_val,~,~,nextind] = sscanf(readstr,' %f ',1);
           eval(['dat.params.' param_name '=' sprintf('%0.20f',param_val) ';']);
           % Ugh - would be much nicer to have assignment of the new
           % variable made outside an eval statement to avoid conversion
           % from float to string and back.
           
       end
       
       % Reserved for named parameters
    elseif (headerType==8)
        tmp_data=sscanf(tline,'%d %d');
        dat.nExtras = tmp_data(2);
        if (dat.nExtras>0)
            formatstr='%d %d';
            for i=1:dat.nExtras
                formatstr=strcat(formatstr,' %f');
            end
            tmp_data=sscanf(tline,formatstr);
            dat.extras=tmp_data(3:end);
        end
    end
    % ignore i==2 - timestamp
    % ignore futher headers
end
if (doDataLoad==1 && dat.nDims>0 && dat.xPoints>0 && dat.yPoints>0)
    dat.data=reshape(fscanf(fid,'%f'),dat.xPoints,dat.yPoints,dat.nDims);
end
fclose(fid);