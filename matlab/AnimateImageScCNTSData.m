function AnimateImageScCNTSData(dat,variableInd)

range=1:length(dat);

for i=range
    ImageScCNTSData(dat(i),variableInd);
    title(sprintf('t=%f',dat(i).time));
    caxis([0 1]); colormap(jet(2560));
    drawnow;
   % pause(.3);
end