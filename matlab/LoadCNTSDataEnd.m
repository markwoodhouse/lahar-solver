  
function dataarr = LoadCNTSDataEnd(filestring,range)

firsttime=1;
str='';
for i=1:length(range)
    prevstr=str;
    str=sprintf(filestring,range(i));
    
    [fid,message] = fopen(str,'r');
    if (fid==-1)
           break;
    end
    fclose(fid);
    
    data=LoadCNTSData(str,0);
    if (firsttime==1)
        datatimestamp=data.startTime;
        firsttime=0;
    else
        if (~strcmp(data.startTime,datatimestamp))
            break;
        end
    end
end

if (isempty(prevstr))
    fprintf('Cant find any files of that name\n');
    return
end
fprintf('Loading %s\n',prevstr);
dataarr=LoadCNTSData(prevstr);
