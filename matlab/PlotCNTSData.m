function PlotCNTSData(dat,index,varargin)
% Plot 1-D data

firstavailablearg=1;
noexpand=0;
mode='';
optargin = size(varargin,2);
varlen=numel(varargin);
skip=1;
if (optargin>=firstavailablearg)
    
    if (firstavailablearg<varlen && strcmp(varargin(firstavailablearg),'Scale'))
        scaleindex=varargin{firstavailablearg+1};
        firstavailablearg=firstavailablearg+2;
    end
    if (firstavailablearg<=varlen &&strcmp(varargin(firstavailablearg),'NoExpand'))
        noexpand=1;
        firstavailablearg=firstavailablearg+1;
    end
    
    if (firstavailablearg<varlen && strcmp(varargin(firstavailablearg),'TScalePower'))
        tpow=varargin{firstavailablearg+1};
        firstavailablearg=firstavailablearg+2;
    end
    
    if (firstavailablearg<varlen && strcmp(varargin(firstavailablearg),'XScalePower'))
        xpow=varargin{firstavailablearg+1};
        firstavailablearg=firstavailablearg+2;
    end
    if (firstavailablearg<=varlen && strcmp(varargin(firstavailablearg),'semilogy'))
        mode='semilogy';
        firstavailablearg=firstavailablearg+1;
    end
    if (firstavailablearg<=varlen && strcmp(varargin(firstavailablearg),'loglog'))
        mode='loglog';
        firstavailablearg=firstavailablearg+1;
    end
    if (firstavailablearg<varlen && strcmp(varargin(firstavailablearg),'skip'))
        skip=varargin{firstavailablearg+1};
        firstavailablearg=firstavailablearg+2;
    end
end

if (dat.nExtras==0 || noexpand==1)
    xplot=dat.xGrid;
else
    xplot=dat.xGrid*dat.extras(1);
end

if (exist('xpow'))
    xplot=xplot.*((dat.time+1).^xpow);
end

if (numel(index)==1)
    if (~exist('scaleindex'))
        yplot=dat.data(:,1,index);
    else
        yplot=dat.data(:,1,index)./dat.data(:,1,scaleindex);
    end
else % Tread index as a vector of powers.
    yplot=ones(size(dat.data(:,1,1)));
    for ind=1:numel(index)
        yplot=yplot.*(dat.data(:,1,ind).^index(ind));
    end
end


%Setup skip, but add last point if it would not be there with this skip
%setting
plotindices=1:skip:numel(xplot);
if (skip > 1 && mod(numel(xplot),skip)~=1)
    plotindices=[plotindices numel(xplot)];
end

if (exist('tpow'))
    yplot=yplot.*((dat.time+1).^tpow);
end
if (strcmp(mode,'semilogy'))
    semilogy(xplot(plotindices),yplot(plotindices),varargin{firstavailablearg:end});
elseif (strcmp(mode,'loglog'))
    loglog(xplot(plotindices),yplot(plotindices),varargin{firstavailablearg:end});
else
    plot(xplot(plotindices),yplot(plotindices),varargin{firstavailablearg:end});
    
end

end

