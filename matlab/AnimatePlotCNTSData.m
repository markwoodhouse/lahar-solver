function AnimatePlotCNTSData(dat,variableInd,varargin)
%   Plots a sequence of 1D data files.
%
%   Usage:  AnimatePlotCNTSData(dat,variableInd,...)
%           AnimatePlotCNTSData(dat,variableInd,[ymin ymax],...)
%           AnimatePlotCNTSData(dat,variableInd,[xmin xmax ymin ymax],...)
%
%           If the first variable length argument is 'Pause', the following
%           double parameter sets the time (in seconds) between each frame.
%           Subsequent variable length arguments are passed directly to
%           the plot command.

range=1:length(dat);
firstavailablearg=1;

pauseTime=0.1;
xaxislims=[dat(1).xMin dat(1).xMin+dat(1).xSize];
yaxislims=[];
optargin = size(varargin,2);

if (optargin>=firstavailablearg)
    firstarg=varargin{1};
    if (isa(firstarg,'double'))
        firstavailablearg=firstavailablearg+1;
        if (numel(firstarg)==2)
            yaxislims=varargin{1};
        elseif (numel(varargin{1})==4)
            xaxislims=[firstarg(1) firstarg(2)];
            yaxislims=[firstarg(3) firstarg(4)];
        end
    end
end
if (optargin>=firstavailablearg+1)
    if (strcmp(varargin(firstavailablearg),'Pause'))
    pauseTime=varargin{firstavailablearg+1};
    firstavailablearg=firstavailablearg+2;
    end
end

for i=range
    PlotCNTSData(dat(i),variableInd,varargin{firstavailablearg:end});
    title(sprintf('t=%g',dat(i).time));
    
    if (numel(yaxislims)>0)
        set(gca,'YLim',yaxislims,'YLimMode','manual');    
    end
    set(gca,'XLim',xaxislims,'XLimMode','manual');    
   
    drawnow;
    pause(pauseTime);
end