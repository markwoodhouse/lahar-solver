function [C,h]=ContourCNTSData(dat,variableInd,contourArray)

if (variableInd=='h')
    plotdata=dat.data(:,:,1)';
    caxis([0 5]);
elseif (variableInd=='u')
    plotdata=dat.data(:,:,2)'./dat.data(:,:,1)';
elseif (variableInd=='v')
     plotdata=dat.data(:,:,3)'./dat.data(:,:,1)';
elseif (strcmp(variableInd,'speed'))
    plotdata=sqrt(dat.data(:,:,3).^2+dat.data(:,:,2).^2)'./dat.data(:,:,1)';
elseif (strcmp(variableInd,'fr'))
     plotdata=log(sqrt(dat.data(:,:,3).^2+dat.data(:,:,2).^2)'./sqrt(dat.frCoef)*(dat.data(:,:,1)'.^1.5))/log(10);
elseif (strcmp(variableInd,'div'))
    u=dat.data(:,:,2)./dat.data(:,:,1);
    v=dat.data(:,:,3)./dat.data(:,:,1);
    dx=dat.xGrid(2)-dat.xGrid(1);
    dy=dat.yGrid(2)-dat.yGrid(1);
    
    diverg=zeros(dat.xPoints,dat.yPoints);
    diverg(2:(dat.xPoints-1),2:(dat.yPoints-1)) = (v(3:dat.xPoints,2:(dat.yPoints-1))-v(1:(dat.xPoints-2),2:(dat.yPoints-1)))/dx + (u(2:(dat.xPoints-1),3:dat.yPoints)-u(2:(dat.xPoints-1),1:(dat.yPoints-2)))/dy;
    
    plotdata=diverg';
    colred=[ones(256,1)*255,(0:255)',(0:255)']/255;
    colblue=[(255:-1:0)',(255:-1:0)',ones(256,1)*255]/255;
    colormap([colred;colblue]);
else
     plotdata=dat.data(:,:,variableInd)';
end


if nargin==2
    if nargout==2
        [C,h]=contour(dat.xGrid,dat.yGrid,plotdata);
    else
        contour(dat.xGrid,dat.yGrid,plotdata);
    end
elseif nargin==3
    if nargout==2
        [C,h]=contour(dat.xGrid,dat.yGrid,plotdata,contourArray);
    else
        contourf(dat.xGrid,dat.yGrid,plotdata,contourArray);
    end
end
pbaspect([dat.xSize,dat.ySize,1]);
axis([dat.xMin dat.xMin+dat.xSize dat.yMin dat.yMin+dat.ySize]);
xlabel(['x (', sprintf('%d',dat.xPoints), ' grid points)']);
ylabel(['y (', sprintf('%d',dat.yPoints), ' grid points)']);