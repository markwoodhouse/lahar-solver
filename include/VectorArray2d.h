#ifndef ARRAY2D_H
#define ARRAY2D_H

#include <exception>
#include <stdexcept>

//using namespace std;

/// \brief Storage class for a 2D array, with N doubles at each point on the array, plus an 'extras' vector
class VectorArray2d
{
	private:
		int width, height, dimension, extras;
		double * __restrict__ data;

	public:
		int size() const {return width*height*dimension + extras;} ///< Returns total number of doubles stored
		int Width() const {return width;}
		int Height() const {return height;}
		int Dimension() const {return dimension;}
		int Extras() const {return extras;}
		void SetSize(int d, int w, int h, int ex=0);
		VectorArray2d();
		VectorArray2d(const VectorArray2d &rhs);
		VectorArray2d(int d, int w, int h, int ex=0);		
		VectorArray2d(int l); ///< Construct from integer: sets width=height=1, dimension=l, extras=0
		~VectorArray2d();
		
		double L2Norm(); ///< Return L2 norm over 2D-array part only (ignores extras)
		double L1Norm(); ///< Return L1 norm over 2D-array part only (ignores extras)
		double LInfNorm(); ///< Return L_infinity (maximum) norm over 2D-array part only (ignores extras)
		
		/// Get value at (x,y), dimension i
		inline double operator() (const int i, const int x, const int y) const 
		{
			#ifndef NDEBUG
			if (i>=dimension || i<0 || x>=width || x<0 || y>=height || y<0) throw "VectorArray2d bounds error";
			#endif
			return data[i+(x+y*width)*dimension];
		}
		
		/// Get reference to value at (x,y), dimension i
		inline double &operator() (const int i, const int x, const int y)
		{
			#ifndef NDEBUG
			if (i>=dimension || i<0 || x>=width || x<0 || y>=height || y<0) throw "VectorArray2d bounds error";
			#endif
			return data[i+(x+y*width)*dimension];
		}
		
		
		/// Get extra value i
		inline double operator() (const int i) const 
		{
			#ifndef NDEBUG
			if (i<0 || i>=extras) throw "VectorArray2d bounds error";
			#endif
			return data[i+height*width*dimension];
		}
		
		/// Get reference to extra value i
		inline double &operator() (const int i)
		{
			#ifndef NDEBUG
			if (i<0 || i>=extras) throw "VectorArray2d bounds error";
			#endif
			return data[i+height*width*dimension];
		}
		
		/// Get pointer to values at (x,y). Ptr(x,y)[i] will give the value of the i'th dimension at this point
		inline double* Ptr(const int x, const int y) const
		{
			#ifndef NDEBUG
			if (x>=width || x<0 || y>=height || y<0) throw "VectorArray2d bounds error";
			#endif
			return &(data[(x+y*width)*dimension]);
		}
		
		/// Get pointer to extras. ExtrasPtr()[i] will give the value of the i'th extra value
		inline double* ExtrasPtr() const
		{
			return &(data[(height*width)*dimension]);
		}
		
		/// Raw access to data, of length width*height*dimension + extras
		inline double operator[] (const int index) const {return data[index];}
		
		/// Raw access to data by reference, of length width*height*dimension + extras
		inline double &operator[] (const int index) {return data[index];}
		
		VectorArray2d &operator=(const VectorArray2d &rhs);
		VectorArray2d operator+ (const VectorArray2d &a);
		VectorArray2d operator- (const VectorArray2d &a);
		VectorArray2d operator* (const double &f);
		void Zero(); ///< Sets all data (2D and extras) to zero
};
#endif
