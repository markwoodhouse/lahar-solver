#include <map>
#include <string>
#include <exception>
#include <stdexcept>

/** \brief Parameter store: acts like an STL map with string key
*
* Key strings must be between 1 and 63 characters long, start with an alphabetic letter, and contain only alphanumerics and underscores. This is to ensure that parameters can be exported into MATLAB structures.
*/
template <typename ParamType>
class ParameterStore
{
public:
	typedef ParamType value_type;
	typedef ParamType & reference;
	typedef const ParamType & const_reference;
	typedef typename std::map<std::string,ParamType>::iterator iterator;
	typedef typename std::map<std::string,ParamType>::const_iterator const_iterator;

	iterator begin()
	{
		return paramMap.begin();
	}
	iterator end()
	{
		return paramMap.end();
	}

	void RegisterParameter(const std::string &name, ParamType valuePtr);
private:
	std::map<std::string,ParamType> paramMap;
};


template <typename ParamType>
void ParameterStore<ParamType>::RegisterParameter(const std::string &name, ParamType value)
{
	const int nKeywords=15;
	const static char *keywords[nKeywords] = {"nDims","xPoints","yPoints","xMin","yMin","xSize","ySize","time","cflNo","xGrid","yGrid","outputTime","startTime","outputIndex","extras"};

	// Check for valid MATLAB variable name
	int nameLength=name.length();
	if (nameLength<=0 || nameLength>=64)
		throw std::invalid_argument("Parameter name must be between 1 and 63 characters long in ParameterStore::RegisterParameter");

	if (!isalpha(name.c_str()[0]))
		throw std::invalid_argument("Parameter name must start with an alphabetic letter in ParameterStore::RegisterParameter");

	for (int i=0; i<nameLength; i++)
	{
		char c = name.c_str()[i];
		if (!(std::isalnum(c) || c=='_'))
			throw std::invalid_argument("Only alphanumeric characters and underscore allowed in parameter names in ParameterStore::RegisterParameter");
	}

	for (int i=0; i<nKeywords; i++)
		if (name==std::string(keywords[i])) throw std::invalid_argument("Parameter name clashes with a reserved keyword in ParameterStore::RegisterParameter");

	if (paramMap.find(name)!=paramMap.end())
		throw std::invalid_argument("Parameter already registered with this name in ParameterStore::RegisterParameter");

	paramMap[name]=value;
}
