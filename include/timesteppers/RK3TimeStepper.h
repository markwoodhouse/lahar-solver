#ifndef RK3TIMESTEPPER_H
#define RK3TIMESTEPPER_H

#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <assert.h>
#include "../VectorArray2d.h"
using namespace std;

/// 3rd order Runge-Kutta timestepper
class RK3TimeStepper : public TimeStepper
{
public:
	RK3TimeStepper() : cfl(0.25) {}
	
	void SetCFL(double cflNo) {cfl=cflNo;}
	const char *Name() {return "RK3";}
	
	bool Integrate(double& t, const double& tend, VectorFunctionBase *f, VectorArray2d& y)
	{
		// double timeInterval = tend - t;

		long _size = y.size(), outputStep=500000/_size+1;
		VectorArray2d intermed1(y), intermed2(y), ddt(y); // Setup internal data: allocate memory and set correct width/height/etc.
		
		ddt.Zero();

		if (tend <= t)
		{
			cerr << "End-time Tend is less than initial time" << endl;
			return true;
		}
		unsigned long step=0;
		do
		{
			double unitCFLTimeStep;
			f->CalculateExplicitTimeDerivative(y,t,ddt,unitCFLTimeStep);
			
			double advisedTimeStep=cfl*unitCFLTimeStep;
			double nextT=min(tend,t+advisedTimeStep);
			double thisDeltaT=nextT-t;
			
			for (int i=0; i<_size; i++) intermed1[i] = y[i]+thisDeltaT * ddt[i];
			f->CalculateExplicitTimeDerivative(intermed1,nextT,ddt,unitCFLTimeStep);
			for (int i=0; i<_size; i++) intermed2[i] = 0.75*y[i] + 0.25*(intermed1[i]+ thisDeltaT*ddt[i]);
			f->CalculateExplicitTimeDerivative(intermed2,nextT,ddt,unitCFLTimeStep);
			for (int i=0; i<_size; i++) y[i] = (1.0/3.0)*y[i] + (2.0/3.0)*(intermed2[i]+ thisDeltaT*ddt[i]);

			t=nextT;
			
			if (realtimeOutput)
			{
				if (!(step%outputStep))
				{
					cerr << "t=" << t << "                 \r";
					cerr.flush();
				}
				step++;
			}
			
		//	f->StepEndProcessing(y);
		} while (t < tend);
		return false;
	}
		
private:
	double cfl;
};
#endif
