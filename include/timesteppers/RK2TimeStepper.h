#ifndef RK2TIMESTEPPER_H
#define RK2TIMESTEPPER_H

#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include "../VectorArray2d.h"
using namespace std;

/// 2nd order Runge-Kutta timestepper
class RK2TimeStepper : public TimeStepper
{
public:
	RK2TimeStepper() : cfl(0.25) {}
	
	void SetCFL(double cflNo) {cfl=cflNo;}
	void SetMaxdt(double maxStep) {maxdt=maxStep;}
	const char *Name() {return "RK2";}
	bool Integrate(double& t, const double& tend, VectorFunctionBase *f, VectorArray2d& y)
	{
		// double timeInterval = tend - t;

		long size = y.size(), outputStep=500000/size+1;
		VectorArray2d intermed(y), ddt(y);
		intermed.Zero();
		ddt.Zero();

		if (tend <= t)
		{
			cerr << "End-time Tend is less than initial time" << endl;
			return true;
		}

		unsigned long step=0;
		do
		{
			double unitCFLTimeStep;
			f->CalculateExplicitTimeDerivative(y,t,ddt,unitCFLTimeStep);
			
			double advisedTimeStep=min(cfl*unitCFLTimeStep,maxdt);
			double nextT=min(tend,t+advisedTimeStep);
			double thisDeltaT=nextT-t;
			
			for (int i=0; i<size; i++) intermed[i] = y[i]+thisDeltaT * ddt[i];
			f->CalculateExplicitTimeDerivative(intermed,nextT,ddt,unitCFLTimeStep);
			for (int i=0; i<size; i++) y[i] = 0.5*y[i] + 0.5*(intermed[i]+ thisDeltaT*ddt[i]);

			t=nextT;

			if (realtimeOutput)
			{
				if (!(step%outputStep))
				{
					cerr << "t=" << t << "                 \r";
					cerr.flush();
				}
			}
			step++;

		//	f->StepEndProcessing(y);
		} while (t < tend);
		return false;
	}
		
private:
	double cfl;
	double maxdt;
};
#endif
