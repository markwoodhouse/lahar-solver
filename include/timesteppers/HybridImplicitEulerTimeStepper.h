#ifndef HYBRIDIMPLICITEULERTIMESTEPPER_H
#define HYBRIDIMPLICITEULERTIMESTEPPER_H

/*
 
- Our timestep takes the form: u_k+1 = u_k + dt*g(u_k) + dt*f(u_k+1)
  where g is the explicit source term function and f is the implicit source term function

- Write this as the nonlinear residual equation: F(u_k+1; u+k) = u_k+1 - dt f(u_k+1) - u_k - dt g(u_k) = 0

- Solve this for u_k+1 using a Newton-Raphson iteration

- We start the iteration with u_k+1 = u_k and apply the iteration
     u_k+1 -> u_k+1 - J(u_k+1)^-1 F(u_k+1; v_k)
  until u_k+1 converges.

- The Jacobian is defined by J = dF/d(u_k+1) = I - dt df(u_k+1)/d(u_k+1),

- The Jacobian and residual equation need to be evaluated every step of the iteration. However, the explicit
  part of the residuals (the part independent of u_k+1) can be precalculated before the Newton iteration starts.

- Note that decreasing dt means that the Jacobian gets increasingly close to I - therefore if
  the iteration doesn't converge, we can reduce dt and try again. (Not implemented, yet)
*/


#include <cmath>
#include <iostream>
#include <fstream>
#include <gsl/gsl_linalg.h>
#include "../VectorArray2d.h"

/** \brief 1st order hybrid-implicit Euler time stepper
 * 
 * This timestepper copes with stiff source terms by combining a first-order explicit Euler step for the spatial discretisation
 * and non-stiff source terms with an implicit Euler step for the stiff source terms. Because the source term equations are local
 * (to each finite-volume cell), the Jacobian of the implicit problem is highly block-diagonal, making it easy to invert (in other
 * words, we solve nXPoints*nYPoints nonlinear systems (one for each grid point) of size nDim, rather than solving one system of
 * size nXPoints*YPoints*nDim).
**/
class HybridImplicitEulerTimeStepper : public TimeStepper
{
public:
	HybridImplicitEulerTimeStepper()
	{
		DebugOutput(false);
		
		SetCFL(0.25);
		SetMaxResiduals(1e8);
		SetMinResiduals(1e-10);
		SetMaxIterations(100);
		SetStartupMaxTimestep(-1,-1);
	}
	
	void DebugOutput(double dbo) {debugOutput=dbo;}
	void SetCFL(double cflNo) {cfl=cflNo;}
	void SetMaxResiduals(double mr) {maxResiduals=mr;}
	void SetMinResiduals(double mr) {minResiduals=mr;}
	void SetMaxIterations(int mi) {maxIterations=mi;}
	const char *Name() {return "Hybrid implicit Euler";}
	
	
	bool Integrate(double& t, const double& tend, VectorFunctionBase *f, VectorArray2d& y)
	{
		int dMax=y.Dimension();
		long size = y.size(), outputStep=500000/size+1;
		
		VectorArray2d explicitPart(y);
		explicitPart.Zero();
		double *jacobian = new double[dMax * dMax];
		double *residuals = new double[dMax];
		gsl_vector *du = gsl_vector_alloc(dMax);
		gsl_permutation * p = gsl_permutation_alloc(dMax);

		if (tend <= t)
		{
			std::cerr << "End-time Tend is less than initial time" << std::endl;
			return true;
		}

		unsigned long step=0;
		do
		{
			double unitCFLTimeStep;
			int maxAttainedIterations=-1;
			double meanIterations=0;
			
			// Calculate explicit part of the residuals (also get timestep)
			f->CalculateExplicitTimeDerivative(y,t,explicitPart,unitCFLTimeStep);
			
			// Calculate actual timestep
			double advisedTimeStep=cfl*unitCFLTimeStep;
			if (t<startupPeriod)
			{
					if (startupMaxTimestep>0) advisedTimeStep=std::min(advisedTimeStep, startupMaxTimestep);
			}
			
			//advisedTimeStep=min(advisedTimeStep,0.1); // Timestep limit needs to be done properly
			double nextT=std::min(tend,t+advisedTimeStep);
			double dt=nextT-t;
			
			// Store explicit part of the residuals in advance of the Newton iteration
			for (int i=0; i<size; i++) explicitPart[i] = y[i] + dt * explicitPart[i];
			
			// Set initial value for the Newton iteration to be the explicit part of the timestep u_k + dt*g(u_k)
			// (This is a sensible initial guess, since if there is no implicit time derivative for a grid point
			//  it results in zero residuals, and an immidiate abort of the Newton iteration before the Jacobian
			//  is ever evaluated.)
			for (int i=0; i<size; i++) y[i] = explicitPart[i];
			for (int yi=0; yi<y.Height(); yi++) for (int xi=0; xi<y.Width(); xi++)
			{
				int iterationCount=0;
				int s; // Required for output argument of gsl_linalg_LU_decomp - but not used here
				while (1)
				{
					// Evaluate residuals
					for (int d=0; d<dMax; d++) residuals[d]=0;
					f->CalculateImplicitTimeDerivative(y.Ptr(xi,yi),t,residuals);
					for (int d=0; d<dMax; d++) residuals[d] = y(d,xi,yi) - dt*residuals[d] - explicitPart(d,xi,yi);
					
					// Evaluate residuals L2 norm
					double normResiduals=0;
					for (int d=0; d<dMax; d++) normResiduals+=residuals[d]*residuals[d];
					normResiduals=sqrt(normResiduals);
					
					if (normResiduals<minResiduals) break; // Converged, so leave iteration

					if (normResiduals>maxResiduals)
					{
						std::cerr << "Implicit Newton solve error: reached maxResidual of "<<maxResiduals<<" at iteration "<<iterationCount<<" at location ("<<xi<<", "<<yi<<")"<<std::endl;
						return true;
					}
					
					// Evaluate Jacobian
					for (int d=0; d<dMax*dMax; d++) jacobian[d]=0;
					f->CalculateImplicitJacobian(y.Ptr(xi,yi),t,jacobian);
					for (int d=0; d<dMax*dMax; d++) jacobian[d]*=-dt;
					for (int d=0; d<dMax; d++) jacobian[d*(1+dMax)]+=1.0;
			
					// Solve Jacobian equation to obtain du, and update y
					gsl_matrix_view m = gsl_matrix_view_array(jacobian, dMax, dMax);				
					gsl_vector_view b = gsl_vector_view_array(residuals, dMax);
					gsl_linalg_LU_decomp(&m.matrix, p, &s);
					gsl_linalg_LU_solve(&m.matrix, p, &b.vector, du);
					for (int d=0; d<dMax; d++) y(d,xi,yi) -= gsl_vector_get(du,d);

					iterationCount++;
					if (iterationCount>maxIterations)
					{
						std::cerr << "Implicit Newton solve error: reached maxIterations of "<<maxIterations<<" with residual norm "<<normResiduals<<" at location ("<<xi<<", "<<yi<<")"<<std::endl;
						return true;
					}
				}
				if (iterationCount>maxAttainedIterations) maxAttainedIterations=iterationCount;
				meanIterations+=iterationCount;
			}
			meanIterations /= y.Width() * y.Height();

			if (debugOutput) std::cerr << "t=" << t << "; dt=" << dt << "; max iterations=" << maxAttainedIterations << "; mean iterations=" << meanIterations << std::endl;
	
			t=nextT;

			if (realtimeOutput)
			{
				if (!(step%outputStep))
				{
					std::cerr << "t=" << t << "                 \r";
					std::cerr.flush();
				}
			}
			step++;

		} while (t < tend);

		gsl_permutation_free(p);
		gsl_vector_free(du);
		delete [] jacobian;
		delete [] residuals;

		return false;
	}
		
	void SetStartupMaxTimestep(double maxTimeStep, double period)
	{
		startupPeriod=period;
		startupMaxTimestep=maxTimeStep;
	}
private:
	bool debugOutput;
	
	double cfl; // CFL number
	double minResiduals; //the L2 norm of the residuals at which the Newton iteration is considered converged.
	double maxResiduals; // value of the L2 norm of the residuals at which an error is generated due to suspected divergence of the Newton iteration.
	int maxIterations; // maximum number of Newton iterations before an error is generated due to suspected non-convergence of the Newton iteration.
	double startupPeriod, startupMaxTimestep;
};
#endif
