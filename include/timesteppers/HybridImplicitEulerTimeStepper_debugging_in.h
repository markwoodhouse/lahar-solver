#ifndef RK2TIMESTEPPER_H
#define RK2TIMESTEPPER_H

#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <gsl/gsl_linalg.h>
#include "../VectorArray2d.h"
using namespace std;

/** \brief Old, nonfunctional.
**/
class HybridImplicitTimeStepper : public TimeStepper
{
public:
	HybridImplicitTimeStepper() : cfl(0.25) {}
	
	void SetCFL(double cflNo) {cfl=cflNo;}
	const char *Name() {return "Hybrid Implicit Euler";}
	bool Integrate(double& t, const double& tend, VectorFunctionBase *f, VectorArray2d& y)
	{
		// double timeInterval = tend - t;

		int dMax=y.Dimension();
			
		long size = y.size(), outputStep=500000/size+1;
		VectorArray2d explicitPart(y), implicitPart(y),ddt(y), nextNewtonIteration(y), newtonResiduals(y);
		ddt.Zero();
		explicitPart.Zero();
		implicitPart.Zero();
		nextNewtonIteration.Zero();
		newtonResiduals.Zero();
		double *jacobian = new double[dMax * dMax];
		double *fdjacobian = new double[dMax * dMax];

		if (tend <= t)
		{
			cerr << "End-time Tend is less than initial time" << endl;
			return true;
		}

		unsigned long step=0;
		do
		{
			double unitCFLTimeStep;
			f->CalculateExplicitTimeDerivative(y,t,ddt,unitCFLTimeStep);
			double advisedTimeStep=cfl*unitCFLTimeStep;
			advisedTimeStep=min(advisedTimeStep,0.1);
			double nextT=min(tend,t+advisedTimeStep);
			double thisDeltaT=nextT-t;
//			cout << "t=" << t <<" thisDeltaT="<<thisDeltaT<<endl;
/*

  F(u_k+1; u+k) = u_k+1 - dt f(u_k+1) - u_k - dt g(u_k) = 0

Using a Newton-Raphson iteration, where J=dF/d(v_k+1) = I - dt df(v_k+1)/d(v_k+1),
we start with v_k+1 = v_k, and apply the iteration

or J(du) = F
v_k+1_nextiteration -> v_k+1 - J(v_k+1)^-1 F(v_k+1; v_k)
until v_k+1 converges.

In the variables here
newtonResiduals = y - dt f(y) -  explicitPart

nextNewtonIteration = y - J^-1 * newtonResiduals
*/
			
			for (int i=0; i<size; i++) explicitPart[i] = y[i]+thisDeltaT * ddt[i];
			for (int i=0; i<size; i++) y[i] = explicitPart[i];
			
			gsl_vector *xvect = gsl_vector_alloc(dMax);
			gsl_permutation * p = gsl_permutation_alloc(dMax);
					
			
			for (int yi=0; yi<y.Height(); yi++) for (int xi=0; xi<y.Width(); xi++)
			{
			//	cout <<"Starting newton iteration for point " << xi<<", "<<yi<<endl;
				int iterCount=0;
				while (1)
				{
				//	cout << "    Start of Newton step:"<<endl;
				//if (xi==47 && yi==44) cout <<"    y="<<y(0,xi,yi)<<","<<y(1,xi,yi)<<","<<y(2,xi,yi)<<","<<y(3,xi,yi)<<endl;
					
					for (int d=0; d<dMax; d++) implicitPart(d,xi,yi)=0;
					f->CalculateImplicitTimeDerivative(y.Ptr(xi,yi),t,implicitPart.Ptr(xi,yi));
				//	cout <<"    implicit time derivative="<<implicitPart[0]<<","<<implicitPart[1]<<","<<implicitPart[2]<<","<<implicitPart[3]<<","<<endl;
			
					for (int d=0; d<dMax; d++) newtonResiduals(d,xi,yi) = y(d,xi,yi) + implicitPart(d,xi,yi)*(-thisDeltaT) - explicitPart(d,xi,yi);
					
					//cout <<"    residuals="<<newtonResiduals[0]<<","<<newtonResiduals[1]<<","<<newtonResiduals[2]<<","<<newtonResiduals[3]<<","<<endl;
					
					
					for (int d=0; d<dMax*dMax; d++) jacobian[d]=0;
					f->CalculateImplicitJacobian(y.Ptr(xi,yi),t,jacobian);
					
					/*for (int d=0; d<dMax; d++)
					{
						VectorArray2d yp(y), res(y), ypplus(y), resplus(y);
						res.Zero();
						resplus.Zero();
						ypplus[d]+=1e-8;
						f->CalculateImplicitTimeDerivative(yp,t,res);
						f->CalculateImplicitTimeDerivative(ypplus,t,resplus);
						for (int d1=0; d1<dMax; d1++) fdjacobian[d + dMax*d1] = (resplus(d1,0,0)-res(d1,0,0))/1e-8;
					}*/
				//	cout << "J=";
				//	for (int d1=0; d1<dMax; d1++) {for (int d=0; d<dMax; d++) cout << jacobian[d+dMax*d1] <<", "; cout << endl;}
				//	cout << endl;
					/*cout << "fdJ=";
					for (int d1=0; d1<dMax; d1++) {for (int d=0; d<dMax; d++) cout << fdjacobian[d+dMax*d1] <<", "; cout << endl;}
					cout << endl;
					*/
					
					for (int d=0; d<dMax*dMax; d++) jacobian[d]*=-thisDeltaT;
					for (int d=0; d<dMax; d++) jacobian[d*(1+dMax)]+=1.0;
			

					gsl_matrix_view m = gsl_matrix_view_array(jacobian, dMax, dMax);
				
					//cout << "    J="<<endl;
					//gsl_matrix_fprintf(stdout, &m.matrix, "%f");
					
					
					gsl_vector_view b = gsl_vector_view_array(newtonResiduals.Ptr(xi,yi), dMax);
					
					int s;
					gsl_linalg_LU_decomp(&m.matrix, p, &s);
					gsl_linalg_LU_solve(&m.matrix, p, &b.vector, xvect);

					//if (xi==47 && yi==44) cout << "    du=";
					double norm_du=0;
					for (int d=0; d<dMax; d++)
					{
						double comp=gsl_vector_get(xvect,d);
						y(d,xi,yi) -= comp;
						norm_du+=comp*comp;
					//	if (xi==47 && yi==44) cout << comp << ", ";
					}
		//	if (xi==47 && yi==44)	cout << endl << "    new y="<<y(0,xi,yi)<<","<<y(1,xi,yi)<<","<<y(2,xi,yi)<<","<<y(3,xi,yi)<<endl;
					norm_du=sqrt(norm_du);
					
		//		if (xi==47 && yi==44) cout << "    norm du="<<norm_du<<endl;
			//		cout << "    norm du="<<norm_du<<endl;
					if (norm_du<1e-10) break;
	//				cout << endl;
					iterCount++;
					if (iterCount>100) {cout << "Warning: reached "<<iterCount<<" iterations in newton solve at ("<<xi<<", "<<yi<<")"<<endl; throw "blah";}
				}
					//cout << "    new y="<<y[0]<<","<<y[1]<<","<<y[2]<<","<<y[3]<<endl;
	//			cout <<"Finished newton iteration for point " << xi<<", "<<yi<<endl;
			}

			gsl_permutation_free(p);
			gsl_vector_free(xvect);

			t=nextT;

			if (realtimeOutput)
			{
				if (!(step%outputStep))
				{
					cerr << "t=" << t << "                 \r";
					cerr.flush();
				}
			}
			step++;

		} while (t < tend);

		delete [] jacobian;
		delete [] fdjacobian;

		return false;
	}
		
private:
	double cfl;
};
#endif
