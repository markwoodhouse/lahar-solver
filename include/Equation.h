#ifndef EQUATION_H
#define	EQUATION_H
#include "ParameterStore.h"
#include <fstream>
#include <assert.h>
#include <vector>
#include <iostream>
#include <string>
#include <sstream>

#include "VectorArray2d.h"

typedef void (*ConvectionFluxFunction)(double *, const double*, const double*, const double*);
typedef void (*SourceTermFunction)(double *, double*, double *, double*, const double *, const double *);
typedef void (*ImplSourceTermFunction)(double *, double*);
typedef double (*WaveSpeedFunction)(const double*, const double*, const double*);
typedef void (*VectorSpatialFunctionPt)(double *, const double, const double);
typedef void (*ScalarSpatialFunctionPt)(double &, const double, const double);
typedef void (*ExtrasPt)(double *);
typedef void (*ExtrasFunction)(double *, const VectorArray2d*);
typedef void (*JacobianFunction)(double *, double, double *);

/** \brief Base class for equations
 * 
 * Derived classes need to call the appropriate constructor of this base class in their constructor
 * initialisation list, and override the FillOutEquations method with pointers to static methods
 * containing the equations.
 * 
 * Any equation parameters which are member data of the derived equation should be registered using
 * RegisterParameter in the derived equation constructor. These parameters will need to be static,
 * in order that they can be used in the static equation functions. This static data means that
 * derived equations must be treated as singletons.
 * 
 * This equation base class stores the dimensions of the problem (solved-for fields, not-solved-for fields, extras),
 * per-field flags relating to positivity preservation, and a set of named parameters for the problem. 
 **/
class Equation
{
public:
	/// \brief Construct equation with additional not-solved-for fields and an auxillary vector to be time-integrated
	Equation(int nSolved, int nSpecified, int nExtraValues) : 
		nSolvedFields(nSolved), nSpecifiedFields(nSpecified), nExtras(nExtraValues), flags(nSolved,0) {}
	/// \brief Construct equation with additional not-solved-for fields
	Equation(int nSolved, int nSpecified) : 
		nSolvedFields(nSolved), nSpecifiedFields(nSpecified), nExtras(0), flags(nSolved,0) {}
	Equation(int nDimensions) : 
	/// \brief Construct basic equation
		nSolvedFields(nDimensions), nSpecifiedFields(0),  nExtras(0), flags(nSolvedFields, 0) {}
		
	virtual ~Equation() {}
	
	/** \brief Virtual method by which the equation sets function pointers in the solver to the appropriate equations
	 * 
	 * The x convection flux (fx) and x min and max wave speeds (xMaxWs, xMinWS) must be set. The y equivalents
	 * must be set if the problem is not 1-dimensional (number of y points > 1). The implicit source term jacobian
	 * (jac) must be set if the implicit source term function (implst) is. The extras function (ef) and explicit
	 * source term (explst) functions are optional. The default value of optional functions is to return nothing/
	 * all zeros as appropriate.*/
	virtual void FillOutEquations(ConvectionFluxFunction &fx,								  
                              ConvectionFluxFunction &fy,
                              SourceTermFunction &explst,
                              ImplSourceTermFunction &implst,
							  JacobianFunction &jac,
                              WaveSpeedFunction &xMaxWS,
                              WaveSpeedFunction &yMaxWS,
                              WaveSpeedFunction &xMinWS,
                              WaveSpeedFunction &yMinWS,
							  ExtrasFunction &ef) = 0;
	
							  
	/// \brief Register a double as an equation parameter, to be stored in results files etc.
	void RegisterParameter(const std::string &name, double * value)
	{
		paramStore.RegisterParameter(name,value);
	}
	
	// Below is just setters and getters
	
	int NSolvedFields() {return nSolvedFields;}
	int NSpecifiedFields() {return nSpecifiedFields;}
	int NExtras() {return nExtras;}

	void OutputParameters(std::ostream &out=std::cout, char spacer=' ')
	{
		ParameterStore<double*>::iterator i;
		for (i=paramStore.begin(); i!=paramStore.end(); i++) out << i->first << spacer << *(i->second) << spacer;
	}
	
	std::string ParameterList()
	{
		std::ostringstream oss;
		ParameterStore<double*>::iterator i;
		for (i=paramStore.begin(); i!=paramStore.end(); i++) oss << i->first <<  " = " << *(i->second) << std::endl;
		return oss.str();
	}
	
	bool IsPositivityPreserving(unsigned field)
	{
		assert(field<nSolvedFields);
		return (flags[field]&PositivityPreserving)!=0;
	}

	bool IsPositivityReconstruct(unsigned field)
	{
		assert(field<nSolvedFields);
		return (flags[field]&PositivityReconstruct)!=0;
	}

	unsigned PositivityReconstructId(unsigned field)
	{
		assert(IsPositivityReconstruct(field));
		assert(field<nSolvedFields);
		return (flags[field]&0xFF00)>>8;
	}

protected:
	void SetPositivityPreserving(unsigned field)
	{
		assert(field<nSolvedFields);
		flags[field]|=1;
	}

	void ClearPositivityPreserving(unsigned field)
	{
		assert(field<nSolvedFields);
		flags[field]&=~1;
	}

	void SetPositivityReconstruct(unsigned field, unsigned char targetField)
	{
		assert(field<nSolvedFields);
		assert(targetField<nSolvedFields);
		flags[field]|=2;
		flags[field]&=~0xFF00;
		flags[field]+=(targetField<<8);
	}

	void ClearPositivityReconstruct(unsigned field)
	{
		assert(field<nSolvedFields);
		flags[field]&=~2; 
		flags[field]&=~0xFF00;
	}
	
private:
	unsigned nSolvedFields, nSpecifiedFields, nExtras;
	std::vector<unsigned long> flags; /// LSB of flags field is flags. Next byte is positivity-reconstruction field ID
	ParameterStore<double *> paramStore;
	
	static const int PositivityPreserving=1;
	static const int PositivityReconstruct=2;
	
};


#endif

