#ifndef KPSOLVER_H
#define KPSOLVER_H

/* Things that need sorting:
 *      Boundary conditions: work out what is going on. Mixed inflow/outflow?
 *      Non-calculated functions: correct BCs. Time-dependent?
 *      Actions before/after/during(?) timestep
 *      Proper documentation
 *      Plug-in timesteppers
 *      Initial step sizes? Essentially this is a case of rapid ODE source terms. Use eigenvalue-finding routine?
 *      Cell-edge boundary conditions
 *		Check that times being passed to derivative function evaluations in RK timesteppers are correct
 */

#include "VectorArray2d.h"
#include <vector>
#include <fstream>
#include <string>
#include "Equation.h"
#include "TimeStepper.h"

/*! \mainpage Kurganov-Petrova hyperbolic equation solver
 * 
	Typical usage:
	\code
	TidalDragSWEqn eqn(U, cD);
	eqn.RegisterParameter("inflowFlux",&inflowFlux);
	eqn.RegisterParameter("inflowRadius",&inflowRadius);
	eqn.RegisterParameter("CFL",&cfl);
	KPSolver solver(100, 100, &eqn, new HybridImplicitEulerTimeStepper());
	dynamic_cast<HybridImplicitEulerTimeStepper*>(solver.TimeStepperPtr())->SetCFL(cfl);
	solver.SetLimiter(KPSolver::MinMod1);
	solver.SetDomain(-5,-10,30,20);		
	solver.SetInitialConditions(InitialConditions,10);
	solver.SetExtrapolatedBoundaryConditions();	
	solver.Run(200.0,200);
	\endcode
 */


typedef double (*LimiterPt)(const double, const double);
typedef void (*BoundaryConditionPointer)(double *, const double, const double);

void NullBCFunction(double *u, double x, double y);


double GetTimer();
/** \brief Main calculation class

 Integrates hyperbolic conservation laws with source terms and diffusive fluxes of the form
 
 \f$ \frac{\mathrm{d}}{\mathrm{d}t} \mathbf{u} + \frac{\mathrm{d}}{\mathrm{d}x} \mathbf{F}_x(\mathbf{u}) + \frac{\mathrm{d}}{\mathrm{d}y} \mathbf{F}_y(\mathbf{u}) = \frac{\mathrm{d}}{\mathrm{d}x} \mathbf{Q}_x\left(\mathbf{u},\frac{\mathrm{d}u}{\mathrm{d}x},\frac{\mathrm{d}u}{\mathrm{d}y}\right) + \frac{\mathrm{d}}{\mathrm{d}y} \mathbf{Q}_y\left(\mathbf{u},\frac{\mathrm{d}u}{\mathrm{d}x},\frac{\mathrm{d}u}{\mathrm{d}y}\right) + \mathbf{s}(\mathbf{u})\f$ 
 
 in the rectangular domain
 
 \f$ (x,y) \in [x_\mathrm{min}, x_\mathrm{max}]\times[y_\mathrm{min}, y_\mathrm{max}] \f$.
  
 All functions can be dependent also on \f$ x \f$ and \f$ y \f$, by adding these fields to \f$ \mathbf{u} \f$.
 
 \section cantdo What can't be done:
 \li Earth-pressure coefficients: \f$ \mathbf{F} \f$ does not depend on derivatives of \f$ \mathbf{u} \f$
 \li Time-dependent equations, other than through boundary conditions (is this true?)
 \li Diffusive fluxes involving higher orders
 \li Non-rectangular domains
 
 Notes for future improvement:
\li Extra class of dimensions in the solver, which are derived from existing fields, e.g. derivatives for earth-pressure coef. Would need helper functions for derivatives.
\li Time-dependence in equations (easy - can have double time in Equation class that is set by timestepper, as is currently done for time in KPSolver)
\li Have solver templated on equation in order that equation functions are inlined.
\li Multi-threading
\li Extras in Equation (and VectorArray2D). Can this be done more nicely? The current ExtrasFunction in equations is a mess. Could have helper functions that evaluate at the end of the domain (in 1D)? Likewise could have helper functions in the Flux functions (etc.) which evaluate extras (and d/dt of extras). How do the helpers know which u, extras, dextras/dt to look at?
\li Split up this class. The Solver class has two main parts: the problem setup/outputs/shutdown, and the time deriative evaluation. There is still quite strong coupling between these sections, but the key thing is that the time derivative evaluations don't use u (it is passed as the parameter uvect), and the rest of the class doesnt use uLimX, etc. which are really just temporary variables to help compute the time derivative. It would be nice to split the class along these lines. Need to ascertain: are uLimX etc. stateless (in the sense that they could be set to junk before every call of CalculateExplicitTimeDerivative) or not? I think they should be for solved-for variables; for non-solved-for variables, the initial condition functions set up the values, which are then preserved. It would be nice to get rid of the hacky initial conditions code that currently exists.
 */
class KPSolver : public VectorFunctionBase
{
public:
	
	/** \brief 1-D problem constructor
	 * 
	 * The equation pointer is not deleted in the KPSolver destructor,
	 * but the timestepper pointer is (not quite sure of the logic here...).
	 * 
	 * The constructor doesn't do much apart from set up variables of the right
	 * dimension, so equation/timestepper properties don't have to be set before
	 * passing them to the constructor.  
	 */
	KPSolver(int nx, Equation *lEqn, TimeStepper *lTs);
	
	/** \brief 2-D problem constructor
	 * 
	 * The equation pointer is not deleted in the KPSolver destructor,
	 * but the timestepper pointer is (not quite sure of the logic here...).
	 * 
	 * The constructor doesn't do much apart from set up variables of the right
	 * dimension, so equation/timestepper properties don't have to be set before
	 * passing them to the constructor.  
	 */
	KPSolver(int nx, int ny, Equation *lEqn, TimeStepper *lTs);
	
	~KPSolver();
	
	
	/** \brief Sets problem initial conditions
	 * 
	 * As for the other overload of SetInitialConditions, but without the auxillary extra vector.
	 */
	void SetInitialConditions(VectorSpatialFunctionPt icf, int subSamplesPerDimension=1);
	
	/** \brief Sets problem initial conditions, with auxillary extra vector
	 * 
	 * icf is a function pointer which returns initial conditions at a given x and y location.
	 * 
	 * ept is a function pointer which returns initial values for each component of the auxillary extra vector.
	 * 
	 * With subSamplesPerDimension = 1 (the default) the cell-average initial conditions are taken
	 * to be point-values at the cell centres. Increasing subSamplesPerDimension causes more samples
	 * to be taken (subSamplesPerDimension^2 per cell), resulting in primitive integration
	 * of the initial condition function over the area of the finite-volume cell. This is useful for
	 * discontinuous initial conditions, but can get slow for subSamplesPerDimension>10 or so.
	 */
	void SetInitialConditions(VectorSpatialFunctionPt icf, ExtrasPt ept, int subSamplesPerDimension=1);
	
	/// \brief Loads initial conditions from a file
    void LoadInitialConditions(const char *filename, int d);

	/// \brief Loads initial conditions from an input stream
    void LoadInitialConditions(std::ifstream &in,int d);

	/// \brief Integrates the equations forward to destinationTime
	void IntegrateTo(double destinationTime);
	
	/// \brief Integrates the equations by a further integrationTime
	void IntegrateFor(double integrationTime);
	
	/// \brief Output ASCII format solution (including header and parameters) to a stream
	void OutputResults(std::ofstream &out);
	
	/// \brief Output ASCII format solution (including header and parameters) to file
	void OutputResults(std::string filename);
	
	/// \brief Output ASCII format solution (including header and parameters) to file
	void OutputResults(const char *filename);
	
	/// \brief Fills a VectorArray2d with cell-centred samples of a function, using the domain and cell geometry of this solver
	void SampleFunction(VectorArray2d &soln, VectorSpatialFunctionPt fn);
	
	/// \brief Sets terrain for KP positivity preservation. Terrain system not yet implemented.
	void SetTerrain(ScalarSpatialFunctionPt fn);

	void Run(double finalTime, int nSteps, const char *filename);
	void Run(const char *OutFile, double finalTime, int nSteps);
	
	//! Type of nonlinear limiter for TVD scheme
	enum Limiter
	{
		MinMod2, //!< MinMod 2: not very dissipative but prone to small oscillations
		MinMod1, //!< MinMod 1: more dissipative, not so prone to small oscillations, is the only one for which positivity-preservation is guaranteed
		WENO,    //!< Homebrew three-point WENO: doesn't have the branches of the MinMods, and so can introduce less limiter noise. Reduces to none for very smooth functions
		None	 //!< No limiter. Oscillates at shocks, but the best for smooth functions
	};
	
	//! Boundary condition types on the reconstructed gradient of the edge cells
	enum GradientBoundary
	{
		GradientPeriodic, //!< Periodic (set ghost cell to periodic value)
		GradientExtrapolate0, //!< Set ghost cell to the value of the edge cells -- usually resulting in zero gradient
		GradientExtrapolate1, //!< Set ghost cells through first order extrapolation --- the best 'outflow' condition
		GradientExtrapolate2  //!< Set ghost cells through second order extrapolation --- not really better than first order, and has the potential to be unstable.
	};
					
	//! Boundary condition types on the fluxes across the boundary of the domain
	enum FluxBoundary
	{
		FluxPeriodic, //!< Periodic flux
		FluxZeroFlux, //!< todo: document exactly what this does
		FluxSetValue, //!< todo: document exactly what this does
		FluxSetFlux, //!< todo: document exactly what this does
		FluxUseExtrapolated //!< todo: document exactly what this does
	};

	enum BoundaryID {North=0, South=1, East=2, West=3};


	void SetLimiter(Limiter l);
	void SetDomainSize(double xsize, double ysize);
	void SetDomainOffset(double xo, double yo);
	void SetDomain(double xo, double yo, double xsize, double ysize);

	void SetBoundaryConditionType(BoundaryID bid, int variable, GradientBoundary gradientBC, FluxBoundary fluxBC);
	void SetBoundaryConditionFunctions(BoundaryID bid, BoundaryConditionPointer valueFn=NullBCFunction, BoundaryConditionPointer fluxFn=NullBCFunction);
	void SetPeriodicBoundaryConditions();
	void SetExtrapolatedBoundaryConditions();
	
    void SetInitialStepSize(double val);
	
	/// \brief Returns the simulation time. Returns the correct value mid-way through integration steps, so can be used in BCs.
	double Time() {return evaluationTime;}
    
	int XPoints() {return nXPoints;}
	int YPoints() {return nYPoints;}
	int Dimension() {return dimension;}
	int SolvedDimensions() {return nSolvedDimensions;}
	
	const VectorArray2d& Solution() const {return u;}
	VectorArray2d& Solution() {return u;}

	TimeStepper *TimeStepperPtr() {return ts;}
	
private:
	TimeStepper *ts;
	Equation *eqn;
	GradientBoundary *gradientBCs[4];
	FluxBoundary *fluxBCs[4];
	BoundaryConditionPointer fluxValueBCFunction[4], fluxFluxBCFunction[4];
	
    double *uPlusConvectionFlux, *uMinusConvectionFlux;
	LimiterPt limiterPtr;
	int outputCount;
	std::string startTime;
	
	/// Constructing code; separated from constructors themselves for code reuse
	void Construct(int nx, int ny, Equation *lEqn, TimeStepper *lTs);
	void SetSpatialInitialConditions(VectorSpatialFunctionPt icf, int subSamplesPerDimension);
	
	void CalculateExplicitTimeDerivative(const VectorArray2d &uvect, const double t, VectorArray2d &result, double &unitCFL);
	//void CalculateImplicitTimeDerivative(VectorArray2d &uvect, double t, VectorArray2d &result);
	void CalculateImplicitTimeDerivative(double *uvect, double t, double *result);
	void CalculateImplicitJacobian(double *uPtr, double t, double *j)
	{
		jacobianFunction(uPtr, t, j);
	}
	
	void CalculateLimitedDerivs(const VectorArray2d&uvect);
	void CalculateLimitedDerivsBoundary(int d, int i, int j, const VectorArray2d&uvect, BoundaryID bid, GradientBoundary bType);
	void CalculateFluxes(const VectorArray2d &uvect, const VectorArray2d&results);
	void CalculateFluxBoundaryConditions(const VectorArray2d &uvect, const VectorArray2d &results);
	void SetBoundaryFlux(int i, int j,  BoundaryID bid, FluxBoundary *boundaryType, const VectorArray2d&uvect, const VectorArray2d &results);

	
	std::string GenerateOutputFilename(const char *base, int n, int width=6);

	double deltaX, deltaY, domainXSize, domainYSize, domainXOffset, domainYOffset;
	double deltaXRecip, deltaYRecip;
	
	double simulationTime;
	int dimension, nSolvedDimensions, nSpecifiedDimensions, nExtras, nXPoints, nYPoints;
	ConvectionFluxFunction xConvectionFlux, yConvectionFlux;
	WaveSpeedFunction xMaxWaveSpeedFunction, yMaxWaveSpeedFunction;
	WaveSpeedFunction xMinWaveSpeedFunction, yMinWaveSpeedFunction;
	SourceTermFunction explicitSourceTermFunction;
	ImplSourceTermFunction implicitSourceTermFunction;
	ExtrasFunction extrasFunction;
	JacobianFunction jacobianFunction;
	
	double posPresEps4;
	void SetPositivityPreservationHeight(double h) {posPresEps4=h*h*h*h;}
	
	VectorArray2d u;
	
	VectorArray2d uLimX;   ///< Limited x-derivatives at cell centres
	VectorArray2d uLimY;   ///< Limited y-derivatives at cell centres 
	VectorArray2d uPlusX;  ///< u value at plus side of an inter-cell discontinuity in x (i.e. value at the left-most point of the cell to the right of the discontinuity)
	VectorArray2d uMinusX; ///< u value at minus side of an inter-cell discontinuity in x  (i.e. value at the right-most point of the cell to the left of the discontinuity)
	VectorArray2d uPlusY;  ///< u value at plus side of an inter-cell discontinuity in y (i.e. value at the bottom-most point of the cell to above the discontinuity)
	VectorArray2d uMinusY; ///< u value at minus side of an inter-cell discontinuity in y  (i.e. value at the top-most point of the cell below the discontinuity)
	VectorArray2d hXFlux;  ///< Advective x-flux (on x-boundaries)
	VectorArray2d hYFlux;  ///< Advective y-flux (on y-boundaries)
	VectorArray2d terrainCorner;  ///< Terrain height, at cell corners
	VectorArray2d terrainEdgeX;  ///< Terrain height, at inter-cell discontinuity in x (i.e. at the centre of the line between a cell and the one to the left/right of it)
	VectorArray2d terrainEdgeY;  ///< Terrain height, at inter-cell discontinuity in y (i.e. at the centre of the line between a cell and the one above/below it)
	
	static double LimiterMinMod1(const double a, const double b);
	static double LimiterMinMod2(const double a, const double b);
	static double LimiterNone(const double a, const double b);
	static double LimiterWENO(const double a, const double b);
	
	double integrationStepSize;
	double unitCFLTimeStep;
	double evaluationTime;
	
	bool isOneD;
};

#endif

