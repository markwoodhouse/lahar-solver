#include "VectorArray2d.h"

/** \brief Abstract base for classes which provide a (vector) du/dt, given a (vector) u.
   
 Classes derived from VectorFunctionBase can be integrated with a TimeStepper
 */
class VectorFunctionBase
{
public:
	/// \brief Virtual method for calculating time-derivatives of a solution vector, given that solution vector and the time. Also calculates an appropriate time-step based on a CFL condition.
    virtual void CalculateExplicitTimeDerivative(const VectorArray2d& y/**< [in] solution */,
												 const double t/**< [in] time */, 
												 VectorArray2d& ddt/**< [out] dy/dt */, 
												 double &unitCFLTimeStep/**< [out] time step that corresponds to a CFL number of 1 */) = 0;
												 
    //virtual void CalculateImplicitTimeDerivative(VectorArray2d& y, double t, VectorArray2d& ddt) = 0;
	virtual void CalculateImplicitTimeDerivative(double *uvect, double t, double *result)=0;
    virtual void CalculateImplicitJacobian(double *uPtr, double t, double *j) = 0;
	
	// Not required any more. Will need this when putting DUMKA3 back in, but since we are no longer templating agains
	// the storage type (now fixed at VectorArray2d), this can be done in DUMKA3 itself.
    //virtual double GetNonZeroElement() = 0;
	
	virtual ~VectorFunctionBase() {}
};


/** \brief Abstract time-stepper base class
  * 
 Derived time-steppers override:
 \li an Integrate method, which performs the integration
 \li a Name method, which returns a human-readable const char * name of the time-stepping method
 
 Note that the equations and solution are passed to the Integrate method each time, suggesting
 that each call is independent of other calls. Since all timesteppers implemented so far are RK
 rather than linear multistep, this has been the case. Linear multistep methods really need to
 store a state, and so subsequent calls to Integrate would need to represent the same equations
 and start from the endpoint of the previous call. I <i>think</i> this is the case anyway in all
 circumstances where timesteppers are used, but would need to check. If this is the case, it may
 be more sensible to store t, f and (a copy of) y in the timestepper, along with the timestep
 history required for linear multistep methods.
 */
class TimeStepper
{
public: 
	TimeStepper() : realtimeOutput(false) 
	{
	}
	virtual ~TimeStepper() {}
	void RealtimeOutput(bool ra) {realtimeOutput=ra;}
	// Returns true on error
	/// Integrates y from t to tEnd, using the derivatives set by f
	virtual bool Integrate(double &t, const double &tEnd, VectorFunctionBase *f, VectorArray2d &y) = 0;
	
	/// Returns a human-readable name for this timestepper
	virtual const char *Name() = 0;
	
protected:
	bool realtimeOutput;
};
